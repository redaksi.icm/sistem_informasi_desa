<script>
$(function() {
var keyword = <?php echo $keyword?> ;
$( "#cari" ).autocomplete({
source: keyword
});
});
</script>
<script src="<?php echo base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC"> 
<!-- Start of Space Admin -->
<table class="inner">
<tr style="vertical-align:top">
		<td class="side-menu">
		<fieldset>
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset>
			<div  id="sidecontent3" class="lmenu">
				<ul>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li  class="selected"><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
		</fieldset>
	</td>
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
<h3>Data Keluarga</h3>
</div>

<div id="contentpane"> 
<form id="mainform" name="mainform" action="" method="post">
 
<div class="ui-layout-center" id="maincontent" style="padding: 5px;">
<div class="table-panel top">

<div class="left">
<select name="jenis" onchange="formAction('mainform','<?php echo site_url('analisis/laporan_keluarga/jenis_b')?>')">
<option value="">Jenis Analisis</option>
<option value="1" <?php if($jenis==1) :?>selected<?php endif?>>AKP</option>
<option value="2" <?php if($jenis==2) :?>selected<?php endif?>>BPS</option>
</select>

<select name="tahun"  onchange="formAction('mainform','<?php echo site_url('analisis/laporan_keluarga/tahun_b')?>')">';   
<option value="0"> -- Pilih Tahun -- </option>
<?php $i=2010;?>
<?php while($i++<2020){?>
<option value="<?php echo $i?>" <?php if($i==$tahun){?>selected<?php }?>><?php echo $i?></option>
<?php }?>
</select>      


<!--<select name="bulan"  onchange="formAction('mainform','<?php //=site_url('analisis/laporan_keluarga/bulan_b')?>')">';   
<option value="0"> -- Pilih Bulan -- </option>
<?php $i=0;?>
<?php while($i++<12){?>
<option value="<?php //=$i?>" <?php if($i==$bulan){?>selected<?php }?>><?php //=$i?></option>
<?php }?>
</select> --> 
<b>Laporan Analisis Keluarga Per Nilai Jawaban</b>
</div>


<div class="right">
<input name="cari" id="cari" type="text" class="inputbox help tipped" size="20" value="<?php echo $cari?>" title="Search.." onkeypress="if (event.keyCode == 13) {$('#'+'mainform').attr('action','<?php echo site_url('analisis/laporan_keluarga/search_b')?>');$('#'+'mainform').submit();}" />
<button type="button" onclick="$('#'+'mainform').attr('action','<?php echo site_url('analisis/laporan_keluarga/search_b')?>');$('#'+'mainform').submit();" class="uibutton tipsy south"title="Cari Data"><span class="icon-search icon-large">&nbsp;</span>Search</button>
</div>
</div>
<table class="list">
<thead>
<tr>
 <th>No</th>

<th align="left" align="center" width="100">No. KK</th>
<th align="left" align="center"width="100">Kepala Keluarga</th>
<?php  foreach($pertanyaan as $thead){ ?>
<th align="center" width="100"><?php echo $thead['id']?></th>
<?php }?>
<th align="left" align="center" width="100">Total</th>

</tr>
</thead>
<tbody>
<?php  foreach($analisis as $data): ?>
<tr>
 <td align="center" width="2"><?php echo $data['no']?></td>

<td><?php echo $data['no_kk']?></td>
<td><?php echo unpenetration($data['kepala_kk'])?></td>
<?php  $x=0; ?>
<?php  foreach($data['jawaban'] AS $jawab){ ?>
 	<?php  if($jawab){?>
		<td><?php echo $jawab['hasil']?></td>
		<?php  $x=$x+$jawab['hasil'] ; ?>
	<?php  }else{?>
	<td>-</td>
<?php  }}?>
<td><?php echo $x ;?></td>

</tr>
<?php  endforeach; ?>
</tbody>
</table>
</div>
</form>
<div class="ui-layout-south panel bottom">
<div class="left"> 
<div class="table-info">
<form id="paging" action="<?php echo site_url('analisis/laporan_keluarga/lap_detail_b')?>" method="post">
<label>Tampilkan</label>
<select name="per_page" onchange="$('#paging').submit()" >
<option value="50" <?php  selected($per_page,50); ?> >50</option>
<option value="100" <?php  selected($per_page,100); ?> >100</option>
<option value="200" <?php  selected($per_page,200); ?> >200</option>
</select>
<label>Dari</label>
<label><strong><?php echo $paging->num_rows?></strong></label>
<label>Total Data</label>
</form>
</div>
</div>
<div class="right">
<div class="uibutton-group">
<?php  if($paging->start_link): ?>
<a href="<?php echo site_url("analisis/laporan_keluarga/lap_detail_b/$paging->start_link/$o")?>" class="uibutton">First</a>
<?php  endif; ?>
<?php  if($paging->prev): ?>
<a href="<?php echo site_url("analisis/laporan_keluarga/lap_detail_b/$paging->prev/$o")?>" class="uibutton">Prev</a>
<?php  endif; ?>
</div>
<div class="uibutton-group">
 
<?php  for($i=$paging->start_link;$i<=$paging->end_link;$i++): ?>
<a href="<?php echo site_url("analisis/laporan_keluarga/lap_detail_b/$i/$o")?>" <?php  jecho($p,$i,"class='uibutton special'")?> class="uibutton"><?php echo $i?></a>
<?php  endfor; ?>
</div>
<div class="uibutton-group">
<?php  if($paging->next): ?>
<a href="<?php echo site_url("analisis/laporan_keluarga/lap_detail_b/$paging->next/$o")?>" class="uibutton">Next</a>
<?php  endif; ?>
<?php  if($paging->end_link): ?>
 <a href="<?php echo site_url("analisis/laporan_keluarga/lap_detail_b/$paging->end_link/$o")?>" class="uibutton">Last</a>
<?php  endif; ?>
</div>
</div>
 </div>
</div>
</td></tr></table>
</div>
