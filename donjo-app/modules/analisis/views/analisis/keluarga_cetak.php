<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Laporan Statistik</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url()?>assets/css/report.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">

<!-- Print Body -->
<div id="body">

<table>
    <tbody><tr>
        <td align="center" >
            <h1>LAPORAN ANALISIS KEMISKINAN RUMAH TANGGA <?php echo strtoupper($stat)?></h1>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px 20px;">
		<br>
            <table><tr>
<td>Kepala Rumah Tangga</td>
<td>:</td>
<td><?php  echo unpenetration($kk['nama']) ?></td></tr>
<tr><td>No. Rumah Tangga</td>
<td>:</td>
<td><?php  echo $kk['no_kk'] ?></td></tr>
<tr><td>Tahun</td>
<td>:</td>
<td><?php  echo $_SESSION['tahun'] ?></td></tr>



</table>
<p>&nbsp;</p>

<?php  foreach($nilai AS $data){?>
<table width="100%" id="tfhover" class="tftable" border="1">
        <tbody>
                <tr>
                        <td width=25%>Paling Tinggi (Maksimum)</td>
                        <td><?php  echo $data['nilai_max'];?></td>
                </tr>
                <tr>
                        <td>Paling Rendah (Minimum)</td>
                        <td><?php  echo $data['nilai_min'];?></td>
                </tr>
                <tr>
                        <td>Rata-rata</td>
                        <td><?php  echo $data['nilai_rata'];?></td>
                </tr>
        </tbody>
</table>
<?php }?>

<p>&nbsp;</p>
<p>&nbsp;</p>


<div>
<table width="100%" id="tfhover" class="tftable" border="1">
<thead>
<tr><th>No</th>
<th>Indikator</th>
<th>Bobot</th>
<th>Pilihan Jawaban</th>
<th>Nilai Jawaban</th>
<th>Hasil Analisis</th></tr>
</thead>

<tbody>
<?php  $i=1; $tot_bobot=0; $tot_nilai=0; $tot_hasil=0;?>
<?php  foreach($analisis AS $data){?>
	<tr><td><?php  echo $i;?></td>
	<td><?php  echo $data['pertanyaan']?></td>
	<td align="center"><?php  echo $data['bobot']?> </td>
	<td><?php  echo $data['jawaban']?> </td>
	<td align="center"><?php  echo $data['nilai']?> </td>
	<td align="center">
	<?php  echo $data['hasil'] ?>
	</td>
	</tr>
<?php  $i++;$tot_bobot=$tot_bobot+$data['bobot'];$tot_nilai=$tot_nilai+$data['nilai'];$tot_hasil=$tot_hasil+$data['hasil']; } ?>
</tbody>

<tfoot>
<tr><th></th>
<th>JUMLAH</th>
<th><?php  echo $tot_bobot;?></th>
<th></th>
<th><?php  echo $tot_nilai;?></th>
<th><?php  echo $tot_hasil;?></th>
</tr>
</tfoot>
</table>      
</div>
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>

</body></html>
