<?php 

class Analisis_Laporan_Keluarga_Model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	
	function autocomplete(){
		$sql   = "SELECT dusun_nama FROM tweb_wil_dusun";
		$query = $this->db->query($sql);
		$data  = $query->result_array();
		
		$i=0;
		$outp='';
		while($i<count($data)){
			$outp .= ",'" .$data[$i]['dusun_nama']. "'";
			$i++;
		}
		$outp = strtolower(substr($outp, 1));
		$outp = '[' .$outp. ']';
		return $outp;
	}
		function get_config(){	
		$sql   = "SELECT * FROM config WHERE 1";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}
	function search_sql(){
		if(isset($_SESSION['cari'])){
		$cari = $_SESSION['cari'];
			$kw = $this->db->escape_like_str($cari);
			$kw = '%' .$kw. '%';
			$search_sql= " AND u.nama LIKE '$kw'";
			return $search_sql;
			}
		}

	function jenis_sql(){		
		if(isset($_SESSION['jenis'])){
			$kh = $_SESSION['jenis'];
			$jenis_sql= " AND jenis = $kh";
		return $jenis_sql;
		}
	}

	function tahun_sql(){		
		if(isset($_SESSION['tahun'])){
			$kh = $_SESSION['tahun'];
			$tahun_sql= " AND tahun = $kh";
		return $tahun_sql;
		}
	}

	function bulan_sql(){		
		if(isset($_SESSION['bulan'])){
			$kh = $_SESSION['bulan'];
			$bulan_sql= " AND bulan = $kh";
		return $bulan_sql;
		}
	}

	function dusun_sql(){		
		if(isset($_SESSION['dusun'])){
			$kf = $_SESSION['dusun'];
			if($kf<>''){
			$dusun_sql= " AND c.dusun = '$kf'";}
		return $dusun_sql;
		}
	}
	
	function rw_sql(){		
		if(isset($_SESSION['rw'])){
			$kf = $_SESSION['rw'];
			$rw_sql= " AND c.rw = '$kf'";
		return $rw_sql;
		}
	}
	
	function rt_sql(){		
		if(isset($_SESSION['rt'])){
			$kf = $_SESSION['rt'];
			$rt_sql= " AND c.rt = '$kf'";
		return $rt_sql;
		}
	}
    
	function search_keluarga_sql(){
		if(isset($_SESSION['cari'])){
		$cari = $_SESSION['cari'];
			$kw = $this->db->escape_like_str($cari);
			$kw = '%' .$kw. '%';
			$search_keluarga_sql= " AND (kepala_kk LIKE '%$cari%' OR no_kk LIKE '%$cari%')";
			return $search_keluarga_sql;
			}
		}
		
	function paging($lap=0,$p=1,$o=0){
			
		$sql      = "SELECT count(s.id) as id FROM  sub_analisis_keluarga s INNER JOIN master_analisis_keluarga m ON s.id_master=m.id   ";
		
	
		$sql     .= $this->jenis_sql();   
		//$sql     .= $this->tahun_sql();
		//$sql     .= $this->bulan_sql();  
		$query    = $this->db->query($sql);
		$row      = $query->row_array();
		$jml_data = $row['id'];
		
		$this->load->library('paging');
		$cfg['page']     = $p;
		$cfg['per_page'] = $_SESSION['per_page'];
		$cfg['num_rows'] = $jml_data;
		$this->paging->init($cfg);
		
		return $this->paging;
	}
	
	
	function list_data($lap=0,$o=0,$offset=0,$limit=500){
	
		//Paging SQL
			$daerah = "";
			$kluster = "";
			if(isset($_SESSION['kategori'])){
				$kf = $_SESSION['kategori'];
				$kw="(";
				$i=0;
				while($i<count($kf)){
					$kw .=$kf[$i].",";
					$i++;
				}
				$kw =$kw."7)";
				$kelas_sql= " AND k.id IN $kw";
				$daerah = $kelas_sql;
			}
			
			if(isset($_SESSION['klaster'])){
				$kf = $_SESSION['klaster'];
				$kw="(";
				$i=0;
				while($i<count($kf)){
					$kw .=$kf[$i].",";
					$i++;
				}
				$kw =$kw."7)";
				$kelas_sql= " AND l.id IN $kw";
				$kluster = $kelas_sql;
			}
				
			$sql   = "SELECT max,id FROM tipe_analisis WHERE aktif = 1";
			$query = $this->db->query($sql);
			$row  = $query->row();
			$b = $row->id;
			$d = "dari".$b;
			$s = "sampai".$b;
		
		$paging_sql = ' LIMIT ' .$offset. ',' .$limit;
		
		/*$sql="SELECT * FROM (SELECT a.jenis, a.id as id_master, b.id as id_sub, a.nama as pertanyaan, b.nama as jawaban,bulan, tahun, (SELECT (COUNT(id)) FROM analisis_keluarga where id_master=a.id and id_sub_analisis=b.id and bulan=$_SESSION[bulan] and tahun=$_SESSION[tahun]) as jml_responden FROM master_analisis_keluarga a LEFT JOIN sub_analisis_keluarga b  ON a.id=b.id_master LEFT JOIN analisis_keluarga d ON b.id=d.id_sub_analisis  and bulan=$_SESSION[bulan] and tahun=$_SESSION[tahun] GROUP BY a.id, b.id, a.nama , b.nama ,bulan, tahun,(SELECT (COUNT(id)) FROM analisis_keluarga where id_master=a.id and id_sub_analisis=b.id and bulan=$_SESSION[bulan] and tahun=$_SESSION[tahun])) AS TB WHERE 1  ";*/
		$sql="SELECT m.jenis, m.id as id_master, s.id as id_sub, m.nama as pertanyaan, s.nama as jawaban, (SELECT COUNT(a.id_kel) FROM analisis_keluarga a LEFT JOIN hasil_analisis_keluarga h ON a.id_kel = h.id_kel AND h.tahun = a.tahun LEFT JOIN klasifikasi_analisis_keluarga k ON h.hasil > k.dari AND h.hasil < k.sampai LEFT JOIN klasifikasi_analisis_keluarga l ON h.hasil2 > l.$d AND h.hasil2 < l.$s WHERE a.id_sub_analisis=s.id AND a.tahun=$_SESSION[tahun] AND k.id $daerah $kluster) as jml_responden FROM sub_analisis_keluarga s INNER JOIN master_analisis_keluarga m ON s.id_master=m.id ";
			
		$sql     .= $this->jenis_sql();
		$sql .= $paging_sql;
		print $sql;
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		//Formating Output
		$i=0;
		$j=$offset;
		$x=$offset;$gakosong="";
		while($i<count($data)){
			//$data[$i]['no']=$j+1;
			
			//if($data[$i]['jumlah']<1)
				$data[$i]['jumlah']="-";	
					
			$data[$i]['tanya']=$gakosong;
			if($data[$i]['tanya']==$data[$i]['pertanyaan']){
				$data[$i]['tanya']="";
			}else{
				$data[$i]['tanya']=$data[$i]['pertanyaan'];
				$gakosong=$data[$i]['pertanyaan'];				
			}
			if($data[$i]['tanya']<>""){
				$data[$i]['no']=$x+1;
				$x++;
			}
			$i++;
			$j++;
			
		}
		return $data;
	}

	function list_data_detail($stat=0,$jwb=0,$thn='',$bln='',$lap=0,$o=0,$offset=0,$limit=500){
	
		$paging_sql = ' LIMIT ' .$offset. ',' .$limit;
		$thnsql= "   AND tahun=$thn ";		
		$blnsql= "  AND bulan=$bln  ";			
		
		$sql   = "SELECT k.*, p.nama as kepala_kk, (SELECT COUNT(id) FROM tweb_penduduk WHERE id_kk=a.id_kel) as jml_anggota, c.dusun, c.rw, c.rt 
				FROM analisis_keluarga a LEFT JOIN tweb_rtm k ON a.id_kel=k.id LEFT JOIN tweb_penduduk p ON k.nik_kepala=p.id LEFT JOIN tweb_wil_clusterdesa c ON p.id_cluster=c.id LEFT JOIN master_analisis_keluarga m ON a.id_master=m.id LEFT JOIN sub_analisis_keluarga s ON a.id_sub_analisis=s.id WHERE  a.id_master=$stat AND id_sub_analisis=$jwb  ";  
		
		$sql .= $thnsql;
		//$sql .= $blnsql;
		$sql .= $this->dusun_sql();
		$sql .= $this->rw_sql();
		$sql .= $this->rt_sql();
		
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		//Formating Output
		$i=0;
		$j=$offset;
		$x=$offset;
		while($i<count($data)){
			$data[$i]['no']=$j+1;
			
			$i++;
			$j++;
			
		}

		return $data;
	}
	
	function get_master_analisis_keluarga($id=0){
		$sql   = "SELECT * FROM master_analisis_keluarga WHERE id=$id";
		$query = $this->db->query($sql);
		$data['tanya']=$query->row_array();
		return $data;
	}

	function get_sub_analisis_keluarga($id=0){
		$sql   = "SELECT * FROM sub_analisis_keluarga WHERE id=$id";
		$query = $this->db->query($sql);
		$data['jawab']=$query->row_array();
		return $data;
	}
        function list_graph(){
		$sql   = "SELECT ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]-5 ),0) as satu,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]-4 ),0) as dua,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]-3 ),0) as tiga,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]-2 ),0) as empat,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]-1 ),0) as lima,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun] ),0) as enam,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]+1 ),0) as tujuh,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]+2 ),0) as delapan,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]+3 ),0) as sembilan,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]+4 ),0) as sepuluh,
					ifnull((SELECT round(AVG(hasil),2) FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]+5 ),0) as sebelas
		           FROM hasil_analisis_keluarga where  tahun='$_SESSION[tahun]' limit 1 ";
//		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->row_array();
		//print_r($data);
		return $data;
	
	}
	
        function list_graph_tanya($id=''){
		$sql   = "SELECT a.id as idmaster, a.nama as pertanyaan, b.id as idanalisis, b.nama as jawaban, 
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-5),0) as satu,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-4),0) as dua,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-3),0) as tiga,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-2),0) as empat,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-1),0) as lima,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]),0) as enam,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+1),0) as tujuh,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+2),0) as delapan,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+3),0) as sembilan,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+4),0) as sepuluh,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+5),0) as sebelas
		FROM master_analisis_keluarga a LEFT JOIN sub_analisis_keluarga b ON a.id=b.id_master WHERE a.id='$id' ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->result_array();
		//print_r($data);
		return $data;
	
	}

         function list_graph_jawab($id='',$id2=''){
		$sql   = "SELECT a.id as idmaster, a.nama as pertanyaan, b.id as idanalisis, b.nama as jawaban, 
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-5 ),0) as satu,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-4 ),0) as dua,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-3 ),0) as tiga,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-2 ),0) as empat,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]-1 ),0) as lima,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun] ),0) as enam,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+1 ),0) as tujuh,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+2 ),0) as delapan,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+3 ),0) as sembilan,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+4 ),0) as sepuluh,
		ifnull((SELECT COUNT(id_kel) FROM analisis_keluarga WHERE id_master=a.id AND id_sub_analisis=b.id AND tahun=$_SESSION[tahun]+5 ),0) as sebelas
		FROM master_analisis_keluarga a LEFT JOIN sub_analisis_keluarga b ON a.id=b.id_master WHERE a.id='$id' AND b.id='$id2' ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->row_array();
		//print_r($data);
		return $data;
	
	}
	function get_tanya($id=''){
		$sql   = "SELECT * FROM master_analisis_keluarga WHERE id='$id' ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->row_array();
		//print_r($data);
		return $data;
	
	}
	function get_jawab($id=''){
		$sql   = "SELECT * FROM sub_analisis_keluarga WHERE id='$id' ";
		//$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->row_array();
		//print_r($data);
		return $data;
	
	}
	
		function pagingx($p=1,$o=0){
	
		$sql      = "select count(id) as id from (SELECT (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id   AND tahun='$_SESSION[tahun]') as hasil,u.*,t.nama AS kepala_kk, t.nik FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id  order by (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id   AND tahun='$_SESSION[tahun]') desc) as tb1 where 1 ";
		$sql .= $this->search_keluarga_sql();   

		$query    = $this->db->query($sql);
		$row      = $query->row_array();
		$jml_data = $row['id'];
		
		$this->load->library('paging');
		$cfg['page']     = $p;
		$cfg['per_page'] = $_SESSION['per_page'];
		$cfg['num_rows'] = $jml_data;
		$this->paging->init($cfg);
		
		return $this->paging;
	}
	function list_pertanyaan(){
		$sql   = "SELECT * FROM master_analisis_keluarga WHERE aktif = 1 ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
		$data = $query->result_array();
	
		return $data;
	
	}
	function list_analisis_keluargax($o=0,$offset=0,$limit=500){
		//menurun
		$paging_sql = ' LIMIT ' .$offset. ',' .$limit;
		$sql   = "select * from (SELECT (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id   AND tahun='$_SESSION[tahun]') as hasil,u.*,t.nama AS kepala_kk, t.nik FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id  order by (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id   AND tahun='$_SESSION[tahun]') desc) as tb1 where 1 ";
		
		//$sql="SELECT k.no_kk, p.nama as kepala_kk FROM tweb_rtm k LEFT JOIN tweb_penduduk p ON k.nik_kepala=p.id ";
		//$sql   = "select * from (SELECT (select sum(nilai*bobot) from analisis_keluarga a inner join sub_analisis_keluarga b on a.id_sub_analisis=b.id inner join master_analisis_keluarga c on b.id_master=c.id where a.id_kel=u.id  AND bulan='$_SESSION[bulan]' AND tahun='$_SESSION[tahun]') as hasil,u.*,t.nama AS kepala_kk, t.nik FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id  order by (select sum(nilai*bobot) as x from analisis_keluarga a inner join sub_analisis_keluarga b on a.id_sub_analisis=b.id inner join master_analisis_keluarga c on b.id_master=c.id where a.id_kel=u.id  AND bulan='$_SESSION[bulan]' AND tahun='$_SESSION[tahun]') desc) as tb1 where 1 ";
		
		$sql .= $this->search_keluarga_sql();
		//$sql .= $this->rentang1_search();
		//$sql .= $this->rentang2_search();
		$sql .= $paging_sql;
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		$i=0;
		while($i<count($data)){//mendatar
			$sql1   = "SELECT * FROM master_analisis_keluarga WHERE aktif = 1 AND jenis=$_SESSION[jenis]";
			//$sql1 .= $this->jenis_sql();
			$query1 = $this->db->query($sql1);
			$data1 =  $query1->result_array();
			
			$j=0;
			while($j<count($data1)){ //mendatar
				$sql2   = "SELECT * FROM (SELECT s.nama as nama, s.nilai as nilai, m.bobot, (s.nilai*m.bobot)/100 as hasil, bulan, tahun FROM analisis_keluarga k LEFT JOIN sub_analisis_keluarga s ON k.id_sub_analisis = s.id LEFT JOIN master_analisis_keluarga m ON k.id_master=m.id WHERE k.id_master = ? AND k.id_kel = ? AND tahun='$_SESSION[tahun]') AS TB WHERE 1  ";
				//$sql2 .= $this->tahun_sql();
				//$sql2 .= $this->bulan_sql();
				$query2 = $this->db->query($sql2,array($data1[$j]['id'],$data[$i]['id']));
				$data2 =  $query2->row_array();
				$data[$i]['jawaban'][$j] = $data2;
				$thn=$data2['tahun'];
				$bln=$data2['bulan'];
				$j++;
			}
			$thn[$i]=$thn;
			$bln[$i]=$bln;
			$i++;
		}
		
		//Formating Output
		$i=0;
		$j=$offset;
		while($i<count($data)){
			$data[$i]['no']=$j+1;
			$i++;
			$j++;
		}
		return $data;
	
	}
	
	function list_analisis_keluargay($o=0,$offset=0,$limit=500){
		//menurun
		$paging_sql = ' LIMIT ' .$offset. ',' .$limit;
		$sql   = "select * from (SELECT (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id   AND tahun='$_SESSION[tahun]') as hasil,u.*,t.nama AS kepala_kk, t.nik FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id  order by (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id   AND tahun='$_SESSION[tahun]') desc) as tb1 where 1 ";
		
		$sql .= $this->search_keluarga_sql();
		$sql .= $paging_sql;
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		$i=0;
		while($i<count($data)){//mendatar
			$sql1   = "SELECT * FROM master_analisis_keluarga WHERE aktif = 1 and jenis=$_SESSION[jenis] ";
			//$sql1 .= $this->jenis_sql();
			$query1 = $this->db->query($sql1);
			$data1 =  $query1->result_array();
			
			$j=0;
			while($j<count($data1)){ //mendatar
				$sql2   = "SELECT * FROM (SELECT s.nama as nama, s.nilai as nilai, m.bobot, s.nilai as hasil,  tahun FROM analisis_keluarga k LEFT JOIN sub_analisis_keluarga s ON k.id_sub_analisis = s.id LEFT JOIN master_analisis_keluarga m ON k.id_master=m.id WHERE k.id_master = ? AND k.id_kel = ?   AND tahun='$_SESSION[tahun]') AS TB WHERE 1  ";
				//$sql2 .= $this->tahun_sql();
				//$sql2 .= $this->bulan_sql();
				$query2 = $this->db->query($sql2,array($data1[$j]['id'],$data[$i]['id']));
				$data2 =  $query2->row_array();
				$data[$i]['jawaban'][$j] = $data2;
				$thn=$data2['tahun'];
				$bln=$data2['bulan'];
				$j++;
			}
			$thn[$i]=$thn;
			$bln[$i]=$bln;
			$i++;
		}
		
		//Formating Output
		$i=0;
		$j=$offset;
		while($i<count($data)){
			$data[$i]['no']=$j+1;
			$i++;
			$j++;
		}
		return $data;
	
	}
	
	function list_rw($dusun=''){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt = '0' AND dusun = ? AND rw <> '0'";
		$query = $this->db->query($sql,$dusun);
		$data=$query->result_array();
		return $data;
	}
	
	function list_rw_all(){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt = '0' AND rw <> '0'";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		return $data;
	}
				
	function list_rt_all(){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt <> '0' AND rw <> '-'";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		return $data;
	}
	
	function list_rt($dusun='',$rw=''){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rw = ? AND dusun = ? AND rt <> '0'";
		$query = $this->db->query($sql,array($rw,$dusun));
		$data=$query->result_array();
		return $data;
	}
	
	function list_dusun(){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt = '0' AND rw = '0' ";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		return $data;
	}
	
	function list_data_rentang(){
		$order_sql=" order by dari ";

		$sql   = "SELECT r.*, (SELECT count(id_kel) from hasil_analisis_keluarga where hasil>r.dari-1 AND hasil<=r.sampai AND tahun=$_SESSION[tahun]) as jumlah FROM klasifikasi_analisis_keluarga r WHERE 1 "; 
		
		$sql .= $this->jenis_sql(); 
		$sql .= $order_sql; 
		$query = $this->db->query($sql);
		$data=$query->result_array();

		return $data;
	}
	
	function get_rentang($id=0){	
		$sql   = "SELECT * FROM klasifikasi_analisis_keluarga WHERE id= $id ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}
	
	function get_rentang_terakhir(){	
		$sql   = "SELECT (case when max(sampai) is null then '0' else (max(sampai)+1) end) as dari FROM klasifikasi_analisis_keluarga WHERE 1 ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}	
	
	function insert_rentang(){
		$data = $_POST;
		$data['jenis']=$_SESSION['jenis'];
		$outp = $this->db->insert('klasifikasi_analisis_keluarga',$data);
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
		
	function update_rentang($id=0){
		$data = $_POST;
		$sql   = "UPDATE klasifikasi_analisis_keluarga SET nama='$data[nama]', dari='$data[dari]', sampai='$data[sampai]' WHERE id='$id' ";
		$outp=$this->db->query($sql);
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function delete_rentang($id=0){
		$sql   = "DELETE FROM klasifikasi_analisis_keluarga WHERE id='$id' ";
		$outp=$this->db->query($sql);
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function delete_all_rentang(){
		$id_cb = $_POST['id_cb'];
		
		if(count($id_cb)){
			foreach($id_cb as $id){
				$sql  = "DELETE FROM klasifikasi_analisis_keluarga WHERE id=?";
				$outp = $this->db->query($sql,array($id));
			}
		}
		else $outp = false;
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function list_detail_rentang($id){

		$order_by=" ORDER BY a.hasil";
	
		$sql   = "SELECT k.*, p.nama as kepala_kk, (SELECT COUNT(id) FROM tweb_penduduk WHERE id_kk=a.id_kel) as jml_anggota, a.hasil as nilai, c.dusun, c.rw, c.rt 
				FROM hasil_analisis_keluarga a LEFT JOIN tweb_rtm k ON a.id_kel=k.id LEFT JOIN tweb_penduduk p ON k.nik_kepala=p.id LEFT JOIN tweb_wil_clusterdesa c ON p.id_cluster=c.id WHERE  hasil>=(select dari from klasifikasi_analisis_keluarga where id=$id) and hasil<=(select sampai from klasifikasi_analisis_keluarga where id=$id)  ";  
		
		$sql .= $thnsql;
		//$sql .= $blnsql;
		$sql .= $this->dusun_sql();
		$sql .= $this->rw_sql();
		$sql .= $this->rt_sql();
		$sql .= $order_by;

		
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		//Formating Output
		$i=0;
		$j=$offset;
		$x=$offset;
		while($i<count($data)){
			$data[$i]['no']=$j+1;
			
			$i++;
			$j++;
			
		}

		return $data;
	}
	
	function list_jenis(){
		$sql   = "SELECT * FROM jenis_analisis_keluarga WHERE 1 ";
		
		$query = $this->db->query($sql);
		$data  = $query->result_array();
		return $data;
	}
	function get_akp_daerah(){
		if(isset($_SESSION['kategori'])){
			$kf = $_SESSION['kategori'];
			$kw="(";
			$i=0;
			while($i<count($kf)){
				$kw .=$kf[$i].",";
				$i++;
			}
			$kw =$kw."7)";
			$kelas_sql= " AND id IN $kw";
			
			$sql   = "SELECT nama FROM klasifikasi_analisis_keluarga WHERE 1 $kelas_sql";
			$query = $this->db->query($sql);
			$data = $query->result_array();
			
			$i=0;
			$data2 = "";
			while($i<count($data)){
				$data2 .= " | <b>".$data[$i]['nama']."</b>";
				$i++;
			}
			return $data2;
		}
	}
	
	function get_akp_klaster(){
		if(isset($_SESSION['klaster'])){
			$kf = $_SESSION['klaster'];
			$kw="(";
			$i=0;
			while($i<count($kf)){
				$kw .=$kf[$i].",";
				$i++;
			}
			$kw =$kw."7)";
			$kelas_sql= " AND id IN $kw";
			
			$sql   = "SELECT nama FROM klasifikasi_analisis_keluarga WHERE 1 $kelas_sql";
			$query = $this->db->query($sql);
			$data = $query->result_array();
			
			$i=0;
			$data2 = "";
			while($i<count($data)){
				$data2 .= " | <b>".$data[$i]['nama']."</b>";
				$i++;
			}
			return $data2;
		}
	
	}
}

?>
