<?php
/*
 * Berkas default dari halaman web utk publik
 * 
 * Copyright 2013 
 * Rizka Himawan <himawan.rizka@gmail.com>
 * Muhammad Khollilurrohman <adsakle1@gmail.com>
 * Asep Nur Ajiyati <asepnurajiyati@gmail.com>
 *
 * SID adalah software tak berbayar (Opensource) yang boleh digunakan oleh siapa saja selama bukan untuk kepentingan profit atau komersial.
 * Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat ciptaan turunan bukan untuk kepentingan komersial
 * selama mereka mencantumkan asal pembuat kepada Anda dan melisensikan ciptaan turunan dengan syarat yang serupa dengan ciptaan asli.
 * Untuk mendapatkan SID RESMI, Anda diharuskan mengirimkan surat permohonan ataupun izin SID terlebih dahulu, 
 * aplikasi ini akan tetap bersifat opensource dan anda tidak dikenai biaya.
 * Bagaimana mendapatkan izin SID, ikuti link dibawah ini:
 * http://lumbungkomunitas.net/bergabung/pendaftaran/daftar-online/
 * Creative Commons Attribution-NonCommercial 3.0 Unported License
 * SID Opensource TIDAK BOLEH digunakan dengan tujuan profit atau segala usaha  yang bertujuan untuk mencari keuntungan. 
 * Pelanggaran HaKI (Hak Kekayaan Intelektual) merupakan tindakan  yang menghancurkan dan menghambat karya bangsa.
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sistem Administrasi dan Informasi Desa</title>
<link rel="shortcut icon" href="<?=base_url()?>favicon.ico" />
<link href="<?=base_url()?>assets/css/login.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/css/ui-buttons.css" rel="stylesheet" type="text/css" />

</head>
<body onload="document.getElementById('focus').focus();">	
<script type="text/javascript" src="<?=base_url()?>assets/flash/swfobject.js"></script>
<script type="text/javascript">
		var flashvars = {};
		flashvars.xml = "<?=base_url()?>assets/flash/config.xml";
		var attributes = {};
		attributes.wmode = "transparent";
		attributes.id = "slider";
		swfobject.embedSWF("<?=base_url()?>assets/flash/cu3er.swf", "slider", "378", "180", "9", "<?=base_url()?>assets/flash/expressInstall.swf", flashvars, attributes);
</script>
<div id="container">
    <div id="loginbox">
        <h4>Sistem Administrasi dan Informasi Desa</h4>
        <h5>Kab. <?=$desa['nama_kabupaten']?>, Kec. <?=$desa['nama_kecamatan']?>, Desa <?=$desa['nama_desa']?></h5>
        <form id="loginform" action="<?=site_url('siteman/auth')?>" method="post">
        <div id="header">
			<div id="slider"></div>
		</div>
        <div class="box">
		<table id="inputform">
            <tr>
				<td width="75">
					<label id="username-label">Username :</label>
				</td>
				<td>
					 <div class='loadingbox' id='username-check'><input type="text" name="username" class="inputbox" id="focus"/><div class="loadingbar"><span>Completed!..</span></div></div>
				</td>
			</tr>
            <tr>
				<td>
					<label id="password-label">Password :</label>
				</td>
            <td>
				<div class='loadingbox' id='password-check'><input type="password" name="password" class="inputbox"/><div class="loadingbar"><span>Completed!..</span></div></div>
			</td>
			</tr>
		</table>
            <hr />
			<?php if($_SESSION['siteman']==-1): ?>
				<div id='notification' class='failed'>
				  <p>
					<img src='<?=base_url()?>assets/images/icon/exclamation.png' alt=''/>Login Gagal, Username atau Password yang anda masukkan salah!..
				  </p>
				</div>
			<?endif?>
            <div style="text-align: right;">
                <div class="uibutton-group">
                    <button class="uibutton large" type="reset">Clear</button>
                    <button id="login-button" onclick="loginForm();return false" class="uibutton special large" type="submit">Login</button>
                </div>
            </div>
        </div>
        </form>
		<div id="slider">
			</div>
				<div id="footer">
					Powered by <a href="http://www.combine.or.id" target="_blank" style="color:#fff; text-decoration: none; font-weight: bold;">combine<span style="color: rgb(238, 102, 0);">.or.id</span></a>
				</div>
			</div>
		</div>
<script src="<?=base_url()?>assets/js/jquery-1.5.2.min.js"></script>
<script src="<?=base_url()?>assets/js/login.js"></script>
</body>
</html>
