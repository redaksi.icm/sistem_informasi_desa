<?php

class First_Keluarga_M extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	function list_raskin(){
		
		$dus = "";
		$rw = "";
		$rt = "";
		
		if(isset($_SESSION['dusun']))
			$dus = " AND c.dusun = '$_SESSION[dusun]'";
	
		if(isset($_SESSION['rw']))
			$rw = " AND c.rw = '$_SESSION[rw]'";
	
		if(isset($_SESSION['rt']))
			$rt = " AND c.rt = '$_SESSION[rt]'";
	
		$sql   = "SELECT s.*,
		(SELECT COUNT(h.id_kel) AS id FROM hasil_analisis_keluarga h LEFT JOIN tweb_keluarga u ON h.id_kel=u.id LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id LEFT JOIN tweb_wil_clusterdesa c ON t.id_cluster = c.id WHERE  h.id_klasifikasi = s.id AND tahun=(SELECT MAX(tahun) FROM hasil_analisis_keluarga) $dus $rw $rt) as jumlah,
		(SELECT COUNT(h.id_kel) AS id FROM hasil_analisis_keluarga h LEFT JOIN tweb_keluarga u ON h.id_kel=u.id LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id LEFT JOIN tweb_wil_clusterdesa c ON t.id_cluster = c.id WHERE  h.id_klasifikasi = s.id AND tahun=(SELECT MAX(tahun) FROM hasil_analisis_keluarga) $dus $rw $rt AND u.raskin = 1) as raskin,
		(SELECT COUNT(h.id_kel) AS id FROM hasil_analisis_keluarga h LEFT JOIN tweb_keluarga u ON h.id_kel=u.id LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id LEFT JOIN tweb_wil_clusterdesa c ON t.id_cluster = c.id WHERE  h.id_klasifikasi = s.id AND tahun=(SELECT MAX(tahun) FROM hasil_analisis_keluarga) $dus $rw $rt AND t.jamkesmas = 1) as jamkesmas FROM klasifikasi_analisis_keluarga s WHERE 1";
		
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}

