<script>
$(function(){ 
	var skpd_select_width = (parseInt($('#skpd_select').width())/2)-10;
	$('#skpd_select div').css('width',skpd_select_width);
	$('#skpd_select input:checked').parent().css({'background':'#c9cdff','border':'1px solid #7a82eb'});
	$('#skpd_select input').change(function(){
		if ($(this).is(':checked')){
			$(this).parent().css({'background':'#c9cdff','border':'1px solid #7a82eb'});
		} else {
			$(this).parent().css({'background':'#fafafa','border':'1px solid #ddd'});
		}
	});	
	
	var xskpd_select_width = (parseInt($('#xskpd_select').width())/2)-10;
	$('#xskpd_select div').css('width',xskpd_select_width);
	$('#xskpd_select input:checked').parent().css({'background':'#c9cdff','border':'1px solid #7a82eb'});
	$('#xskpd_select input').change(function(){
		if ($(this).is(':checked')){
			$(this).parent().css({'background':'#c9cdff','border':'1px solid #7a82eb'});
		} else {
			$(this).parent().css({'background':'#fafafa','border':'1px solid #ddd'});
		}
	});
	
	$('#skpd_select label').click(function(){
		$(this).prev().trigger('click');
	})
	 
});
</script>
<style>
#skpd_select div{
	vertical-align:top;
	margin:1px 0;
	padding:2px 1px 0px;
	background:#fafafa;
	border:1px solid #ddd;
}
#skpd_select input{
	vertical-align:middle;
	margin-right:2px;
}
#skpd_select label{
	font-size:11px;
	font-weight:normal;
}
</style><form method="post" action="<?php echo $form_action?>" >
<input type="hidden" name="rt" value="">
<table>
<?php $last="";foreach($list_jawab AS $data){?>
<?php if($data['pertanyaan']!=$last){?></td></tr><tr><td><label><?php echo $data['pertanyaan']?></label></td></tr>
<tr><td id="skpd_select">
<div style="display:inline-block;"><input type="checkbox" name="id_cb[]" value="<?php echo $data['id_jawaban']?>"<?php if($data['cek']){echo " checked";}?>><label><?php echo $data['jawaban']?></label></div>
<?php }else{?>
<div style="display:inline-block;"><input type="checkbox" name="id_cb[]" value="<?php echo $data['id_jawaban']?>"<?php if($data['cek']){echo " checked";}?>><label><?php echo $data['jawaban']?></label></div>
<?php }?>
<?php $last=$data['pertanyaan'];
}?>
</table>
<div class="buttonpane" style="text-align: right; width:580px;bottom:0px;">
    <div class="uibutton-group">
        <button class="uibutton" type="button" onclick="$('#multi').dialog('close');">Batal</button>
        <button class="uibutton confirm" type="submit">Lanjut</button>
    </div>
</div>
</form>
