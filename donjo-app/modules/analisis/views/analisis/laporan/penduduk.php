<script type="text/javascript" src="<?php echo base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	
		
		
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Laporan</h3>
</div>
<div id="contentpane" style="overflow:auto;">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel top">
        <div class="left">
            <div class="uibutton-group">
			<a href="<?php echo site_url("analisis/laporan_penduduk/cetak/$lap")?>" class="uibutton tipsy south" title="Cetak Data" target="_blank"><span class="icon-print icon-large">&nbsp;</span>Cetak Data</a>
			<a href="<?php echo site_url("analisis/laporan_penduduk/graph_laporan")?>" class="uibutton tipsy south" title="Grafik"><span class="icon-bar-chart icon-large">&nbsp;</span>Grafik Data</a>
			<a href="<?php echo site_url("analisis/laporan_penduduk/lap_detail_a")?>" class="uibutton tipsy south" title="Laporan"><span class="icon-th-list icon-large">&nbsp;</span>Persentase Jawaban</a>
			<a href="<?php echo site_url("analisis/laporan_penduduk/lap_detail_b")?>" class="uibutton tipsy south" title="Laporan"><span class="icon-th-list icon-large">&nbsp;</span>Nilai Jawaban</a>
            </div>
        </div>
    </div>
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <div class="table-panel top">
            <div class="left">
            


<select name="tahun" onchange="formAction('mainform','<?php echo site_url('analisis/laporan_penduduk/tahun')?>')">';   
<option value="0"> -- Pilih Tahun -- </option>
<?php $i=2010;?>
<?php while($i++<2020){?>
<option value="<?php echo $i?>" <?php if($i==$tahun){?>selected<?php }?>><?php echo $i?></option>
<?php }?>
</select>      


<select name="bulan" onchange="formAction('mainform','<?php echo site_url('analisis/laporan_penduduk/bulan')?>')">';   
<option value="0"> -- Pilih Bulan -- </option>
<?php $i=0;?>
<?php while($i++<12){?>
<option value="<?php echo $i?>" <?php if($i==$bulan){?>selected<?php }?>><?php echo $i?></option>
<?php }?>
</select>   

            </div>
            <div class="right">
            </div>
        </div>
        <table class="list">
		<thead>
            <tr>
                <th>No</th>
				<th align="left" align="center">Statistik</th>
				<th align="left" align="center">Jawaban</th>
				<th align="left" align="center">Jumlah responden</th>
				<th align="left" align="center" width="60">Laki-laki</th>
				<th align="left" align="center" width="60">Perempuan</th>

			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
          <td align="center" width="2"><?php echo $data['no']?></td>
         <td ><a href="<?php echo site_url("analisis/laporan_penduduk/tanya/$data[id_master]")?>"><?php echo $data['tanya']?></a></td>
          <td><a href="<?php echo site_url("analisis/laporan_penduduk/jawaban/$data[id_master]/$data[id_sub]")?>"><?php echo $data['jawaban']?></a></td>
          <td><a href="<?php echo site_url("analisis/laporan_penduduk/detail/$data[id_master]/$data[id_sub]/$data[tahun]/$data[bulan]/0")?>"><?php echo $data['jml_responden']?></a></td>
	  <td><a href="<?php echo site_url("analisis/laporan_penduduk/detail/$data[id_master]/$data[id_sub]/$data[tahun]/$data[bulan]/1") ?>"><?php  if($data['laki']<1){ echo "0";}else{?><?php echo $data['laki'];}?></a></td>
          <td><a href="<?php echo site_url("analisis/laporan_penduduk/detail/$data[id_master]/$data[id_sub]/$data[tahun]/$data[bulan]/2")?>"><?php  if($data['perempuan']<1){ echo "0";}else{?><?php echo $data['perempuan'];}?></a></td>

		  </tr>
        <?php  endforeach; ?>
		</tbody>
        </table>
    </div>
	</form>
    <div class="ui-layout-south panel bottom">
        <div class="left"> 
		<div class="table-info">
          <form id="paging" action="<?php echo site_url("analisis/laporan_penduduk/index/$lap/")?>" method="post">
		  <label>Tampilkan</label>
            <select name="per_page" onchange="$('#paging').submit()" >
              <option value="20" <?php  selected($per_page,20); ?> >20</option>
              <option value="50" <?php  selected($per_page,50); ?> >50</option>
              <option value="100" <?php  selected($per_page,100); ?> >100</option>
            </select>
            <label>Dari</label>
            <label><strong><?php echo $paging->num_rows?></strong></label>
            <label>Total Data</label>
          </form>
          </div>
        </div>
        <div class="right">
            <div class="uibutton-group">
            <?php  if($paging->start_link): ?>
				<a href="<?php echo site_url("analisis/laporan_penduduk/index/$lap/$paging->start_link/$o")?>" class="uibutton"  >First</a>
			<?php  endif; ?>
			<?php  if($paging->prev): ?>
				<a href="<?php echo site_url("analisis/laporan_penduduk/index/$lap/$paging->prev/$o")?>" class="uibutton"  >Prev</a>
			<?php  endif; ?>
            </div>
            <div class="uibutton-group">
                
				<?php  for($i=$paging->start_link;$i<=$paging->end_link;$i++): ?>
				<a href="<?php echo site_url("analisis/laporan_penduduk/index/$lap/$i/$o")?>" <?php  jecho($p,$i,"class='uibutton special'")?> class="uibutton"><?php echo $i?></a>
				<?php  endfor; ?>
            </div>
            <div class="uibutton-group">
			<?php  if($paging->next): ?>
				<a href="<?php echo site_url("analisis/laporan_penduduk/index/$lap/$paging->next/$o")?>" class="uibutton">Next</a>
			<?php  endif; ?>
			<?php  if($paging->end_link): ?>
                <a href="<?php echo site_url("analisis/laporan_penduduk/index/$lap/$paging->end_link/$o")?>" class="uibutton">Last</a>
			<?php  endif; ?>
            </div>
        </div>
    </div>
</div>
</td></tr></table>
</div>
