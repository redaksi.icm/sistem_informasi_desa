<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/validasi.js"></script>
<script>
$(function(){
    var nik = {};
    nik.results = [
<?php foreach($penduduk as $data){?>
	   {id:'<?php echo $data['id']?>',name:"<?php echo $data['no_kk']." - ".$data['nama']?>",info:"<?php foreach($data['alamat'] AS $anggota){echo '<p>['.$anggota['nik'].'] '.$anggota['nama'];if($anggota['kk_level']==1){echo " <label>Kepala Keluarga</label></p>";}else{echo "</p>";}}?>"},
<?php }?>
    ];
nik.total = nik.results.length;

$('#no_kk').flexbox(nik, {
	resultTemplate: '<div><label>NO. KK : {name}</label></div><div>Daftar Anggota</div><div>{info}</div>',
	watermark: 'Ketik nama / nik di sini..',
    width: 400,
    noResultsText :'Tidak ada nama / no. kk yang sesuai..',
});
});
</script>
<form action="<?php echo $form_action?>" method="post" id="validasi">
<table class="list">
<tr>
<th align="left">Nomor KK</th>
	<td>
		<div id="no_kk" name="no_kk" style="float:right;" class="required"></div>
	</td>
</tr>

<tr>
<td align="left" colspan="2">&nbsp;</td>
</tr>
<tr>
<td align="left" colspan="2">Keterangan</td>
</tr>
	<td colspan="2">
		Silahkan masukan pencarian nama kepala keluarga/ No. KK dari data keluarga yang sudah terinput.</br>
		Seluruh anggota keluarga termasuk kepala keluarga yang belum terdaftar di rumah tangga lain otomatis akan menjadi anggota dalam rumah tangga terkait.
	</td>
</tr>
</table>

<div class="buttonpane">
    <div class="uibutton-group">
        <button class="uibutton" type="button" onclick="$('#window').dialog('close');">Tutup</button>
        <button class="uibutton confirm" type="submit">Simpan</button>
    </div>
</div>
</form>
