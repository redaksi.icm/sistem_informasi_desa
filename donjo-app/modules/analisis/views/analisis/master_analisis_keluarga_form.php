<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">

		<fieldset>
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li class="selected" ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset>
			<div  id="sidecontent3" class="lmenu">
				<ul>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
		</fieldset>
	</td>
	<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Form Master Analisis</h3>
</div>
<div id="contentpane">
    <form id="validasi" action="<?php echo $form_action?>" method="POST" id="mainform" name="mainform">
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <table class="form">
            <tr>
                <th>Kode</th>
                <?php  if($master_analisis_keluarga['id']){?>
                        <td><input name="id" type="hidden" class="inputbox required" size="10" value="<?php echo $master_analisis_keluarga['id']?>"/><?php echo $master_analisis_keluarga['id']?></td>
                <?php }else{?>
                        <td><input name="id" type="text" class="inputbox required" size="10" /></td>
                 <?php }?>
            </tr>
            <tr>
                <th>Nama Analisis</th>
                <td><input name="nama" type="text" class="inputbox required" size="60" value="<?php echo $master_analisis_keluarga['nama']?>"/></td>
            </tr>     
            
            <tr>
                <th>Bobot</th>
                <td><input name="bobot" type="text" class="inputbox required" size="10" value="<?php echo $master_analisis_keluarga['bobot']?>"/></td>
            </tr> 
            
        </table>
    </div>
    <div class="ui-layout-south panel bottom">
        <div class="left">     
            <a href="<?php echo site_url()?>analisis/master_keluarga" class="uibutton icon prev">Kembali</a>
        </div>
        <div class="right">
            <div class="uibutton-group">
                <button class="uibutton" type="reset">Clear</button>
                <button class="uibutton confirm" type="submit" >Simpan</button>
            </div>
        </div>
    </div> </form>
</div>
</td></tr></table>
</div>
