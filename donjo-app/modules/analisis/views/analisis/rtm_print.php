<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Data Rumah Tangga</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url()?>assets/css/report.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">

<!-- Print Body -->
<div id="body">
<div class="header" align="center">
<h3> Data Analisis Rumah Tangga </h3>
</div>
<br>
    <table class="border thick">
	<thead>
		<tr class="border thick">
			<th>No</th>
			<th align="left" align="center" width="78">No. Rumah Tangga</th>
			<th align="left" align="center" >Kepala Rumah Tangga</th>
			<th align="left" align="center" width="50">Jumlah Anggota</th>
			<th align="left" align="center" >Alamat</th>
			<th align="left" align="center" width="30">Indikator Daerah</th>
			<th align="left" align="center" width="100">Klasifikasi Daerah</th>
			<th align="left" align="center" width="30">Indikator Klaster</th>
			<th align="left" align="center" width="100">Klasifikasi Klaster</th>
		</tr>
		</thead>
		
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
			<td  width="2"><?php echo $data['no']?></td>
			<td><?php echo $data['no_kk']?></td>
			<td><?php echo $data['kepala_kk']?></td>
			<td><?php echo $data['anggota'] ;?></td>
			<td><?php echo $data['alamat']?></td>
			<td><?php echo $data['hasil'] ;?></td>
			<td><?php echo $data['analisis'] ;?></td>
			<td><?php echo $data['hasil2'] ;?></td>
			<td><?php echo $data['analisis2'] ;?></td>
		</tr>
		<?php  endforeach; ?>
	</tbody>
	
</table>
</div>
   
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>

</body></html>
