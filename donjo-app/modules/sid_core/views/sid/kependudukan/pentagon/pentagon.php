<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/highcharts2.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/highcharts-more.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/exporting.js"></script>

<script>
$(function () {

	window.chart = new Highcharts.Chart({
	            
	    chart: {
	        renderTo: 'container',
	        polar: true,
	        type: 'line'
	    },
	    
	    title: {
	        text: 'Pentagon Asset',
	        x: -80
	    },
	    
	    pane: {
	    	size: '60%'
	    },
	    
	    xAxis: {
	        categories: [ 'SDA', 'SDM', 'Keuangan', 
	                'infra struktur', 'Sosial'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	        
	    yAxis: {
	        gridLineInterpolation: 'polygon',
	        lineWidth: 0,
	        min: 0
	    },
	    
	    tooltip: {
	    	shared: true,
	        valuePrefix: '$'
	    },
	    
	    legend: {
	        align: 'right',
	        verticalAlign: 'top',
	        y: 10,
	        layout: 'vertical'
	    },
	    
	    series: [{
	        name: 'Allocated Budget',
	        data: [ 3, 3, 4, 10, 7],
	        pointPlacement: 'on'
	    }]
	
	});
});
</script>
<div style="width:420px;height:355px;" id="container"></div>