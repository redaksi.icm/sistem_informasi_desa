<?php 

class import_Model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	function import_excel(){

$data = new Spreadsheet_Excel_Reader($_FILES['userfile']['tmp_name']);

// membaca jumlah baris dari data excel
$baris = $data->rowcount($sheet_index=0);

// nilai awal counter untuk jumlah data yang sukses dan yang gagal diimport
$sukses = 0;
$gagal = 0;


//buat tabel impor
$a="CREATE TABLE IF NOT EXISTS impor ( 
id int(11) NOT NULL AUTO_INCREMENT,  
dusun varchar(50) NOT NULL DEFAULT 0,
rw varchar(10) NOT NULL DEFAULT 0, 
rt varchar(10) NOT NULL DEFAULT 0,  
nama varchar(100) NOT NULL,  
nik varchar(16) NOT NULL,   
sex tinyint(4) unsigned DEFAULT NULL,  
tempatlahir varchar(100) NOT NULL,
tanggallahir date NOT NULL,  
agama_id int(10) unsigned NOT NULL,  
pendidikan_kk_id int(10) unsigned NOT NULL,
pendidikan_id int(10) unsigned NOT NULL,
pendidikan_sedang_id int(10) unsigned NOT NULL,
pekerjaan_id int(10) unsigned NOT NULL,  
status_kawin tinyint(4) unsigned NOT NULL,  
kk_level tinyint(2) NOT NULL DEFAULT 0,
warganegara_id int(10) unsigned NOT NULL,  
nama_ayah varchar(100) NOT NULL,  
nama_ibu varchar(100) NOT NULL,
golongan_darah_id int(11) NOT NULL,  
jamkesmas varchar(160) NOT NULL DEFAULT 2, 
id_kk varchar(200) NOT NULL DEFAULT '0',  
PRIMARY KEY (id),  
KEY FK_tweb_kawin (status_kawin),
KEY FK_tweb_pekerjaan (pekerjaan_id),  
KEY FK_tweb_pendidikan_kk (pendidikan_kk_id),
KEY FK_tweb_pendidikan (pendidikan_id),  
KEY FK_tweb_sex (sex),
KEY FK_tweb_warganegara (warganegara_id),  
KEY FK_tweb_agama (agama_id),  
KEY pend_nik (nik)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;";
$b = mysql_query($a);

$a="DELETE FROM impor";
$b = mysql_query($a);

$a="TRUNCATE tweb_wil_clusterdesa";
$b = mysql_query($a);

$a="TRUNCATE tweb_keluarga";
$b = mysql_query($a);

$a="TRUNCATE tweb_penduduk";
$b = mysql_query($a);

// import data excel mulai baris ke-2 (karena baris pertama adalah nama kolom)
$baris2 ="";
for ($i=2; $i<=$baris; $i++){
	 // membaca data nim (kolom ke-n)

	$dusun = $data->val($i, 1);
	$rw = $data->val($i, 2);
	$rt = $data->val($i, 3);
	$nama = $data->val($i, 4);
	$nama = str_replace("'","`",$nama);
	$id_kk= $data->val($i, 5);
	$nik = $data->val($i, 6);
	$sex = $data->val($i, 7);
	$tempatlahir= $data->val($i, 8);
	$tanggallahir= $data->val($i, 9);
	$agama_id= $data->val($i, 10);
	$pendidikan_kk_id= $data->val($i, 11);
	$pendidikan_id= $data->val($i, 12);
	$pendidikan_sedang_id= $data->val($i, 13);
	$pekerjaan_id= $data->val($i, 14);
	$status_kawin= $data->val($i, 15);
	$kk_level= $data->val($i, 16);
	$warganegara_id= $data->val($i, 17);
	$nama_ayah= $data->val($i, 18);
	$nama_ayah = str_replace("'","`",$nama_ayah);
	$nama_ibu= $data->val($i, 19);
	$nama_ibu = str_replace("'","`",$nama_ibu);
	$golongan_darah_id= $data->val($i, 20);
	$jamkesmas= $data->val($i, 21);
	

	 // masukin ke tabel impor
	$query="INSERT INTO 
impor(dusun, rw,rt, nama, nik, sex, tempatlahir, tanggallahir, agama_id, pendidikan_kk_id, pendidikan_id, pendidikan_sedang_id, pekerjaan_id, status_kawin, kk_level, warganegara_id, nama_ayah, nama_ibu, golongan_darah_id, jamkesmas,id_kk)VALUES
			('$dusun','$rw','$rt','$nama','$nik' ,'$sex','$tempatlahir','$tanggallahir','$agama_id','$pendidikan_kk_id','$pendidikan_id','$pendidikan_sedang_id','$pekerjaan_id','$status_kawin',
'$kk_level','$warganegara_id','$nama_ayah','$nama_ibu','$golongan_darah_id','$jamkesmas','$id_kk')";
$hasil = mysql_query($query);

	  if($hasil){
		$sukses++;
	  }else{
		$gagal++;
		$baris2 .=$i.",";
	  }
}

if($gagal==0)
	$baris2 ="tidak ada data yang gagal di import.";
	
// masukin ke tabel tweb_wil_clusterdesa
	$query="INSERT INTO tweb_wil_clusterdesa(rt,rw,dusun) select * from (SELECT rt, rw, dusun from impor GROUP BY rt,rw,dusun
			union SELECT '0' as rt, '0' as rw, dusun from impor GROUP BY dusun
			union SELECT '0' as rt, '-' as rw, dusun from impor GROUP BY dusun
			union SELECT '-' as rt, '-' as rw, dusun from impor GROUP BY dusun
			union SELECT '-' as rt, rw as rw, dusun from impor GROUP BY dusun,rw
			union SELECT '0' as rt, rw as rw, dusun from impor GROUP BY dusun,rw) as tb";
	$hasil = mysql_query($query);

// masukin ke tabel tweb_penduduk
	$query="INSERT INTO tweb_keluarga(no_kk) SELECT * FROM (SELECT a.id_kk FROM impor a GROUP BY id_kk) as tb";
	$hasil = mysql_query($query);

// masukin ke tabel tweb_penduduk
$query="INSERT INTO tweb_penduduk(nama, nik, id_kk, kk_level, sex, tempatlahir, tanggallahir, agama_id, pendidikan_kk_id, pendidikan_id, pendidikan_sedang_id, pekerjaan_id, status_kawin, warganegara_id, nama_ayah, nama_ibu, golongan_darah_id, jamkesmas, id_cluster,status) SELECT * FROM (SELECT nama, nik, (SELECT id FROM tweb_keluarga WHERE no_kk=a.id_kk) as id_kk, kk_level, sex, tempatlahir, tanggallahir, agama_id, pendidikan_kk_id, pendidikan_id, pendidikan_sedang_id, pekerjaan_id, status_kawin, warganegara_id, nama_ayah, nama_ibu, golongan_darah_id, jamkesmas, (SELECT id FROM tweb_wil_clusterdesa where dusun=a.dusun AND rw=a.rw AND rt=a.rt) as id_cluster,'1' as status from impor a) as tb";
	$hasil = mysql_query($query);

// masukin ke tabel tweb_penduduk
	$sql="SELECT id FROM tweb_keluarga";
	if ($a=mysql_query($sql)){
			while ($hsl=mysql_fetch_array($a)){
				$idnya=($hsl['id']);
				$kirim = "UPDATE tweb_keluarga SET nik_kepala=(SELECT id FROM tweb_penduduk where kk_level='1' AND id_kk=$idnya) WHERE id=$idnya";
				$query=mysql_query($kirim);
			}
		}

$a="DROP TABLE impor";
$b = mysql_query($a);

$a="DELETE FROM tweb_wil_clusterdesa WHERE rt = '' AND rw = '' AND dusun = '';";
$b = mysql_query($a);

$a="DELETE FROM tweb_keluarga WHERE no_kk = '' AND nik_kepala = '';";
$b = mysql_query($a);

$a="DELETE FROM  tweb_penduduk WHERE nama = '' AND nik = '';";
$b = mysql_query($a);


	}
	
	function import_dasar(){

		$data = "";
		$in = "";
		$outp = "";
		$filename = $_FILES['userfile']['tmp_name'];
		if ($filename!=''){	
			$lines = file($filename);
			foreach ($lines as $line){$data .= $line;}
			$penduduk=Parse_Data($data,"<penduduk>","</penduduk>");
			$keluarga=Parse_Data($data,"<keluarga>","</keluarga>");
			$cluster=Parse_Data($data,"<cluster>","</cluster>");
			//echo $cluster;
			$penduduk=explode("\r\n",$penduduk);
			$keluarga=explode("\r\n",$keluarga);
			$cluster=explode("\r\n",$cluster);
			
			$inset = "INSERT INTO tweb_penduduk VALUES ";
			for($a=1;$a<(count($penduduk)-1);$a++){
				$p = preg_split("/\+/", $penduduk[$a]);
				$in .= "(";
				for($j=0;$j<(count($p));$j++){
					$in .= ',"'.$p[$j].'"';
				}
				$in .= "),";
			}
			$x = strlen($in);
			$in[$x-1] =";";
			$outp = mysql_query($inset.$in);
			//echo $inset.$in;
			
			$in = "";
			$inset = "INSERT INTO tweb_wil_clusterdesa VALUES ";
			for($a=1;$a<(count($cluster)-1);$a++){
				$p = preg_split("/\+/", $cluster[$a]);
				$in .= "(";
				for($j=0;$j<(count($p));$j++){
					$in .= ',"'.$p[$j].'"';
				}
				$in .= "),";
			}
			$x = strlen($in);
			$in[$x-1] =";";
			$outp = mysql_query($inset.$in);
			
			$in = "";
			$inset = "INSERT INTO tweb_keluarga VALUES ";
			for($a=1;$a<(count($keluarga)-1);$a++){
				$p = preg_split("/\+/", $keluarga[$a]);
				$in .= "(";
				for($j=0;$j<(count($p));$j++){
					$in .= ',"'.$p[$j].'"';
				}
				$in .= "),";
			}
			$x = strlen($in);
			$in[$x-1] =";";
			$outp = mysql_query($inset.$in);
		}
		if($outp) $_SESSION['success']=1;
		else $_SESSION['success']=-1;
	}
	
	function import_akp(){
		$id_desa = $_SESSION['user'];
		$data = "";
		$in = "";
		$outp = "";
		$filename = $_FILES['userfile']['tmp_name'];
		if ($filename!=''){	
			$lines = file($filename);
			foreach ($lines as $line){$data .= $line;}
			$penduduk=Parse_Data($data,"<akpkeluarga>","</akpkeluarga>");
			//echo $cluster;
			$penduduk=explode("\r\n",$penduduk);
			
			$inset = "INSERT INTO analisis_keluarga VALUES ";
			for($a=1;$a<(count($penduduk)-1);$a++){
				$p = preg_split("/\+/", $penduduk[$a]);
				$in .= "(".$id_desa;
				for($j=0;$j<(count($p));$j++){
					$in .= ',"'.$p[$j].'"';
				}
				$in .= "),";
			}
			$x = strlen($in);
			$in[$x-1] =";";
			$outp = mysql_query($inset.$in);
			
		}
		if($outp) $_SESSION['success']=1;
		else $_SESSION['success']=-1;
	}
}

?>
