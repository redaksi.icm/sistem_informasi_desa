<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>SID Sistem Informasi Desa 3.04</title>
<link rel="shortcut icon" href="<?=base_url()?>favicon.ico" />
<link href="<?=base_url()?>assets/css/login.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/css/ui-buttons.css" rel="stylesheet" type="text/css" />

<!--[if lte IE 6]>
<style type="text/css">
img, div,span,a,button { behavior: url(assets/js/plugins/iepngfix.htc) }
</style> 
<![endif]-->
</head>
<body onload="document.getElementById('focus').focus();">	
<div id="container">
    <div id="loginbox">
        <a href="<?=site_url()?>">
        <h4>SID Sistem Informasi Desa</h4>
        <h5>Kab. <?=unpenetration($desa['nama_kabupaten'])?>, Kec. <?=unpenetration($desa['nama_kecamatan'])?>, Desa <?=unpenetration($desa['nama_desa'])?></h5>
		</a>
        <form id="loginform" action="<?=site_url('siteman/auth')?>" method="post">
        <div id="header">
			<div id="slider"></div>
		</div>
        <div class="box">
		<table id="inputform">
            <tr>
				<td width="75">
					<label id="username-label">Username :</label>
				</td>
				<td>
					 <div class='loadingbox' id='username-check'><input type="text" name="username" class="inputbox" id="focus"/><div class="loadingbar"><span>Completed!..</span></div></div>
				</td>
			</tr>
            <tr>
				<td>
					<label id="password-label">Password :</label>
				</td>
            <td>
				<div class='loadingbox' id='password-check'><input type="password" name="password" class="inputbox"/><div class="loadingbar"><span>Completed!..</span></div></div>
			</td>
			</tr>
		</table>
            <hr />
			<?php if($_SESSION['siteman']==-1){ ?>
				<div id='notification' class='failed'>
				  <p>
					<img src='<?=base_url()?>assets/images/icon/exclamation.png' alt=''/>Login Gagal, Username atau Password yang anda masukkan salah!..
				  </p>
				</div>
			<?}?>
            <div style="text-align: right;">
                <div class="uibutton-group">
                    <button class="uibutton large" type="reset">Clear</button>
                    <button id="login-button" onclick="loginForm();return false" class="uibutton special large" type="submit">Login</button>
                </div>
            </div>
        </div>
        </form>
        <div id="footer">
            SID v3.04 - Powered by <a href="#" target="_blank" style="color: rgb(32,140,0); text-decoration: none; font-weight: bold;"> combine<span style="color: rgb(200, 102, 40);">.or.id</span></a>
		</div>
    </div>
</div>
<script src="<?=base_url()?>assets/js/jquery-1.5.2.min.js"></script>
<?php if($_SESSION['siteman']==-1){ ?>
<script src="<?=base_url()?>assets/js/jquery-ui-1.8.16.custom.min.js"></script>
<?}?>
<script src="<?=base_url()?>assets/js/login.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/flash/swfobject.js"></script>

<script type="text/javascript">
	$(function(){
	<?php if($_SESSION['siteman']==-1){ ?>
		$("#loginbox").removeAttr("style").hide().fadeIn();
		$("#notification").delay(7000).effect('puff',700);
	<?unset($_SESSION['siteman']);}?>
	});
	var flashvars = {};
	flashvars.xml = "<?=site_url()?>siteman/flash";
	var attributes = {};
	attributes.wmode = "transparent";
	attributes.id = "slider";
	swfobject.embedSWF("<?=base_url()?>assets/flash/cu3er.swf", "slider", "378", "180", "9", "<?=base_url()?>assets/flash/expressInstall.swf", flashvars, attributes);
	
	function modal(id,title,message,width,height){
	  if (width==null || height==null){
		width='500';
		height='auto';
	  }
	  $('#'+id+'').remove();
	  $('body').append('<div id="'+id+'" title="'+title+'" style="display:none;">'+message+'</div>');
			$('#'+id+'').dialog({
				resizable: false,
				draggable: true,
		  width:width,
		  height:height,
		  autoOpen: false,
				modal: false,
		  dragStart: function(event, ui) { 
			$(this).parent().addClass('drag');
		  },
		  dragStop: function(event, ui) { 
			$(this).parent().removeClass('drag');
		  },buttons: {
					"OK": function() {
						$('#'+id+'').remove();
						$( this ).dialog( "close" );
				}
			}
		});
	  $('#'+id+'').dialog('open');
	  }
</script>
</body>
</html>