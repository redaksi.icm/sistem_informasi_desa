<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">

	
<td style="background:#fff;padding:0px;"> 
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/highcharts.js"></script>
		<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'line'
            },
            title: {
                text: 'Analisis Penduduk Desa <?php  echo $config[nama_desa]?> Tahun <?php  echo $_SESSION[tahun]?>'
            },
            subtitle: {
                text: 'Kategori : <?php  echo $tanya[nama]?>'
            },
            xAxis: {
               categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
            },
            yAxis: {
                title: {
                    text: 'Jumlah Responden'
                }
            },
            tooltip: {
                enabled: false,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'°C';
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [<?php  foreach($grafik AS $data){?>
            {
                name: '<?php echo $data['jawaban']?>',
                data: [<?php echo $data['jan']?>,<?php echo $data['feb']?>,<?php echo $data['mar']?>,<?php echo $data['apr']?>,<?php echo $data['mei']?>,<?php echo $data['jun']?>,<?php echo $data['jul']?>,<?php echo $data['ags']?>,<?php echo $data['sep']?>,<?php echo $data['okt']?>,<?php echo $data['nov']?>,<?php echo $data['des']?>]
            }, 
               <?php }?>
            
]
        });
    });
    
});
		</script>

<div class="content-header">
    <h3>Data Keluarga</h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
            <div class="uibutton-group">
				
				
            </div>
        </div>
        <div class="right">
            <div class="uibutton-group">
<a href="<?php echo $form_action_kembali?>" class="uibutton icon prev">Kembali</a>
            </div>
        </div>
    </div>
    <div class="ui-layout-center" id="chart" style="padding: 5px;">    
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
       </div>
    <div class="ui-layout-center" id="chart" style="padding: 5px;">                
         </div>
    <div class="ui-layout-south panel bottom" style="max-height: 150px;overflow:auto;">
    
        
    </div>
	</form>
</div>
</td></tr></table>
</div>
