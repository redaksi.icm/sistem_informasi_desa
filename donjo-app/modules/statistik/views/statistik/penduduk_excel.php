<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Statistik_penduduk.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Laporan Statistik</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?=base_url()?>assets/css/report.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">

<!-- Print Body -->
<div id="body">

<table>
    <tbody>
    <tr>
        <td style="padding: 5px 20px;">
           
		<br>
		<table class="border thick data">
		<thead>
            <tr class="thick">
                <th class="thick">No</th>
				<th class="thick">Statistik</th>
				<th class="thick">Jumlah</th>
				<th class="thick" width="60">Laki-laki</th>
				<th class="thick" width="60">Perempuan</th>
			</tr>
		</thead>
		<tbody>
        <? foreach($main as $data): ?>
		<tr>
          <td class="thick" align="center" width="2"><?=$data['no']?></td>
          <td class="thick"><?=$data['nama']?></td>
          <td class="thick"><?=$data['jumlah']?></td>
		  <td class="thick"><?=$data['laki']?></td>
          <td class="thick"><?=$data['perempuan']?></td>
		  </tr>
        <? endforeach; ?>
		</tbody>
        </table>
		
            
            <br>
            
                        
        </td>
    </tr>
</tbody></table>        
</div>
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>

</body></html>
