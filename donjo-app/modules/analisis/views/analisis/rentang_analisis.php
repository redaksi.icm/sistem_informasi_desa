<script>
	$(function() {
		var keyword = <?php echo $keyword?> ;
		$( "#cari" ).autocomplete({
			source: keyword
		});
	});
</script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
			<td class="side-menu">
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li  class="selected"><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
	</td>
		</td>
		<td style="background:#fff;padding:5px;"> 
<div class="content-header">
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
			<h3>Rentang Analisis</h3>
        </div>
		
    </div>
<div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <div class="table-panel top">
            </div>
           
        <table class="list">
		<thead>
		    	<tr>
				<th width="10">No</th>
				<th width="10"><input type="checkbox" class="checkall"/></th>
				<th width="130">Nama Rentang Analisis</th>
		    	<th width="90">Rentang Daerah/Kabupaten</th>	
		    	<th width="90">Kluster <?php echo $kluster['nama']?></th>
		    	<th></th>	
		   	 </tr>
		</thead>
		<tbody>
        		<?php  $no=1; foreach($main as $data): ?>
			<tr>
		  		<td align="center" width="2"><?php echo $no?></td>
				<td align="center" width="5">
					<input type="checkbox" name="id_cb[]" value="<?php echo $data['id']?>" />
				</td>
				 <td><?php echo $data['nama']?></td>
				 <td align="right"><?php echo $data['dari']?> - <?php echo $data['sampai']?> </td>
				 <td align="right"><?php echo $data['darix']?> - <?php echo $data['sampaix']?> </td>
				 <td></td>
			</tr>
      			<?php  $no++; endforeach; ?>
		</tbody>
        </table>


	</form>

</div>
</td></tr></table>
</div>
