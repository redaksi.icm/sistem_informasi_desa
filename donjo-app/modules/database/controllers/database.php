<?php 
/*
 * Berkas default dari halaman web utk publik
 * 
 * Copyright 2013 
 * Rizka Himawan <himawan.rizka@gmail.com>
 * Muhammad Khollilurrohman <adsakle1@gmail.com>
 * Asep Nur Ajiyati <asepnurajiyati@gmail.com>
 *
 * SID adalah software tak berbayar (Opensource) yang boleh digunakan oleh siapa saja selama bukan untuk kepentingan profit atau komersial.
 * Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat ciptaan turunan bukan untuk kepentingan komersial
 * selama mereka mencantumkan asal pembuat kepada Anda dan melisensikan ciptaan turunan dengan syarat yang serupa dengan ciptaan asli.
 * Untuk mendapatkan SID RESMI, Anda diharuskan mengirimkan surat permohonan ataupun izin SID terlebih dahulu, 
 * aplikasi ini akan tetap bersifat opensource dan anda tidak dikenai biaya.
 * Bagaimana mendapatkan izin SID, ikuti link dibawah ini:
 * http://lumbungkomunitas.net/bergabung/pendaftaran/daftar-online/
 * Creative Commons Attribution-NonCommercial 3.0 Unported License
 * SID Opensource TIDAK BOLEH digunakan dengan tujuan profit atau segala usaha  yang bertujuan untuk mencari keuntungan. 
 * Pelanggaran HaKI (Hak Kekayaan Intelektual) merupakan tindakan  yang menghancurkan dan menghambat karya bangsa.
 */
?>

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database extends CI_Controller{

	function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('user_model');
		$this->load->dbforge();
		//$this->load->model('wilayah_model');
		$grup	= $this->user_model->sesi_grup($_SESSION['sesi']);
		if($grup!=1) redirect('siteman');
		$this->load->model('header_model');
		$this->load->model('import_model');
		$this->load->model('export_model');
		
	}
	
	function clear(){
		unset($_SESSION['cari']);
		unset($_SESSION['filter']);
		redirect('export');
	}
	
	function index(){
	
		$nav['act']= 1;
		$header = $this->header_model->get_data();
		$this->load->view('header', $header);
		$this->load->view('export/menu');
		$this->load->view('nav',$nav);
		$data['form_action'] = site_url("database/export_akp");
		$this->load->view('export/exp',$data);
		$this->load->view('footer');
	}
	
	function import(){
	
		$nav['act']= 2;
		$data['form_action'] = site_url("database/import_dasar");
		$header = $this->header_model->get_data();
		$this->load->view('header', $header);
		$this->load->view('export/menu');
		$this->load->view('nav',$nav);
		$this->load->view('import/imp',$data);
		$this->load->view('footer');
	}
	
	
	function export_dasar(){
		$this->export_model->export_dasar();
	}
	
	function export_akp(){
		$this->export_model->export_akp();
		//redirect('database');
	}
	
	function import2(){
		$nav['act']= 2;
		$data['form_action'] = site_url("database/import_dasar");
		$data['form_action2'] = site_url("database/import_akp");
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('export/menu');
		$this->load->view('export/nav',$nav);
		$this->load->view('export/imp',$data);
		$this->load->view('footer');
		
	}


	function pre_migrate(){
		$nav['act']= 3;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('export/menu');
		$this->load->view('export/nav',$nav);
		$this->load->view('export/mig');
		$this->load->view('footer');
	}
	
	function migrate(){
		//$this->wilayah_model->migrate();
		
		$this->dbforge->drop_table('tweb_dusun_x');
		$this->dbforge->drop_table('tweb_rw_x');
		$this->dbforge->drop_table('tweb_rt_x');
		$this->dbforge->drop_table('tweb_keluarga_x');
		$this->dbforge->drop_table('tweb_keluarga_x_pindah');
		$this->dbforge->drop_table('tweb_penduduk_x');
		$this->dbforge->drop_table('tweb_penduduk_x_pindah');
		
	}

	function import_dasar(){
		$this->import_model->import_excel();
		redirect('database/import');
		//import_das();
	}
	
	function import_akp(){
		$this->import_model->import_akp();
		redirect('database/import');
	}

}
