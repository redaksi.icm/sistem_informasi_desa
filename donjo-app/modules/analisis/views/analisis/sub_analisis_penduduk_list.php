<script>
	$(function() {
		var keyword = <?php echo $keyword?> ;
		$( "#cari" ).autocomplete({
			source: keyword
		});
	});
</script>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">
		
	</td>
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3><?php echo $master['nama']?></h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
        </div>
    </div>
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <div class="table-panel top">
            <div class="left">
            </div>
            <div class="right">
            </div>
        </div>
        <table class="list">
		<thead>
            <tr>
                <th>No</th>
                <th><input type="checkbox" class="checkall"/></th>
                <th width="50">Aksi</th>
				<th align="center">Nama</th>
				<th align="center">Nilai</th>
            
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data){ ?>
		<tr>
          <td align="center" width="2"><?php echo $data['no']?></td>
			<td align="center" width="5">
				<input type="checkbox" name="id_cb[]" value="<?php echo $data['id']?>" />
			</td>
          <td>
            <a href="<?php echo site_url("analisis/master_penduduk/sub_analisis_penduduk_form/$data[id]")?>" class="ui-icons icon-edit tipsy south" title="Edit Data"></a><a href="<?php echo site_url("analisis/master_penduduk/sub_delete/$data[id]")?>" class="ui-icons icon-remove tipsy south" title="Delete Data" target="confirm" message="Apakah Anda Yakin?" header="Hapus Data"></a>
          </td>
          <td><?php echo $data['nama']?></td>
          <td><?php echo $data['nilai']?></td>
		  </tr>
        <?php }?>
		</tbody>
        </table>
    </div>
	</form>
    <div class="ui-layout-south panel bottom">
        <div class="left"> 
            <a href="<?php echo site_url()?>analisis/master_penduduk" class="uibutton icon prev">Kembali</a>
        </div>
        <div class="right">
        </div>
    </div>
</div>
</td></tr></table>
</div>
