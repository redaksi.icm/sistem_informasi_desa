<script type="text/javascript" src="<?php echo base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">
		<fieldset><legend>Laporan : </legend>
			<div class="lmenu">
				<ul>
				<li ><a href="<?php echo site_url()?>sid_laporan_bulanan">Laporan Bulanan</a></li>
				<li ><a href="<?php echo site_url()?>sid_laporan_kelompok">Data Kelompok Rentan</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset><legend>Statistik Penduduk Berdasarkan : </legend>
			<div id="sidecontent" class="lmenu">
				<ul>
				<li <?php if($lap==0){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis/index/0">Pendidikan Sekarang</a></li>
				<li <?php if($lap==1){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis/index/1">Pekerjaan/ Usaha</a></li>
				<li <?php if($lap==2){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis/index/2">Kepemilikan tanah</a></li>
				<li <?php if($lap==3){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis/index/3">Jamkesmas</a></li>
				<li <?php if($lap==4){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis/index/4">cacat mental</a></li>
				<li <?php if($lap==5){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis/index/5">cacat fisik</a></li>
				<li <?php if($lap==6){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis/index/6">kepemilikan bangunan</a></li>
				
				</ul>
			</div>
		</fieldset>
		<fieldset><legend>Statistik Keluarga Berdasarkan : </legend>
			<div id="sidecontent" class="lmenu">
				<ul>
	
					<li <?php if($lap==11){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis_keluarga/index/11">Air Bersih</a></li>
					<li <?php if($lap==12){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis_keluarga/index/12">Layanan Kesehatan</a></li>
					<li <?php if($lap==13){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis_keluarga/index/13">Fasilitas MCK</a></li>
					<li <?php if($lap==14){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis_keluarga/index/14">Penerangan Rumah</a></li>
					<li <?php if($lap==15){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis_keluarga/index/15">Lantai Rumah</a></li>
					<li <?php if($lap==16){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis_keluarga/index/16">Pola Makan</a></li>
					<li <?php if($lap==17){?>class="selected"<?php }?>>
					<a href="<?php echo site_url()?>sid_laporan_analisis_keluarga/index/17">Sekolah Anak</a></li>
				</ul>
			</div>
		</fieldset>
		</td>
<td style="background:#fff;padding:0px;"> 
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/exporting.js"></script>
<script type="text/javascript">
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'chart',
						defaultSeriesType: 'column'
					},
					title: {
						text: 'Statistik <?php echo $stat?>'
					},
					xAxis: {
						title: {
							text: '<?php echo $stat?>'
						},
                        categories: [
						<?php  $i=0;foreach($main as $data){$i++;?>
						  <?php if($data['jumlah'] != "-"){echo "'$i',";}?>
						<?php }?>
						]
					},
					yAxis: {
						title: {
							text: 'Populasi'
						}
					},
					legend: {
						layout: 'vertical',
						backgroundColor: '#FFFFFF',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 70,
						floating: true,
						shadow: true,
                        enabled:false
					},
					tooltip: {
						formatter: function() {
							return ''+
								this.x +': '+ this.y +'';
						}
					},
					plotOptions: {
						series: {
                            colorByPoint: true
                        },
                        column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
				        series: [{
						name: 'Pendapatan',
						data: [
						<?php  foreach($main as $data){?>
						  <?php if($data['jumlah'] != "-"){echo $data['jumlah'].",";}?>
						<?php }?>]
				
					}]
				});
				
				
			});
				
</script>
<style>
tr#total{
    background:#fffdc5;
    font-size:12px;
    white-space:nowrap;
    font-weight:bold;
}
</style>
<div id="contentpane">
    <div class="ui-layout-north panel top">
    </div>
    <div class="ui-layout-center" id="chart" style="padding: 5px;">                
        
    </div>
    <div class="ui-layout-south panel bottom" style="max-height: 150px;overflow:auto;">
        <table class="list">
		<thead>
            <tr>
                <th>No</th>
				<th align="left" align="center">Statistik</th>
				<th align="left" align="center">Jumlah</th>
				
            
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
          <td align="center" width="2"><?php echo $data['no']?></td>
          <td><?php echo $data['nama']?></td>
          <td><?php echo $data['jumlah']?></td>
		  
		  </tr>
        <?php  endforeach; ?>
		</tbody>
        </table>
    </div>
</div>
</td></tr></table>
</div>
