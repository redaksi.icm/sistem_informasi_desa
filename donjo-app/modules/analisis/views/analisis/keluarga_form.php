<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">

			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
					<li class="selected"><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>

	</td>
		<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    
</div>
<div id="contentpane">
	<form id="validasi" name="mainform" action="<?php echo $form_action?>" method="post">
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
	<h3>Form Analisis Rumah Tangga</h3>
        <table class="list">
		<thead>
		    	<tr>
				<th width="15">No</th>
				<th width="550">Indikator</th>
				<th>Jawaban</th>
		   	 </tr>
		</thead>
			<?php foreach($analisis AS $data){?>
			<tr>
				<td><?php echo $data['id']?></td>
				<td><label><?php echo $data['nama']?></label></td>
				<td>
					<input type="hidden" name="id_master[]" value="<?php echo $data['id']?>">
					<select  name="id_sub[] idku<?php echo $data['id']?>" class="required">
						<option value=""> - - - - - - - - - -  </option>
					<?php foreach($data['sub_analis'] AS $sub){?>
						<option value="<?php echo $sub['id']?>" <?php if($sub['id']==$sub['sel']){?>selected<?php }?>><?php echo $sub['no_jawaban']?>). <?php echo $sub['nama']?></option>
					<?php }?>
					</select>        
				</td>				  
			</tr>
			<?php }?>
		</table>  
	</div>
	<div class="ui-layout-south panel bottom">
        <div class="left">     
            <a href="<?php echo site_url()?>analisis" class="uibutton icon prev">Kembali</a>
        </div>
        <div class="right">
            <div class="uibutton-group">
                <button class="uibutton" type="reset">Clear</button>
                <button class="uibutton confirm" type="submit" >Simpan</button>
            </div>
        </div>
	</div> 
	</form> 
</div>
</td></tr>
</table>
</div>
