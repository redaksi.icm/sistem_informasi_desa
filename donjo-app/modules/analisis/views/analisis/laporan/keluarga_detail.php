<script type="text/javascript" src="<?php echo base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	
				<td class="side-menu">
		<fieldset>
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset>
			<div  id="sidecontent3" class="lmenu">
				<ul>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li  class="selected"><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
		</fieldset>
	</td>
		
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Laporan</h3>
</div>
<div id="contentpane" style="overflow:auto;">    
	<form id="mainform" name="mainform" action="" method="post">

    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <div class="table-panel top">

<table class="form">
	<tr>
		<tr><td colspan=3 align="center">RINCIAN LAPORAN HASIL ANALISIS KELUARGA</td></tr>
		<tr><td width=7%>Statistik</td><td width=1%>:</td><td><?php  echo $tanya['tanya']['nama']; ?></td></tr>
		<tr><td>Jawaban</td><td>:</td><td><?php  echo $jawab['jawab']['nama']; ?></td></tr>
		<tr><td>Tahun</td><td>:</td><td><?php  echo $thn ; ?></td></tr>

		<tr>
			<td>Dusun</td>
			<td>:</td>
			<td>
				<select name="dusun" onchange="formAction('mainform','<?php echo site_url("analisis/laporan_keluarga/dusun/$stat/$jwb/$thn/$bln/$lap/$p/$o")?>')">
					<option value="">Dusun</option>					
					<?php  foreach($list_dusun AS $data){?>
					<option value="<?php echo $data['dusun']?>" <?php if($dusun == $data['dusun']) :?>selected<?php endif?>><?php echo ununderscore(unpenetration($data['dusun']))?></option>
					<?php  }?>
				</select>
			</td>
		</tr>
		<?php  if($dusun){?>
		<tr>
			<td>RW</td>
			<td>:</td>
			<td>						
                <select name="rw" onchange="formAction('mainform','<?php echo site_url("analisis/laporan_keluarga/rw/$stat/$jwb/$thn/$bln/$lap/$p/$o")?>')">
                    <option value="">RW</option>
					<?php foreach($list_rw AS $data){?>
                    <option value="<?php echo $data['rw']?>" <?php if($rw == $data['rw']) :?>selected<?php endif?>><?php echo $data['rw']?></option>
					<?php  }?>
                </select>
			</td>
		</tr>
		<?php  }?>
		<?php  if($rw){?>
		<tr>
			<td>RT</td>
			<td>:</td>
			<td>
                <select name="rt" onchange="formAction('mainform','<?php echo site_url("analisis/laporan_keluarga/rt/$stat/$jwb/$thn/$bln/$lap/$p/$o")?>')">
                    <option value="">RT</option>
					<?php foreach($list_rt AS $data){?>
                    <option value="<?php echo $data['rt']?>" <?php if($rt == $data['rt']) :?>selected<?php endif?>><?php echo $data['rt']?></option>
					<?php  }?>
                </select>
			</td>
		</tr>
		<?php  }?>
	</tr>
</table>
        </div>
        <table class="list">
		<thead>
	        <tr>
                <th>No</th>
				<th align="left" align="center" >Nomor KK</th>
				<th align="left" align="center" >Kelpala Keluarga</th>
				<th align="left" align="center" >Jumlah Anggota Keluarga</th>  
	            <th align="left" align="center" >Dusun</th>  
	            <th align="left" align="center" >RW</th>  
	            <th align="left" align="center" >RT</th>  				
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
          	<td align="center" width="2"><?php echo $data['no']?></td>
         	<td ><?php echo $data['no_kk']?></td>
         	<td><?php echo $data['kepala_kk']?></td>
	  	 	<td><?php echo $data['jml_anggota']?></td> 
		   	<td><?php echo $data['dusun']?></td> 
		   	<td><?php echo $data['rw']?></td> 
		   	<td><?php echo $data['rt']?></td> 
		</tr>
        <?php  endforeach; ?>
		</tbody>
        </table>
    </div>
	</form>
    <div class="ui-layout-south panel bottom">
<div class="left">     
<a href="<?php echo site_url()?>analisis/laporan_keluarga" class="uibutton icon prev">Kembali</a>
</div>
<div class="right">
<div class="uibutton-group">
<a href="<?php echo site_url("analisis/laporan_keluarga/cetak_detail/$stat/$jwb/$thn/$bln/$lap/$p/$o")?>" target="_blank" class="uibutton special">Cetak</a>

</div>
</div>
</div> </form>
</div>
</td></tr></table>
</div>
