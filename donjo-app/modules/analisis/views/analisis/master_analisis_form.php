<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">

		<fieldset>
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li class="selected" ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset>
			<div  id="sidecontent3" class="lmenu">
				<ul>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
		</fieldset>
		<!-- <fieldset>
		<legend>Keterangan : </legend>
			<div class="lmenu">	
				Basis data ini akan diperbarui oleh Pemerintah Desa secara rutin sebagai bagian dari sistem pelayanan publik untuk administrasi kependudukan desa. Sistem Informasi Desa (SID) dibangun dan dikembangkan bersama oleh tim Pemerintah Desa dengan COMBINE Resource Institution.				
			</div>
		</fieldset> -->
	</td>
	<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Form Master Analisis</h3>
</div>
<div id="contentpane">
    <form id="validasi" action="<?php echo $form_action?>" method="POST" id="mainform" name="mainform">
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <table class="form">
            <tr>
                <th>Kode</th>
                <td><input name="id" type="text" class="inputbox required" size="10" value="<?php echo $master_analisis['id']?>"/></td>
            </tr>
            <tr>
                <th>Nama Analisis</th>
                <td><input name="nama" type="text" class="inputbox required" size="60" value="<?php echo $master_analisis['nama']?>"/></td>
            </tr>     
        </table>
    </div>
    <div class="ui-layout-south panel bottom">
        <div class="left">     
            <a href="<?php echo site_url()?>master_analisis" class="uibutton icon prev">Kembali</a>
        </div>
        <div class="right">
            <div class="uibutton-group">
                <button class="uibutton" type="reset">Clear</button>
                <button class="uibutton confirm" type="submit" >Simpan</button>
            </div>
        </div>
    </div> </form>
</div>
</td></tr></table>
</div>
