<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rtm extends CI_Controller{

function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('user_model');
		$this->load->model('rtm_model');
		$this->load->model('penduduk_model');
		$grup	= $this->user_model->sesi_grup($_SESSION['sesi']);
		if($grup!=1 AND $grup!=2) redirect('siteman');
		$this->load->model('header_model');
	}
	
	function clear(){
		unset($_SESSION['cari']);
		unset($_SESSION['filter']);
		unset($_SESSION['dusun']);
		unset($_SESSION['rw']);
		unset($_SESSION['rt']);
		unset($_SESSION['raskin']);
		unset($_SESSION['id_blt']);
		unset($_SESSION['id_bos']);
		unset($_SESSION['id_pkh']);
		unset($_SESSION['id_jampersal']);
		unset($_SESSION['id_bedah_rumah']);
		$_SESSION['per_page']=50;
		redirect('sid_core/rtm');
	}
	
	function index($p=1,$o=0){
	
		$data['p']        = $p;
		$data['o']        = $o;
		
		if(isset($_SESSION['cari']))
			$data['cari'] = $_SESSION['cari'];
		else $data['cari'] = '';
		
		if(isset($_SESSION['filter']))
			$data['filter'] = $_SESSION['filter'];
		else $data['filter'] = '';
	
		if(isset($_POST['per_page'])) 
			$_SESSION['per_page']=$_POST['per_page'];
		$data['per_page'] = $_SESSION['per_page'];
		
		if(isset($_SESSION['dusun'])){
			$data['dusun'] = $_SESSION['dusun'];
			$data['list_rw'] = $this->penduduk_model->list_rw($data['dusun']);
			
		if(isset($_SESSION['rw'])){
			$data['rw'] = $_SESSION['rw'];
			$data['list_rt'] = $this->penduduk_model->list_rt($data['dusun'],$data['rw']);
						
		if(isset($_SESSION['rt']))
			$data['rt'] = $_SESSION['rt'];
			else $data['rt'] = '';
				
			}else $data['rw'] = '';
			
		}else{
			$data['dusun'] = '';
			$data['rw'] = '';
			$data['rt'] = '';
		}
		$data['grup']	= $this->user_model->sesi_grup($_SESSION['sesi']);
		$data['paging']  = $this->rtm_model->paging($p,$o);
		$data['main']    = $this->rtm_model->list_data($o, $data['paging']->offset, $data['paging']->per_page);
		$data['keyword'] = $this->rtm_model->autocomplete();
		$data['list_dusun'] = $this->penduduk_model->list_dusun();

		$nav['act']= 3;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('sid/menu');
		$this->load->view('sid/nav',$nav);
		$this->load->view('sid/kependudukan/rtm',$data);
		$this->load->view('footer');
	}

	function cetak($o=0){
		$data['main']    = $this->rtm_model->list_data($o, 0, 10000);
		$this->load->view('sid/kependudukan/rtm_print',$data);
	}
	
	function excel($o=0){
		$data['main']    = $this->rtm_model->list_data($o, 0, 10000);
		$this->load->view('sid/kependudukan/rtm_excel',$data);
	}

	function edit_nokk($p=1,$o=0,$id=0){
	
		$data['kk']          = $this->rtm_model->get_rtm($id);
		$data['form_action'] = site_url("sid_core/rtm/update_nokk/$id");
		$this->load->view('sid/kependudukan/ajax_edit_nokk',$data);
	}
	
	function form_old($p=1,$o=0,$id=0){
	
		$data['penduduk'] = $this->rtm_model->list_penduduk_lepas();
		$data['form_action'] = site_url("sid_core/rtm/insert/$id");
		$this->load->view('sid/kependudukan/ajax_add_rtm',$data);
	}

	function form_kk($p=1,$o=0,$id=0){
	
		$data['penduduk'] = $this->rtm_model->list_kk();
		$data['form_action'] = site_url("sid_core/rtm/insert_by_kk/$id");
		$this->load->view('sid/kependudukan/ajax_add_rtm_kk',$data);
	}

	function form_kk_anggota($id=0){
	
		$data['penduduk'] = $this->rtm_model->list_kk();
		$data['form_action'] = site_url("sid_core/rtm/insert_by_kk_anggota/$id");
		$this->load->view('sid/kependudukan/ajax_add_rtm_kk_anggota',$data);
	}

	function dusun($s=0){
		$dusun = $this->input->post('dusun');
		if($dusun!="")
			$_SESSION['dusun']=$dusun;
		else unset($_SESSION['dusun']);
		if($s==1)
			redirect('sid_core/rtm/sosial');
		elseif($s==2)
			redirect('sid_core/rtm/raskin_graph');
		else
			redirect('sid_core/rtm');
	}
		
	function rw($s=0){
		$rw = $this->input->post('rw');
		if($rw!="")
			$_SESSION['rw']=$rw;
		else unset($_SESSION['rw']);
		if($s==1)
			redirect('sid_core/rtm/sosial');
		elseif($s==2)
			redirect('sid_core/rtm/raskin_graph');
		else
			redirect('sid_core/rtm');
	}
	
	function rt($s=0){
		$rt = $this->input->post('rt');
		if($rt!="")
			$_SESSION['rt']=$rt;
		else unset($_SESSION['rt']);
		if($s==1)
			redirect('sid_core/rtm/sosial');
		elseif($s==2)
			redirect('sid_core/rtm/raskin_graph');
		else
			redirect('sid_core/rtm');
	}
	
	function raskin(){
		$raskin = $this->input->post('raskin');
		if($raskin!="")
			$_SESSION['raskin']=$raskin;
		else unset($_SESSION['raskin']);
		redirect('sid_core/rtm');
	}

	function blt(){
		$id_blt = $this->input->post('id_blt');
		if($id_blt!="")
			$_SESSION['id_blt']=$id_blt;
		else unset($_SESSION['id_blt']);
		redirect('sid_core/rtm');
	}

	function bos(){
		$id_bos = $this->input->post('id_bos');
		if($id_bos!="")
			$_SESSION['id_bos']=$id_bos;
		else unset($_SESSION['id_bos']);
		redirect('sid_core/rtm');
	}
	
	function search(){
		$cari = $this->input->post('cari');
		if($cari!='')
			$_SESSION['cari']=$cari;
		else unset($_SESSION['cari']);
		redirect('sid_core/rtm');
	}
	
	function insert(){
		$this->rtm_model->insert();
		redirect('sid_core/rtm');
	}
	
	function insert_by_kk(){
		$id_rtm = $this->rtm_model->insert_by_kk();
		redirect("sid_core/rtm/anggota/1/0/$id_rtm");
	}
	
	function insert_by_kk_anggota($id_rtm=0){
		$this->rtm_model->insert_by_kk_anggota($id_rtm);
		redirect("sid_core/rtm/anggota/1/0/$id_rtm");
	}
	
	function insert_a(){
		$this->rtm_model->insert_a();
		redirect('sid_core/rtm');
	}
	
	function insert_new(){
		$this->rtm_model->insert_new();
		redirect('sid_core/rtm');
	}
	
	function update($id=''){
		$this->rtm_model->update($id);
		redirect('sid_core/rtm');
	}
	
	function update_nokk($id=''){
		$this->rtm_model->update_nokk($id);
		redirect('sid_core/rtm');
	}
	
	function delete($p=1,$o=0,$id=''){
		$this->rtm_model->delete($id);
		redirect('sid_core/rtm');
	}
	
	function delete_all($p=1,$o=0){
		$this->rtm_model->delete_all();
		redirect('sid_core/rtm');
	}	
	
	function anggota($p=1,$o=0,$id=0){
	
		$data['p']        = $p;
		$data['o']        = $o;
		$data['kk']       = $id;
		$data['main']     = $this->rtm_model->list_anggota($id);
		$data['kepala_kk']= $this->rtm_model->get_kepala_rtm($id);

		$nav['act']= 3;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('sid/menu');
		$this->load->view('sid/nav',$nav);
		$this->load->view('sid/kependudukan/rtm_anggota',$data);
		$this->load->view('footer');
	}
	    
	function ajax_add_anggota($p=1,$o=0,$id=0){
	
		$data['p']        = $p;
		$data['o']        = $o;

		$data['main']     = $this->rtm_model->list_anggota($id);
		$kk 			  = $this->rtm_model->get_kepala_kk($id);
		if($kk)
			$data['kepala_kk'] = $kk;
		else
			$data['kepala_kk'] = NULL;
		$data['penduduk'] = $this->rtm_model->list_penduduk_lepas();
		
		$data['form_action'] = site_url("sid_core/rtm/add_anggota/$p/$o/$id");
		
		$this->load->view("sid/kependudukan/ajax_add_anggota_rtm_form", $data);
	}
		    
	function edit_anggota($p=1,$o=0,$id_kk=0,$id=0){
	
		$data['p']        = $p;
		$data['o']        = $o;

		$data['hubungan'] = $this->rtm_model->list_hubungan();
		$data['main']     = $this->rtm_model->get_anggota($id);
		$data['form_action'] = site_url("sid_core/rtm/update_anggota/$p/$o/$id_kk/$id");
		$this->load->view("sid/kependudukan/ajax_edit_anggota_rtm", $data);
	}
	//---------------------
	
	function rtm_print($p=1,$o=0,$id=0){
		$data['p']        = $p;
		$data['o']        = $o;
		$data['kk']       = $id;
		
		$data['main']     = $this->rtm_model->list_anggota($id);
		$data['kepala_kk']= $this->rtm_model->get_kepala_kk($id);
		$this->load->view('sid/kependudukan/rtm_print',$data);
	}
	
	function kartu_rtm($p=1,$o=0,$id=0){
	
		$data['p']        = $p;
		$data['o']        = $o;
		$data['id_kk']    = $id;

		$data['hubungan'] = $this->rtm_model->list_hubungan();
		$data['main']     = $this->rtm_model->list_anggota($id);
		$kk 		  = $this->rtm_model->get_kepala_kk($id);
		$data['desa']     = $this->rtm_model->get_desa();
		
		if($kk)
			$data['kepala_kk'] = $kk;
			
		else
			$data['kepala_kk'] = NULL;
			
		$data['penduduk'] = $this->rtm_model->list_penduduk_lepas();
		$nav['act']= 3;
	
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('sid/menu');
		$this->load->view('sid/nav',$nav);
		$data['form_action'] = site_url("sid_core/rtm/print");
		
		$this->load->view("sid/kependudukan/kartu_rtm", $data);
		$this->load->view('footer');
		
	}
		
	function cetak_kk($id=0){
	
		$data['id_kk']    = $id;

		$data['main']     = $this->rtm_model->list_anggota($id);
		$kk 		  	  = $this->rtm_model->get_kepala_kk($id);
		$data['desa']     = $this->rtm_model->get_desa();
		$data['kepala_kk'] = $kk;
		$nav['act']= 3;
		$header = $this->header_model->get_data();
		$this->load->view("sid/kependudukan/cetak_kk", $data);
		
	}
		
	function add_anggota($p=1,$o=0,$id=0){
		$this->rtm_model->add_anggota($id);
		redirect("sid_core/rtm/anggota/$p/$o/$id");
	}
	
	function update_anggota($p=1,$o=0,$id_kk=0,$id=0){
		$this->rtm_model->update_anggota($id);
		redirect("sid_core/rtm/anggota/$p/$o/$id_kk");
	}
	
	function delete_anggota($p=1,$o=0,$kk=0,$id=''){
		$this->rtm_model->rem_anggota($kk,$id);
		redirect("sid_core/rtm/anggota/$p/$o/$kk");
	}
	
	function delete_all_anggota($p=1,$o=0,$kk=0){
		$this->rtm_model->rem_all_anggota($kk);
		redirect("sid_core/rtm/anggota/$p/$o/$kk");
	}	
	
	function cetak_statistik($tipe=0){
		$data['main']    = $this->rtm_model->list_data_statistik($tipe);
		$this->load->view('sid/kependudukan/rtm_print',$data);
	}
}
