<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Laporan Statistik</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url()?>assets/css/report.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">

<!-- Print Body -->
<div id="body">

<table>
    <tbody><tr>
        <td align="center" >
            <img src="1_files/logo-pemprov-diy-print.jpg" alt="" style="float: left;">
            <h1>PEMERINTAH KABUPATEN <?php echo strtoupper(unpenetration($config['nama_kabupaten']))?> </h1>
            <h1 style="text-transform: uppercase;"></h1>
            <h1>KECAMATAN <?php echo strtoupper(unpenetration($config['nama_kecamatan']))?> </h1>
	    <h1>DESA <?php echo strtoupper(unpenetration($config['nama_desa']))?></h1>
            <h1>LAPORAN ANALISIS STATISTIK <?php echo strtoupper($stat)?></h1>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px 20px;">
		<br>
            <table><tr>
<td>Kepala Keluarga</td>
<td>:</td>
<td><?php  echo unpenetration($pend['nama']) ?></td></tr>
<tr><td>No.Keluarga</td>
<td>:</td>
<td><?php  echo $pend['nik'] ?></td></tr>
<tr><td>Tahun</td>
<td>:</td>
<td><?php  echo $_SESSION['tahun'] ?></td></tr>

<tr><td>Bulan</td>
<td>:</td>
<td><?php  echo $_SESSION['bulan'] ?></td></tr>

</table>
<p>&nbsp;</p>



<p>&nbsp;</p>
<p>&nbsp;</p>


<div>
<table width="100%" id="tfhover" class="tftable" border="1">
<thead>
<tr><th>#</th>
<th>Kategori/Aspek/Bab</th>
<th>Pilihan Jawaban</th>
<th>Hasil Survey</th></tr>
</thead>

<tbody>
<?php  $i=1; $tot_bobot=0; $tot_hasil=0;?>
<?php  foreach($analisis AS $data){?>
	<tr><td><?php  echo $i;?></td>
	<td><?php  echo $data['pertanyaan']?></td>
	<td><?php  echo $data['jawaban']?> <?php  if($data['nilai']){?> (<?php  echo $data['nilai']?> )<?php  } ?></td>
	<td>
	<?php  echo $data['hasil'] ?>
	</td>
	</tr>
<?php  $i++;$tot_hasil=$tot_hasil+$data['hasil']; } ?>
</tbody>

<tfoot>
<tr><th>#</th>
<th>JUMLAH</th>
<th></th>
<th><?php  echo $tot_hasil;?></th>
</tr>
</tfoot>
</table>      
</div>
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>

</body></html>
