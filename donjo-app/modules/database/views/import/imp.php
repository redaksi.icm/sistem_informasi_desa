<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">

<td style="background:#fff;padding:0px;"> 
<div class="content-header">
</div>
<div id="contentpane">    

    <div class="ui-layout-north panel">
    <h3>Import Data Desa</h3>
    </div>

<div class="ui-layout-center" id="maincontent" style="padding: 5px;">
            <div class="left">
<form action="<?php echo $form_action?>" method="post" enctype="multipart/form-data">
               <table class="form">
			   <tr>
					<td width="500" colspan="3">
					<p font-size="14px";>
					Mempersiapkan data dengan bentuk excel untuk import ke dalam database SID/SAID:
					<br>
					<ol>

					<li value="1">Pastikan format data yang akan di import sudah sesuai dengan aturan import data:
					<dl>
					<dl>-> Tidak boleh menggunakan tanda ' (petik satu) dalam penggunaan nama, 
					<br><dl>-> Format tanggal untuk terdeteksi dalam format date dalam database menggunakan tambahan ' (petik satu) di dalam excel. 
					<br>Contoh :  '1988-09-15
					<br><dl>-> Struktur RT RW, jika tidak ada dalam struktur wilayah desa diganti dengan tanda � (min/strip/das)
					<br><dl>-> Data (Jenis Kelamin, Agama, Pendidikan, Pekerjaan, Status Perkawinan, Status Hubungan dalam Keluarga, Kewarganegaraan, Golongan darah, Jamkesmas, raskin, klasifikasi sosial ekonomi) terwakili dengan Nomor. Misal : laki-laki terwakili dengan no 1 dan perempuan dengan kode 2<br>
					</dl>
					<li>Simpan (Save) file Excel sebagai .xls file (jika anda memakai excel 2007 gunakan Save As pilih format .xls) </ul>
					<li>Pastikan format excel ber-ekstensi .xls format excel 2003</ul>

					<li>Data yang dibutuhkan untuk Import dengan memenuhi aturan data<a href="<?php echo base_url()?>assets/import/ATURANDATA.xls"> sebagai berikut</a><br>
					<li>Contoh urutan format dapat dilihat pada <a href="<?php echo base_url()?>assets/import/ContohFormat.xls">tautan berikut</a><br>
					</ol>
					</p>
					</td>
<td>
&nbsp;
</td>
				</tr>

<tr>


<td width="150">
<p>Pilih File XLS:
</td>
<td width="250">
<input name="userfile" type="file" />
<td>
<input type="submit" class="uibutton special" value="Import" /> 
</td>
</p>
</td>
<td>
&nbsp;
</td>
</tr>
</table>
</form>
            </div>


    <div class="ui-layout-south panel bottom">
        <div class="left"> 
		<div class="table-info">
          </div>
        </div>
        <div class="right">
        </div>
    </div>
</div>
</td></tr></table>
</div>
