<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=print.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<div id="body">
<style>
	.num {
	  mso-number-format:General;
	}
	.text{
	  mso-number-format:"\@";/*force text*/
	}
</style>
<div class="header" align="center">
<h3> Data Analisis Rumah Tangga </h3>
</div>
<br>
    <table border=1>
	<thead>
		<tr>
			<th>No</th>
			<th>No. Rumah Tangga</th>
			<th>Kepala Rumah Tangga</th>
			<th>Jumlah Anggota</th>
			<th>Alamat</th>
			<th>Indikator Daerah</th>
			<th>Klasifikasi Daerah</th>
			<th>Indikator Klaster</th>
			<th>Klasifikasi Klaster</th>
		</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
			<td><?php echo $data['no']?></td>
			<td class="text"><?php echo $data['no_kk']?></td>
			<td><?php echo $data['kepala_kk']?></td>
			<td><?php echo $data['anggota'] ;?></td>
			<td><?php echo $data['alamat']?></td>
			<td class="text"><?php echo $data['hasil'] ;?></td>
			<td><?php echo $data['analisis'] ;?></td>
			<td class="text"><?php echo $data['hasil2'] ;?></td>
			<td><?php echo $data['analisis2'] ;?></td>
		</tr>
		<?php  endforeach; ?>
	</tbody>
</table>
</div>