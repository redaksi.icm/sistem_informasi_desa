<style>
	#noticer{
		line-height:20px;
		width:100%;
		clear:both;
		float:left;
		margin:0px auto;
		background:#ddffdd;
	}
	#noticer_akp{
		line-height:20px;
		width:100%;
		clear:both;
		float:left;
		margin:0px auto;
		background:#ffdddd;
	}
</style>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	
		<td class="side-menu">
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li  class="selected"><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
	</td>
		
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Laporan</h3>
</div>
<div id="contentpane" style="overflow:auto;">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel top">
        <div class="left">
            <div class="uibutton-group">
				
				<a href="<?php echo site_url("analisis/laporan_keluarga/cetak")?>" class="uibutton tipsy south" title="Cetak Data" target="_blank"><span class="icon-print icon-large">&nbsp;</span>Cetak Data</a>
				
				<a href="<?php echo site_url("analisis/laporan_keluarga/excel")?>" class="uibutton tipsy south" title="Cetak Data" target="_blank"><span class="icon-file-text icon-large">&nbsp;</span>Export Excell</a>
				
				<a href="<?php echo site_url("analisis/laporan_keluarga/graph_laporan")?>" class="uibutton tipsy south" title="Grafik"><span class="icon-bar-chart icon-large">&nbsp;</span>Grafik Data</a>
				
				<a href="<?php echo site_url("analisis/laporan_keluarga/rentang_analisis")?>" class="uibutton tipsy south" title="Rentang Analisis"><span class="icon-resize-horizontal icon-large">&nbsp;</span>Rentang Analisis</a>
				
				<select name="tahun" onchange="formAction('mainform','<?php echo site_url('analisis/laporan_keluarga/tahun')?>')">';   
				<option value="0"> -- Pilih Tahun -- </option>
				<?php $i=2010;?>
				<?php while($i++<2020){?>
				<option value="<?php echo $i?>" <?php if($i==$thn){?>selected<?php }?>><?php echo $i?></option>
				<?php }?>
				</select>  
            </div>
        </div>
    </div>
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
	<?php if($noticer_akp!=""){?>
	<div id="noticer_akp">
	<?php
	echo "<p>Menampilkan AKP dengan kriteria";
	echo $noticer_akp;
	echo "</p>";
	?>
	</div>
	<?php } ?>
        <table class="list">
		<thead>
            <tr>
              
				<th align="left" align="center">Statistik</th>
				<th align="left" align="center">Jawaban</th>
				<th align="left" align="center">Jumlah responden </th>

            
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
         
          <td ><a href="<?php echo site_url("analisis/laporan_keluarga/tanya/$data[id_master]")?>"><?php echo $data['tanya']?></a></td>
          <td><a href="<?php echo site_url("analisis/laporan_keluarga/jawaban/$data[id_master]/$data[id_sub]")?>"><?php echo $data['jawaban']?></a></td>
	  <td><a href="<?php echo site_url("analisis/turn/$data[id_sub]")?>"><?php echo $data['jml_responden']?></a></td>  

		  </tr>
        <?php  endforeach; ?>
		</tbody>
        </table>
    </div>
	</form>
    <div class="ui-layout-south panel bottom">
        <div class="left"> 
		<div class="table-info">
          <form id="paging" action="<?php echo site_url("analisis/laporan_keluarga/index/")?>" method="post">
		  <label>Tampilkan</label>
            <select name="per_page" onchange="$('#paging').submit()" >
              <option value="20" <?php  selected($per_page,20); ?> >20</option>
              <option value="50" <?php  selected($per_page,50); ?> >50</option>
              <option value="100" <?php  selected($per_page,100); ?> >100</option>
            </select>
            <label>Dari</label>
            <label><strong><?php echo $paging->num_rows?></strong></label>
            <label>Total Data</label>
          </form>
          </div>
        </div>
        <div class="right">
            <div class="uibutton-group">
            <?php  if($paging->start_link): ?>
				<a href="<?php echo site_url("analisis/laporan_keluarga/index/$paging->start_link/$o")?>" class="uibutton"  >First</a>
			<?php  endif; ?>
			<?php  if($paging->prev): ?>
				<a href="<?php echo site_url("analisis/laporan_keluarga/index/$paging->prev/$o")?>" class="uibutton"  >Prev</a>
			<?php  endif; ?>
            </div>
            <div class="uibutton-group">
                
				<?php  for($i=$paging->start_link;$i<=$paging->end_link;$i++): ?>
				<a href="<?php echo site_url("analisis/laporan_keluarga/index/$i/$o")?>" <?php  jecho($p,$i,"class='uibutton special'")?> class="uibutton"><?php echo $i?></a>
				<?php  endfor; ?>
            </div>
            <div class="uibutton-group">
			<?php  if($paging->next): ?>
				<a href="<?php echo site_url("analisis/laporan_keluarga/index/$lap/$paging->next/$o")?>" class="uibutton">Next</a>
			<?php  endif; ?>
			<?php  if($paging->end_link): ?>
                <a href="<?php echo site_url("analisis/laporan_keluarga/index/$lap/$paging->end_link/$o")?>" class="uibutton">Last</a>
			<?php  endif; ?>
            </div>
        </div>
    </div>
</div>
</td></tr></table>
</div>
