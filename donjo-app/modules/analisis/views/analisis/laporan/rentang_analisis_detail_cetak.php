<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Laporan Analisis Keluarga</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url()?>assets/css/report.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">

<!-- Print Body -->
<div id="body">
<div class="header" align="center">
<h3> DATA KELUARGA </h3>
</div>
<br>
<table class="form">
	<tr>
		<tr><td colspan=3 align="center">RINCIAN LAPORAN HASIL ANALISIS KELUARGA</td></tr>
		<tr><td width=10%>Hasil Analisis</td><td width=1%>:</td><td><?php  echo $rentang['nama']; ?></td></tr>
		<tr><td>Tahun</td><td>:</td><td><?php  echo $_SESSION['tahun'] ; ?></td></tr>
		
		<?php  if($dusun){?>
		<tr>
			<td>Dusun</td>
			<td>:</td>
			<td><?php  echo $dusun;?></td>
		</tr>
		<?php }?>
		<?php  if($rw){?>
		<tr>
			<td>RW</td>
			<td>:</td>
			<td><?php  echo $rw; ?></td>
		</tr>
		<?php  }?>
		<?php  if($rt){?>
		<tr>
			<td>RT</td>
			<td>:</td>
			<td><?php  echo $rt; ?></td>
		</tr>
		<?php  }?>
	</tr>
</table>
        </div>
        <table class="list">
		<thead>
	        <tr>
                <th>No</th>
				<th align="left" align="center" >Nomor KK</th>
				<th align="left" align="center" >Kelpala Keluarga</th>
				<th align="left" align="center" >Jumlah Anggota Keluarga</th>  
				<th align="left" align="center" >Nilai</th>  
	            <th align="left" align="center" >Dusun</th>  
	            <th align="left" align="center" >RW</th>  
	            <th align="left" align="center" >RT</th>  				
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
          	<td align="center" width="2"><?php echo $data['no']?></td>
         	<td ><?php echo $data['no_kk']?></td>
         	<td><?php echo $data['kepala_kk']?></td>
	  	 	<td><?php echo $data['jml_anggota']?></td> 
	  	 	<td><?php echo $data['nilai']?></td>
		   	<td><?php echo $data['dusun']?></td> 
		   	<td><?php echo $data['rw']?></td> 
		   	<td><?php echo $data['rt']?></td> 
		</tr>
        <?php  endforeach; ?>
		</tbody>
        </table>
</div>
   
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>

</body></html>


