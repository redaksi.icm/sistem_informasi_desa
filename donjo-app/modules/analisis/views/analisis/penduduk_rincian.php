<div id="pageC">
<table class="inner">
<tr style="vertical-align:top">


<td style="background:#fff;padding:0px;"> 
<div class="content-header">
<h4>Rangkuman Hasil Sensus (Jenis Analisis + Bulan + Tahun ) </h4>
</div>

<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
   <div class="left">
  <div class="uibutton-group">
 <a href="<?php echo site_url("analisis/penduduk/cetak_penduduk/$pend[id]")?>" class="uibutton tipsy south" title="Cetak Data" target="_blank" >
 <span class="icon-print icon-large">&nbsp;</span>Cetak Data</a>
<a href="<?php echo site_url("analisis/penduduk/graph_rincian/$pend[id]")?>" class="uibutton tipsy south" title="Grafik Rincian" ><span class="icon-bar-chart icon-large">&nbsp;</span>Grafik Line</a>
<a href="<?php echo site_url("analisis/penduduk/bar_rincian/$pend[id]")?>" class="uibutton tipsy south" title="Grafik Rincian" ><span class="icon-bar-chart icon-large">&nbsp;</span>Grafik Bar</a>
  </div>
   </div>
    </div>
   
    
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
<div class="table-panel top">

<style type="text/css">
table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
table.tftable th {font-size:12px;background-color:#8DABD4;border-width: 1px;padding: 3px;border-style: solid;border-color: #7195BA;text-align:left;}
table.tftable tr {background-color:#ffffff;}
table.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
</style>

<p>&nbsp;</p>
<table><tr>
<td>Nama Penduduk</td>
<td>:</td>
<td><?php  echo unpenetration($pend['nama']) ?></td></tr><tr>
<td>NIK</td>
<td>:</td>
<td><?php  echo $pend['nik'] ?></td></tr>
<tr><td>Tahun</td>
<td>:</td>
<td><?php  echo $_SESSION['tahun'] ?></td></tr>

<tr><td>Bulan</td>
<td>:</td>
<td><?php  echo $_SESSION['bulan'] ?></td></tr>
</table>
<p>&nbsp;</p>





<p>&nbsp;</p>
<p>&nbsp;</p>


<div>
<table width="100%" id="tfhover" class="tftable" border="1">
<thead>
<tr><th>#</th>
<th>Kategori/Aspek/Bab</th>
<th>Pilihan Jawaban</th>
<th>Hasil Survey</th></tr>
</thead>

<tbody>
<?php  $i=1; $tot_hasil=0;?>
<?php  foreach($analisis AS $data){?>
	<tr><td><?php  echo $i;?></td>
	<td><?php  echo $data['pertanyaan']?></td>
	<td><?php  echo $data['jawaban']?> <?php  if($data['nilai']){?> (<?php  echo $data['nilai']?> )<?php  } ?></td>
	<td>
	<?php  echo $data['hasil'] ?>
	</td>
	</tr>
<?php  $i++;$tot_hasil=$tot_hasil+$data['hasil']; } ?>
</tbody>

<tfoot>
<tr><th>#</th>
<th>JUMLAH</th>

<th></th>
<th><?php  echo $tot_hasil;?></th>
</tr>
</tfoot>
</table></div></div>
</div>












<div class="ui-layout-south panel bottom">
   <div class="left">
  <a href="<?php echo site_url()?>sid_analisis_penduduk" class="uibutton icon prev">Kembali</a>
   </div>
   <div class="right">
  <div class="uibutton-group">
 <button class="uibutton" type="reset">Clear</button>
 <button class="uibutton confirm" type="submit" >Simpan</button>
  </div>
   </div>
</div> 
</form> 
</div>
</td></tr>
</table>
</div>
