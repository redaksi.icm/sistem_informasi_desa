<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=print.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<div id="body">
<style>
	.num {
	  mso-number-format:General;
	}
	.text{
	  mso-number-format:"\@";/*force text*/
	}
</style>
<table width="100%">
	<tr>
        <td align="center" colspan="4">
            PEMERINTAH KABUPATEN <?php echo strtoupper(unpenetration($config['nama_kabupaten']))?>
	<tr>
        <td align="center" colspan="4">
            KECAMATAN <?php echo strtoupper(unpenetration($config['nama_kecamatan']))?>
	<tr>
        <td align="center" colspan="4">
			DESA <?php echo strtoupper(unpenetration($config['nama_desa']))?>
	<tr>
        <td align="center" colspan="4">
            LAPORAN ANALISIS KEMISKINAN <?php echo strtoupper($stat)?> TAHUN <?php echo strtoupper($_SESSION['tahun'])?>
        </td>
    </tr>
</table>
<div>
	<?php if($noticer_akp!=""){?>
	<div id="noticer_akp">
	<?php
	echo "<p>Data AKP dengan kriteria";
	echo $noticer_akp;
	echo "</p>";
	?>
	</div>
	<?php } ?>
<table width="100%" id="tfhover" class="tftable" border="1">
		<thead>
            <tr>
                <th width="8">No</th>
				<th align="left">Pertanyaan</th>
				<th align="left">Jawaban</th>
				<th align="left">Jumlah RTM</th>

            
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
          <td><?php echo $data['no']?></td>
          <td ><?php echo $data['tanya']?></td>
          <td><?php echo $data['jawaban']?></td>
	  <td><?php echo $data['jml_responden']?></td>  
 
		  </tr>
        <?php  endforeach; ?>
		</tbody>
        </table>     
</div>
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>
