<script>
	$(function() {
		var keyword = <?php echo $keyword?> ;
		$( "#cari" ).autocomplete({
			source: keyword
		});
	});
</script>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li class="selected" ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
	</td>
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3></h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
	<div class="left">
		<h4>Indikator : <?php echo $master['nama']?></h4>
    </div>
    </div>
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <div class="table-panel top">
            <div class="left">
            </div>
            <div class="right">
            </div>
        </div>
        <table class="list">
		<thead>
            <tr>
                <th>No</th>
                <th><input type="checkbox" class="checkall"/></th>
				<th align="center">Nama</th>
				<th align="center">Nilai</th>
            
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data){ ?>
		<tr>
          <td align="center" width="2"><?php echo $data['no']?></td>
			<td align="center" width="5">
				<input type="checkbox" name="id_cb[]" value="<?php echo $data['id']?>" />
			</td>
          <td><?php echo $data['nama']?></td>
          <td><?php echo $data['nilai']?></td>
		  </tr>
        <?php }?>
		</tbody>
        </table>
    </div>
	</form>
    <div class="ui-layout-south panel bottom">
        <div class="left"> 
            <a href="<?php echo site_url()?>analisis/master_keluarga" class="uibutton icon prev">Kembali</a>
        </div>
        <div class="right">
        </div>
    </div>
</div>
</td></tr></table>
</div>
