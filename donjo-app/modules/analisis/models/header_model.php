<?php 
class Header_Model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
		
	function get_id_user($user=''){
		$sql   = "SELECT id FROM user WHERE username=?";
		$query = $this->db->query($sql,$user);
		$data = $query->row_array();
		return $data['id'];
	}	
	
	function get_data(){
		$id = $_SESSION['user'];

		$sql   = "SELECT nama,foto FROM user WHERE id=?";
		$query = $this->db->query($sql, $id);
		$data  = $query->row_array();
		$outp['nama'] = $data['nama'];
		$outp['foto'] = $data['foto'];
		
		
		$sql   = "SELECT * FROM config WHERE 1";
		$query = $this->db->query($sql);
		$outp['desa'] = $query->row_array();

		return $outp;
	}
	
	function get_config(){
		$sql   = "SELECT * FROM config WHERE 1";
		$query = $this->db->query($sql);
		$outp['desa'] = $query->row_array();
		return $outp;
	}
}
