<?php 
/*
 * Berkas default dari halaman web utk publik
 * 
 * Copyright 2013 
 * Rizka Himawan <himawan.rizka@gmail.com>
 * Muhammad Khollilurrohman <adsakle1@gmail.com>
 * Asep Nur Ajiyati <asepnurajiyati@gmail.com>
 *
 * SID adalah software tak berbayar (Opensource) yang boleh digunakan oleh siapa saja selama bukan untuk kepentingan profit atau komersial.
 * Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat ciptaan turunan bukan untuk kepentingan komersial
 * selama mereka mencantumkan asal pembuat kepada Anda dan melisensikan ciptaan turunan dengan syarat yang serupa dengan ciptaan asli.
 * Untuk mendapatkan SID RESMI, Anda diharuskan mengirimkan surat permohonan ataupun izin SID terlebih dahulu, 
 * aplikasi ini akan tetap bersifat opensource dan anda tidak dikenai biaya.
 * Bagaimana mendapatkan izin SID, ikuti link dibawah ini:
 * http://lumbungkomunitas.net/bergabung/pendaftaran/daftar-online/
 * Creative Commons Attribution-NonCommercial 3.0 Unported License
 * SID Opensource TIDAK BOLEH digunakan dengan tujuan profit atau segala usaha  yang bertujuan untuk mencari keuntungan. 
 * Pelanggaran HaKI (Hak Kekayaan Intelektual) merupakan tindakan  yang menghancurkan dan menghambat karya bangsa.
 */
?>

<?php 

class Header_Model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	function get_data(){
		$id = $_SESSION['user'];

		$sql   = "SELECT nama,foto FROM user WHERE id=?";
		$query = $this->db->query($sql, $id);
		$data  = $query->row_array();
		$outp['nama'] = $data['nama'];
		$outp['foto'] = $data['foto'];
		
		$sql   = "SELECT * FROM config WHERE 1";
		$query = $this->db->query($sql);
		$outp['desa'] = $query->row_array();
		
		return $outp;
	}
		
	function rtm_optimizer(){	
		$sql   = "UPDATE tweb_penduduk SET id_rtm=0,rtm_level=0 WHERE id_rtm NOT IN(SELECT id FROM tweb_rtm)";
		$query = $this->db->query($sql);
		
		$sql   = "DELETE FROM tweb_rtm WHERE id NOT IN(SELECT DISTINCT id_rtm FROM tweb_penduduk WHERE rtm_level = 1)";
		$query = $this->db->query($sql);
		
	}
	
	function akp_optimizer(){	
		$sql   = "DELETE FROM hasil_analisis_keluarga WHERE id_kel NOT IN(SELECT id FROM tweb_rtm)";
		$this->db->query($sql);
		
		$sql   = "DELETE FROM analisis_keluarga WHERE id_kel NOT IN(SELECT id FROM tweb_rtm)";
		$this->db->query($sql);
	}
	
	function penduduk_optimizer(){	
		$sql   = "UPDATE tweb_penduduk SET status_dasar=1 WHERE status_dasar NOT IN(1,2,3,4)";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE tweb_penduduk SET sex=1 WHERE sex NOT IN(1,2)";
		$this->db->query($sql);
	}
	
	function filter_akp(){	
		$sql   = "UPDATE hasil_analisis_keluarga SET tahun=2014 WHERE 1";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET tahun=2014 WHERE 1";
		$query = $this->db->query($sql);
				
		$a="TRUNCATE TABLE klasifikasi_analisis_keluarga;";
		mysql_query($a);
		
		$a="INSERT INTO klasifikasi_analisis_keluarga (id, nama, dari, sampai, dari1, sampai1, dari2, sampai2, dari3, sampai3, dari4, sampai4, dari5, sampai5, jenis, tipe) VALUES(1, 'Sangat Miskin', 0, 0.41, 0, 0.37, 0, 0.39, 0, 0.39, 0, 0.39, 0, 0.4, 1, 0),(2, 'Miskin', 0.41, 0.56, 0.37, 0.54, 0.39, 0.55, 0.39, 0.55, 0.39, 0.55, 0.4, 0.56, 1, 0),(3, 'Hampir Miskin', 0.56, 0.72, 0.54, 0.7, 0.56, 0.71, 0.55, 0.71, 0.55, 0.71, 0.56, 0.71, 1, 0),(4, 'Rentan Miskin', 0.72, 0.87, 0.7, 0.87, 0.71, 0.87, 0.71, 0.87, 0.71, 0.87, 0.71, 0.87, 1, 0),(5, 'Tidak Miskin', 0.87, 1, 0.87, 1, 0.87, 1, 0.87, 1, 0.87, 1, 0.87, 1, 1, 0);";
		mysql_query($a);

		$sql   = "UPDATE master_analisis_keluarga SET nama = 'Pendidikan yang ditamatkan (Kepala Rumah Tangga)' WHERE id = 24;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE sub_analisis_keluarga SET nama = 'Bagi Hasil' WHERE id = 109;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE sub_analisis_keluarga SET nama = 'Bantuan/Hibah/Warisan' WHERE id = 110;";
		$query = $this->db->query($sql);
				
		$sql   = "UPDATE sub_analisis_keluarga SET nama = 'Milik Orang Tua/Sanak/Saudara' WHERE id = 32;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE sub_analisis_keluarga SET nama = 'Mendapatkan Program/Bantuan' WHERE id = 6;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE sub_analisis_keluarga SET nama = 'Bebas Sewa' WHERE id = 33;";
		$query = $this->db->query($sql);
		
		$sql   = "DELETE FROM sub_analisis_keluarga WHERE id IN(7,8,9,10,11,12)";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET id_sub_analisis = 300 WHERE id_sub_analisis = 33;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET id_sub_analisis = 33 WHERE id_sub_analisis = 32;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET id_sub_analisis = 32 WHERE id_sub_analisis = 300;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET id_sub_analisis = 400 WHERE id_sub_analisis = 110;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET id_sub_analisis = 110 WHERE id_sub_analisis = 109;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET id_sub_analisis = 109 WHERE id_sub_analisis = 400;";
		$query = $this->db->query($sql);
		
		$sql   = "UPDATE analisis_keluarga SET id_sub_analisis = 6 WHERE id_sub_analisis IN(7,8,9,10,11,12)";
		$query = $this->db->query($sql);
	}
	
	function clear_akp(){	
		$a="ALTER TABLE hasil_analisis_keluarga ADD UNIQUE (id_kel,tahun);";
		mysql_query($a);
		
		$a="ALTER TABLE analisis_keluarga ADD UNIQUE (id_kel ,id_master ,tahun);";
		mysql_query($a);
	}
}