<script>
$(function(){
    var nik = {};
    nik.results = [
		<?foreach($penduduk as $data){?>
	   {id:'<?=$data['id']?>',name:'<?=$data['nik']." - ".$data['nama']?>',info:'<?=$data['alamat']?>'},
		<?}?>
		    ];
nik.total = nik.results.length;

$('#id_kepala').flexbox(nik, {
	resultTemplate: '<div><label>No nik : </label>{name}</div><div>{info}</div>',
	watermark: 'Ketik no nik di sini..',
    width: 260,
    noResultsText :'Tidak ada no nik yang sesuai..',
	    onSelect: function() {
		$('#'+'main').submit();
    }  
});
$("#nik_detail").show();
});
</script>
<style>
table.form.detail th{
    padding:5px;
    background:#fafafa;
    border-right:1px solid #eee;
}
table.form.detail td{
    padding:5px;
}
</style>
<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">
		<fieldset>
			<div class="lmenu">
				<ul>
				<li><a href="<?=site_url('web/sosmed')?>">Facebook</a></li>
				</ul>
				<ul>
				<li class="selected" ><a href="<?=site_url('web/sosmed/twitter')?>">Twitter</a></li>
				</ul>
			</div>
		</fieldset>
		
	</td>
		<td style="background:#fff;padding:5px;"> 

<div class="content-header">
    <h3>Pengaturan sosmed Twitter</h3>
</div>
<div id="contentpane">
    <form id="validasi" action="<?=$form_action?>" method="POST" enctype="multipart/form-data">
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <table class="form">
		<tr>
			<td width="150">Link Akun Twitter</td><td><textarea name="twit_link" class=" required" style="resize: none; height:100px; width:250px;" size="300" maxlength='200'><? if($main){echo $main['twit_link'];} ?></textarea></td>
		</tr>
		<tr>
			<td width="150">sosmed-code Twitter</td><td><input class="inputbox" type="text" name="twit_code" value="<?=$main['twit_code']?>" size="50" maxlength="200"/></td>
		</tr>
		<tr>
			<td width="150">Name-tag Twitter</td><td><input class="inputbox" type="text" name="twit_et" value="<?=$main['twit_et']?>" size="50" maxlength="200"/></td>
		</tr>
        </table>
    </div>
   
    <div class="ui-layout-south panel bottom">
        
        <div class="right">
            <div class="uibutton-group">
                <button class="uibutton" type="reset">Clear</button>
                <button class="uibutton confirm" type="submit" >Simpan</button>
            </div>
        </div>
    </div> </form>
</div>
</td></tr></table>
</div>
