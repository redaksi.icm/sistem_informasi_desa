<script>
	$(function() {
		var keyword = <?php echo $keyword?> ;
		$( "#cari" ).autocomplete({
			source: keyword
		});
	});
</script>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">
		
	</td>
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Master Data Analisis</h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
            <div class="uibutton-group">
                <a href="<?php echo site_url('analisis/master_penduduk/form')?>" class="uibutton tipsy south" title="Tambah Data" ><span class="icon-plus icon-large">&nbsp;</span>Tambah Data Analisis</a>
                <button type="button" title="Hapus Data" onclick="deleteAllBox('mainform','<?php echo site_url("analisis/master_penduduk/delete_all")?>')" class="uibutton tipsy south"><span class="icon-trash icon-large">&nbsp;</span>Hapus Data
            </div>
        </div>
    </div>
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <div class="table-panel top">
            <div class="left">
                <select name="filter" onchange="formAction('mainform','<?php echo site_url('analisis/master_penduduk/filter')?>')">
                    <option value="">Semua</option>
                    <option value="1" <?php if($filter==1) :?>selected<?php endif?>>Aktif</option>
                    <option value="2" <?php if($filter==2) :?>selected<?php endif?>>Tidak Aktif</option>
                </select>
            </div>
            <div class="right">
                <input name="cari" id="cari" type="text" class="inputbox help tipped" size="20" value="<?php echo $cari?>" title="Search.."/>
                <button type="button" onclick="$('#'+'mainform').attr('action','<?php echo site_url('analisis/master_penduduk/search')?>');$('#'+'mainform').submit();" class="uibutton tipsy south"  title="Cari Data"><span class="icon-search icon-large">&nbsp;</span>Search</button>
            </div>
        </div>
        <table class="list">
		<thead>
            <tr>
                <th>No</th>
                <th><input type="checkbox" class="checkall"/></th>
                <th width="80">Aksi</th>
				<th align="center">Nama</th>
				<th align="center" width="100">Jumlah Jawaban</th>
				<th align="center" width="70">Enabled</th>
            
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data){ ?>
		<tr>
          <td align="center" width="2"><?php echo $data['no']?></td>
			<td align="center" width="5">
				<input type="checkbox" name="id_cb[]" value="<?php echo $data['id']?>" />
			</td>
          <td> <div class="uibutton-group">
            <a href="<?php echo site_url("analisis/master_penduduk/form/$data[id]")?>" class="uibutton tipsy south" title="Ubah Data"><span class="icon-edit icon-large"> Edit </span></a><a href="<?php echo site_url("analisis/master_penduduk/delete/$data[id]")?>" class="uibutton tipsy south"  title="Hapus Data" target="confirm" message="Apakah Anda Yakin?" header="Hapus Data"><span class="icon-trash icon-large"></span></a><?php  if($data['aktif'] == '2'): ?>
	<a href="<?php echo site_url('analisis/master_penduduk/unlock/'.$data['id'])?>" class="uibutton tipsy south" title="Aktivasi Kategori"><span  class="icon-lock icon-large"></span></a><?php  elseif($data['aktif'] == '1'): ?>
	<a href="<?php echo site_url('analisis/master_penduduk/lock/'.$data['id'])?>" class="uibutton tipsy south" title="Non-aktifkan Kategori"><span class="icon-unlock icon-large"></span></a><?php endif?></div>
          </td>
          <td>
<a href="<?php echo site_url("analisis/master_penduduk/sub_analisis_penduduk/$data[id]")?>" class="uibutton tipsy south" title="Rincian Analisis"><span class="icon-zoom-in icon-large"></span></a><?php echo $data['nama']?></td>
          <td><a href="<?php echo site_url("analisis/master_penduduk/sub_analisis_penduduk_list/$data[id]")?>"><?php echo $data['jml_sub_analisis']?></a></td>
          <td><?php echo $data['aktif_text']?></td>
		  </tr>
        <?php }?>
		</tbody>
        </table>
    </div>
	</form>
    <div class="ui-layout-south panel bottom">
        <div class="left"> 
        </div>
        <div class="right">
        </div>
    </div>
</div>
</td></tr></table>
</div>
