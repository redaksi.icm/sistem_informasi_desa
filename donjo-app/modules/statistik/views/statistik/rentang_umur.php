<script>
	$(function() {
		var keyword = <?=$keyword?> ;
		$( "#cari" ).autocomplete({
			source: keyword
		});
	});
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">
				<fieldset><legend>Statistik Keluarga Berdasarkan : </legend>
			<div id="sidecontent2" class="lmenu">
				<ul>
				<li <?if($lap==21){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/21">Kelas Sosial</a></li>
				<li <?if($lap==22){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/22">Raskin</a></li>
				<li <?if($lap==23){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/23">BLT</a></li>
				<li <?if($lap==24){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/24">BOS</a></li>
				<li <?if($lap==25){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/25">PKH</a></li>
				<li <?if($lap==26){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/26">JAMPERSAL</a></li>
				<li <?if($lap==27){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/27">Bedah Rumah</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset><legend>Statistik Penduduk Berdasarkan : </legend>
			<div  id="sidecontent2" class="lmenu">
				<ul>
				<li <?if($lap==0){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/0">Pendidikan Telah Ditempuh</a></li>
				<li <?if($lap==14){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/14">Pendidikan Sedang Ditempuh</a></li>	
				<li <?if($lap==12){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/12">Pendidikan dalam KK</a></li>
				<li <?if($lap==1){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/1">Pekerjaan</a></li>
				<li <?if($lap==2){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/2">Status Perkawinan</a></li>
				<li <?if($lap==3){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/3">Agama</a></li>
				<li <?if($lap==4){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/4">Jenis Kelamin</a></li>
				<li <?if($lap==5){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/5">Warga Negara</a></li>
				<li <?if($lap==6){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/6">Status Penduduk</a></li>
				<li <?if($lap==7){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/7">Golongan Darah</a></li>	
				<li <?if($lap==9){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/9">Cacat</a></li>
				<li <?if($lap==10){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/10">Sakit Menahun</a></li>		
				<li <?if($lap==11){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/11">Jamkesmas</a></li>	
				<li <?if($lap==13){?>class="selected"<?}?>>
					<a href="<?=site_url()?>statistik/index/13">Umur</a></li>	
				
				</ul>
			</div>
		</fieldset>
		
	</td>
		</td>
		<td style="background:#fff;padding:5px;"> 
<div class="content-header">
    <h3>Rentang Umur</h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
            <div class="uibutton-group">
                <a href="<?=site_url('statistik/form_rentang/0')?>" class="uibutton tipsy south" title="Tambah Data" target="ajax-modal" rel="window" header="Tambah Rentang"><span class="icon-plus-sign icon-large">&nbsp;</span>Tambah Rentang</a>
                <button type="button" title="Hapus Data" onclick="deleteAllBox('mainform','<?=site_url('statistik/delete_all_rentang')?>')" class="uibutton tipsy south"><span class="icon-trash icon-large">&nbsp;</span>Hapus Data
            </div>
        </div>
		<div class="right">
            <div class="uibutton-group">
<a href="<?=site_url('statistik/index/13')?>" class="uibutton icon prev">Kembali</a>
            </div>
        </div>
    </div>

    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <table class="list">
		<thead>
		    	<tr>
				<th width="5%">No</th>
				<th width="5%"><input type="checkbox" class="checkall"/></th>
				<th width="10%">Aksi</th>
				<th width="40%">Nama Kelompok Umur</th>
		    	<th width="40%">Rentang</th>	
		   	 </tr>
		</thead>
		<tbody>
        		<? $no=1; foreach($main as $data): ?>
			<tr>
		  		<td align="center" width="2"><?=$no?></td>
				<td align="center" width="5">
					<input type="checkbox" name="id_cb[]" value="<?=$data['id']?>" />
				</td>
		  		<td align="center">
				<div class="uibutton-group">
		    		    <a href="<?=site_url("statistik/form_rentang/$data[id]")?>" class="uibutton tipsy south" title="Ubah Data" target="ajax-modal" rel="window" header="Ubah Data"><span class="icon-edit icon-large"> Ubah</span></a>
				    <a href="<?=site_url("statistik/rentang_delete/$data[id]")?>" class="uibutton tipsy south" title="Hapus Data" target="confirm" message="Apakah Anda Yakin?" header="Hapus Data"><span class="icon-trash icon-large"></span></a>
				</div>
		  		</td>
				 <td><?=$data['nama']?></td>
				 <td><?=$data['dari']?> - <?=$data['sampai']?> </td>
			</tr>
      			<? $no++; endforeach; ?>
		</tbody>
        </table>

    	</div>
	</form>

</div>
</td></tr></table>
</div>
