<?php 
/*
 * Berkas default dari halaman web utk publik
 * 
 * Copyright 2013 
 * Rizka Himawan <himawan.rizka@gmail.com>
 * Asep Nur Ajiyati <asepnurajiyati@gmail.com>
 * Muhammad Khollilurrohman <adsakle1@gmail.com>
 *
 * SID adalah software tak berbayar (Opensource) yang boleh digunakan oleh siapa saja selama bukan untuk kepentingan profit atau komersial.
 * Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat ciptaan turunan bukan untuk kepentingan komersial
 * selama mereka mencantumkan asal pembuat kepada Anda dan melisensikan ciptaan turunan dengan syarat yang serupa dengan ciptaan asli.
 * Untuk mendapatkan SID RESMI, Anda diharuskan mengirimkan surat permohonan ataupun izin SID terlebih dahulu, 
 * aplikasi ini akan tetap bersifat opensource dan anda tidak dikenai biaya.
 * Bagaimana mendapatkan izin SID, ikuti link dibawah ini:
 * http://lumbungkomunitas.net/bergabung/pendaftaran/daftar-online/
 * Creative Commons Attribution-NonCommercial 3.0 Unported License
 * SID Opensource TIDAK BOLEH digunakan dengan tujuan profit atau segala usaha  yang bertujuan untuk mencari keuntungan. 
 * Pelanggaran HaKI (Hak Kekayaan Intelektual) merupakan tindakan  yang menghancurkan dan menghambat karya bangsa.
 */
?>

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penduduk extends CI_Controller{

        function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('user_model');
		$this->load->model('keluarga_model');
		$grup	= $this->user_model->sesi_grup($_SESSION['sesi']);
		if($grup!=1 AND $grup!=2 AND $grup!=3) redirect('siteman');
		$this->load->model('header_model');
		$_SESSION['per_page'] = 200;
		
	}
	

	function index($p=1,$o=0){
	
		$data['p']        = $p;
		$data['o']        = $o;
		
		if(isset($_SESSION['cari']))
			$data['cari'] = $_SESSION['cari'];
		else $data['cari'] = '';
		
		if(isset($_SESSION['filter']))
			$data['filter'] = $_SESSION['filter'];
		else $data['filter'] = '';
		
		if(isset($_SESSION['jenis'])){
			$data['jenis'] = $_SESSION['jenis'];}
		else {$data['jenis'] = '1';
		$_SESSION['jenis']='1';}
			
		if(isset($_SESSION['bulan'])){
			$data['bulan'] = $_SESSION['bulan'];}
		else {$data['bulan'] = date("m");
		$_SESSION['bulan'] = date("m");}
		
		if(isset($_SESSION['tahun'])){
			$data['tahun'] = $_SESSION['tahun'];}
		else {$data['tahun'] = date("Y");
		$_SESSION['tahun'] = date("Y");}
	
	
		if(isset($_POST['per_page'])) 
			$_SESSION['per_page']=$_POST['per_page'];
		$data['per_page'] = $_SESSION['per_page'];
		
		$data['paging']  = $this->keluarga_model->pagingy($p,$o);
		//$data['main']    = $this->keluarga_model->list_data($o, $data['paging']->offset, $data['paging']->per_page);
		$data['keyword'] = $this->keluarga_model->autocomplete();
		$data['analisis'] = $this->keluarga_model->list_analisis_penduduk($o, $data['paging']->offset, $data['paging']->per_page);
		$data['pertanyaan'] = $this->keluarga_model->list_pertanyaan_penduduk();

		$nav['act']= 1;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('analisis/menu');
		$this->load->view('analisis/nav',$nav);
		$this->load->view('analisis/penduduk',$data);
		$this->load->view('footer');
	}
		
	function form($p=1,$o=0,$id=''){

		$data['kk']          = $this->keluarga_model->get_anggota($id);
		$data['form_action'] = site_url("analisis/penduduk/insert/$id");
		if(isset($_SESSION['bulan']))
			$data['bulan'] = $_SESSION['bulan'];
		else $data['bulan'] = date("m");
		
		if(isset($_SESSION['tahun']))
			$data['tahun'] = $_SESSION['tahun'];
		else $data['tahun'] = date("Y");
		$data['analisis'] = $this->keluarga_model->list_analisis_master_penduduk($id);
		
		$nav['act']= 1;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('analisis/menu');
		$this->load->view('analisis/nav',$nav);
		$this->load->view('analisis/penduduk_form',$data);
		$this->load->view('footer');
	}
	
	function rincian($p=1,$o=0,$id=''){
		$data['pend']          = $this->keluarga_model->get_anggota($id);
		$data['form_action'] = site_url("analisis/penduduk/insert/$id");
		
		$data['analisis'] = $this->keluarga_model->list_analisis_rincian_penduduk($id);
		
		$nav['act']= 1;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('analisis/menu');
		$this->load->view('analisis/nav',$nav);
		$this->load->view('analisis/penduduk_rincian',$data);
		$this->load->view('footer');
	}
	function cetak_penduduk($id=''){

		if(isset($_SESSION['jenis'])){
			$data['jenis'] = $_SESSION['jenis'];}
		else {
		$data['jenis'] = "1";
		$_SESSION['jenis']="1";}
	
        if(isset($_SESSION['bulan']))
			$_SESSION['bulan'] = $_SESSION['bulan'];
		else $_SESSION['bulan'] = date("m");
		
		if(isset($_SESSION['tahun']))
			$_SESSION['tahun'] = $_SESSION['tahun'];
		else $_SESSION['tahun'] = date("Y");
		
		$data['config']  = $this->keluarga_model->get_config();
		$data['pend']          = $this->keluarga_model->get_anggota($id);
//		$data['kepala']          = $this->keluarga_model->get_anggota($data['kk']['nik_kepala']);
        $data['nilai']=$this->keluarga_model->list_nilai_penduduk();
		$data['analisis'] = $this->keluarga_model->list_analisis_rincian_penduduk($id);
		
		$this->load->view('analisis/analisis/penduduk_cetak',$data);	
		}
		
	function graph_rincian($id=''){

		$data['pend']          = $this->keluarga_model->get_anggota($id);
		$data['form_action'] = site_url("analisis/penduduk/insert/$id");
		$data['form_action_kembali'] = site_url("analisis/penduduk/rincian/0/1/$id");
		$data['grafik'] = $this->keluarga_model->list_line_graph_penduduk($id);
		
		$nav['act']= 1;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('analisis/menu');
		$this->load->view('analisis/nav',$nav);
		$this->load->view('analisis/graph_penduduk_rincian',$data);
		$this->load->view('footer');
	}
	
	function bar_rincian($id=''){

		$data['pend']          = $this->keluarga_model->get_anggota($id);
		$data['form_action'] = site_url("analisis/penduduk/insert/$id");
		$data['form_action_kembali'] = site_url("analisis/penduduk/rincian/0/1/$id");

		
		$data['grafik'] = $this->keluarga_model->list_line_graph_penduduk($id);
		
		$nav['act']= 1;
		$header = $this->header_model->get_data();
		$this->load->view('header',$header);
		$this->load->view('analisis/menu');
		$this->load->view('analisis/nav',$nav);
		$this->load->view('analisis/bar_penduduk_rincian',$data);
		$this->load->view('footer');
	}
	
	function search(){
		$cari = $this->input->post('cari');
		if($cari!='')
			$_SESSION['cari']=$cari;
		else unset($_SESSION['cari']);
		redirect('analisis/penduduk');
	}
	
	function jenis(){
		$jenis = $this->input->post('jenis');
		if($jenis!=0)
			$_SESSION['jenis']=$jenis;
		else unset($_SESSION['jenis']);
		redirect('analisis/penduduk');
	}
	
	function bulan(){
		$bulan = $this->input->post('bulan');
		if($bulan!=0)
			$_SESSION['bulan']=$bulan;
		else unset($_SESSION['bulan']);
		redirect('analisis/penduduk');
	}
	
	function tahun(){
		$tahun = $this->input->post('tahun');
		if($tahun!=0)
			$_SESSION['tahun']=$tahun;
		else unset($_SESSION['tahun']);
		redirect('analisis/penduduk');
	}
	
	function insert($id=0){
		$this->keluarga_model->insert_analisis_penduduk($id);
		$this->keluarga_model->insert_hasil_analisis_penduduk($id);
		redirect('analisis/penduduk');
	}
	
	function update($id=''){
		$this->keluarga_model->update_analisis($id);
		redirect('analisis/penduduk');
	}
	
	function delete($p=1,$o=0,$id=''){
		$this->keluarga_model->delete($id);
		redirect('analisis/penduduk');
	}
	
	function delete_all($p=1,$o=0){
		$this->keluarga_model->delete_all();
		redirect('analisis/penduduk');
	}	
	function clear_analisis_all(){
		$this->keluarga_model->clear_analisis_all_penduduk();
		redirect('analisis/penduduk');
	}	

	function clear_analisis($id=''){
		$this->keluarga_model->clear_analisis_penduduk($id);
		redirect('analisis/penduduk');
	}
}
