<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">

	<td class="side-menu">
		<fieldset>
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset>
			<div  id="sidecontent3" class="lmenu">
				<ul>
					<li class="selected"><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
		</fieldset>
	</td>
<td style="background:#fff;padding:0px;"> 
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/highcharts.js"></script>
		<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'line'
            },
            title: {
                text: 'Analisis Kemiskinan Partisipatif'
            },
            subtitle: {
                text: 'No KK : <?php  echo $kk['no_kk'] ?>   Keluarga : <?php  echo spaceunpenetration($kepala['nama'])  ?>'
            },
            xAxis: {
                categories: ['<?php  echo ($_SESSION['tahun']-5) ?>',
							 '<?php  echo ($_SESSION['tahun']-4) ?>',
							 '<?php  echo ($_SESSION['tahun']-3) ?>',
							 '<?php  echo ($_SESSION['tahun']-2) ?>',
							 '<?php  echo ($_SESSION['tahun']-1) ?>',
							 '<?php  echo ($_SESSION['tahun']) ?>',
							 '<?php  echo ($_SESSION['tahun']+1) ?>',
							 '<?php  echo ($_SESSION['tahun']+2) ?>',
							 '<?php  echo ($_SESSION['tahun']+3) ?>',
							 '<?php  echo ($_SESSION['tahun']+4) ?>',
							 '<?php  echo ($_SESSION['tahun']+5) ?>']
            },
            yAxis: {
                title: {
                    text: 'Analisis'
                }
            },
            tooltip: {
                enabled: false,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'°C';
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [<?php  foreach($grafik AS $data){?>
            {
                name: '<?php echo $data['pertanyaan']?>',
                data: [<?php echo $data['satu']?>,<?php echo $data['dua']?>,<?php echo $data['tiga']?>,<?php echo $data['empat']?>,<?php echo $data['lima']?>,<?php echo $data['enam']?>,<?php echo $data['tujuh']?>,<?php echo $data['delapan']?>,<?php echo $data['sembilan']?>,<?php echo $data['sepuluh']?>,<?php echo $data['sebelas']?>]
            }, 
               <?php }?>
            
]
        });
    });
    
});
		</script>
<div class="content-header">
    <h3>Data Keluarga</h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
            <div class="uibutton-group">
				
				
            </div>
        </div>
        <div class="right">
            <div class="uibutton-group">
<a href="<?php echo $form_action_kembali?>" class="uibutton icon prev">Kembali</a>
            </div>
        </div>
    </div>
    <div class="ui-layout-center" id="chart" style="padding: 5px;">    
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
       </div>
    <div class="ui-layout-center" id="chart" style="padding: 5px;">                
         </div>
    <div class="ui-layout-south panel bottom" style="max-height: 150px;overflow:auto;">
    
        
    </div>
	</form>
</div>
</td></tr></table>
</div>
