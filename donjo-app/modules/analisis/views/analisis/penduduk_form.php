<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
	<td class="side-menu">

	</td>
		<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Form Analisis Penduduk</h3>
</div>
<div id="contentpane">
	<form id="mainform" name="mainform" action="<?php echo $form_action?>" method="post">
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <table class="form">
			<tr>
				<th width="130">Bulan</td>
				<td>
					<select name="bulan">';   
						<option value="0"> -- Pilih Bulan -- </option>
					<?php $i=0;?>
					<?php while($i++<12){?>
						<option value="<?php echo $i?>" <?php if($i==$bulan){?>selected<?php }?>><?php echo $i?></option>
					<?php }?>
					</select>       
				</td>				  
			</tr>
			<tr>
				<th width="130">Tahun</td>
				<td>
					<select name="tahun">';   
						<option value="0"> -- Pilih Tahun -- </option>
					<?php $i=2010;?>
					<?php while($i++<2020){?>
						<option value="<?php echo $i?>" <?php if($i==$tahun){?>selected<?php }?>><?php echo $i?></option>
					<?php }?>
					</select>       
				</td>				  
			</tr>
			<td>-------------------------------------------------------------------</td>
			<tr></tr>
			<?php foreach($analisis AS $data){?>
			<input type="hidden" name="id_master[]" value="<?php echo $data['id']?>">
			<tr>
				<th width="130"><?php echo $data['nama']?></td>
				<td>
					<select name="id_sub[]">';   
						<option value="0"> -- Pilih Analisis -- </option>
					<?php foreach($data['sub_analis'] AS $sub){?>
						<option value="<?php echo $sub['id']?>" <?php if($sub['id']==$sub['sel']){?>selected<?php }?>><?php echo $sub['nama']?></option>
					<?php }?>
					</select>        
				</td>				  
			</tr>
			<?php }?>
		</table>  
	</div>
	<div class="ui-layout-south panel bottom">
        <div class="left">     
            <a href="<?php echo site_url()?>analisis/penduduk" class="uibutton icon prev">Kembali</a>
        </div>
        <div class="right">
            <div class="uibutton-group">
                <button class="uibutton" type="reset">Clear</button>
                <button class="uibutton confirm" type="submit" >Simpan</button>
            </div>
        </div>
	</div> 
	</form> 
</div>
</td></tr>
</table>
</div>
