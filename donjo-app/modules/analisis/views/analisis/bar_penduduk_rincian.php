
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">

	
<td style="background:#fff;padding:0px;"> 
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/highcharts.js"></script>
<script type="text/javascript">
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'chart',
						defaultSeriesType: 'column'
					},
					title: {
						text: 'Analisis Kemiskinan Partisipatif'
					},
					subtitle: {
                                                text: 'NIK : <?php  echo $pend['nik'] ?>   Nama: <?php  echo spaceunpenetration($pend['nama']) ?> Tahun : <?php  echo date("Y")?>'
                                         },
					xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
            },
            yAxis: {
                title: {
                    text: 'Analisis'
                }
            },
					legend: {
						layout: 'vertical',
						backgroundColor: '#FFFFFF',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 70,
						floating: true,
						shadow: true,
                        enabled:false
					},
					tooltip: {
						formatter: function() {
							return ''+
								this.x +': '+ this.y +':';
						}
					},
					plotOptions: {
						series: {
                            colorByPoint: true
                        },
                        column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
				        series: [<?php  foreach($grafik AS $data){?>
            {
                name: '<?php echo $data['pertanyaan']?>',
                data: [<?php echo $data['jan']?>,<?php echo $data['feb']?>,<?php echo $data['mar']?>,<?php echo $data['apr']?>,<?php echo $data['mei']?>,<?php echo $data['jun']?>,<?php echo $data['jul']?>,<?php echo $data['ags']?>,<?php echo $data['sep']?>,<?php echo $data['okt']?>,<?php echo $data['nov']?>,<?php echo $data['des']?>]
            }, 
               <?php }?>
            
]
				});
				
				
			});
				
</script>
<div class="content-header">
    <h3>Data Keluarga</h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
            <div class="uibutton-group">
				
				
            </div>
        </div>
        <div class="right">
            <div class="uibutton-group">
<a href="<?php echo $form_action_kembali?>" class="uibutton icon prev">Kembali</a>
            </div>
        </div>
    </div>
    <div class="ui-layout-center" id="chart" style="padding: 5px;">                
        
    </div>
    <div class="ui-layout-south panel bottom" style="max-height: 150px;overflow:auto;">
        <table class="list">
		<thead>
            <tr>
                <th>No</th>
		<th align="left" align="center">Pertanyaan</th>
		<th align="left" align="center">Jan</th>
		<th align="left" align="center">Feb</th>
		<th align="left" align="center">Mar</th>
		<th align="left" align="center">Apr</th>
		<th align="left" align="center">Mei</th>
		<th align="left" align="center">Jun</th>
		<th align="left" align="center">Jul</th>
		<th align="left" align="center">Ags</th>
		<th align="left" align="center">Sep</th>
		<th align="left" align="center">Okt</th>
		<th align="left" align="center">Nov</th>
		<th align="left" align="center">Des</th>
		</tr>
		</thead>
		<tbody>
        <?php  $i=1;foreach($grafik as $data): ?>
		<tr>
          <td align="center" width="2"><?php echo $i?></td>
          <td><?php echo $data['pertanyaan']?></td>
          <td><?php echo $data['jan']?></td>
          <td><?php echo $data['feb']?></td>
          <td><?php echo $data['mar']?></td>
          <td><?php echo $data['apr']?></td>
          <td><?php echo $data['mei']?></td>
          <td><?php echo $data['jun']?></td>
          <td><?php echo $data['jul']?></td>
          <td><?php echo $data['ags']?></td>
          <td><?php echo $data['sep']?></td>
          <td><?php echo $data['okt']?></td>
          <td><?php echo $data['nov']?></td>
          <td><?php echo $data['des']?></td>
		  </tr>
        <?php  $i++; endforeach; ?>
		</tbody>
        </table>
    </div>
	</form>
</div>
</td></tr></table>
</div>
