<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/validasi.js"></script>
<form action="<?php echo $form_action?>" method="post" id="validasi">
<table style="width:100%">

<tr>
	<th align="left">Kelas Sosial</th>
	<td>
	<div class="uiradio">
	<input type="radio" id="ks1" name="kelas_sosial" value="1"/<?php if($kk['kelas_sosial'] == '1'){echo 'checked';}?>>
	<label for="ks1">Kaya</label>
	<input type="radio" id="ks2" name="kelas_sosial" value="2"/<?php if($kk['kelas_sosial'] == '2' OR $kk['kelas_sosial'] == '0'){echo 'checked';}?>>
	<label for="ks2">Sedang</label>
	<input type="radio" id="ks3" name="kelas_sosial" value="3"/<?php if($kk['kelas_sosial'] == '3'){echo 'checked';}?>>
	<label for="ks3">Miskin</label>
	<input type="radio" id="ks4" name="kelas_sosial" value="4"/<?php if($kk['kelas_sosial'] == '4'){echo 'checked';}?>>
	<label for="ks4">Sangat Miskin</label>
	</div>
	</td>
</tr>
<?php  foreach($analisis as $data): ?>
<input type="checkbox" name="id_cb[]" value="<?php  echo $data?>" />
<?php  endforeach; ?>
</tbody>
        </table>
<div class="buttonpane" style="text-align: right; width:400px;position:absolute;bottom:0px;">
    <div class="uibutton-group">
        <button class="uibutton" type="button" onclick="$('#window').dialog('close');">Tutup</button>
        <button class="uibutton confirm" type="submit">Simpan</button>
    </div>
</div>
</form>
