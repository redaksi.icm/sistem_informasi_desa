
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">

	<td class="side-menu">
		<fieldset>
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
				</ul>
			</div>
		</fieldset>
		
		<fieldset>
			<div  id="sidecontent3" class="lmenu">
				<ul>
					<li class="selected"><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
		</fieldset>
	</td>
<td style="background:#fff;padding:0px;"> 
<script type="text/javascript" src="<?php echo base_url()?>assets/js/highcharts/highcharts.js"></script>
<script type="text/javascript">
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'chart',
						defaultSeriesType: 'column'
					},
					title: {
						text: 'Analisis Kemiskinan Partisipatif'
					},
					subtitle: {
                        text: 'No KK : <?php  echo $kk['no_kk'] ?>   Kel: <?php  echo spaceunpenetration($kepala['nama']) ?> Tahun : <?php  echo date("Y")?>'
                                         },
					xAxis: {
                		categories: [
						<?php  $i=0;foreach($grafik as $data){$i++;?>
						  <?php  echo "'$i',"; ?>
						<?php  }?>
						]
					},
					yAxis: {
						title: {
							text: 'Nilai Analisis'
						}
					},
					legend: {
						layout: 'vertical',
						backgroundColor: '#FFFFFF',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 70,
						floating: true,
						shadow: true,
                        enabled:false
					},
					tooltip: {
						formatter: function() {
							return ''+
								this.x +': '+ this.y +':';
						}
					},
					plotOptions: {
						series: {
                            colorByPoint: true
                        },
                        column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
				        series: [{
						name: 'Pertanyaan',
						data: [
						<?php  foreach($grafik as $data){?>
						  	['<?php echo $data['pertanyaan']?>',<?php echo $data['nilai']?>],
						<?php  }?>]
				
					}]
				});
				
				
			});
				
</script>
<div class="content-header">
    <h3>Data Keluarga</h3>
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
        <div class="left">
            <div class="uibutton-group">
				
				
            </div>
        </div>
        <div class="right">
            <div class="uibutton-group">
<a href="<?php echo $form_action_kembali?>" class="uibutton icon prev">Kembali</a>
            </div>
        </div>
    </div>
    <div class="ui-layout-center" id="chart" style="padding: 5px;">                
        
    </div>
    <div class="ui-layout-south panel bottom" style="max-height: 150px;overflow:auto;">
        <table class="list">
		<thead>
            <tr>
                <th>No</th>
		<th align="left" align="center" >Pertanyaan</th>
		<th align="left" align="center">Nilai</th>
		</tr>
		</thead>
		<tbody>
        <?php  $i=1;foreach($grafik as $data): ?>
		<tr>
          <td align="center" width="2"><?php echo $i?></td>
          <td><?php echo $data['pertanyaan']?></td>
          <td><?php echo $data['nilai']?></td>
		  </tr>
        <?php  $i++; endforeach; ?>
		</tbody>
        </table>
    </div>
	</form>
</div>
</td></tr></table>
</div>
