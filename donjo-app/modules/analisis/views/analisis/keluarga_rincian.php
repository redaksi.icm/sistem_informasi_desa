<div id="pageC">
<table class="inner">
<tr style="vertical-align:top">
<td class="side-menu">
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li ><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
					<li class="selected"><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
	</td>

<td style="background:#fff;padding:0px;"> 
<div class="content-header">
<h4>Rangkuman Hasil Sensus (Jenis Analisis + Bulan + Tahun ) </h4>
</div>

<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
   <div class="left">
  <div class="uibutton-group">
 <a href="<?php echo site_url("analisis/cetak_keluarga/$kk[id]")?>"  target="_blank" class="uibutton tipsy south" title="Cetak Data" > <span class="icon-print icon-large">&nbsp;</span>Cetak Data</a>
<a href="<?php echo site_url("analisis/graph_rincian/$kk[id]")?>" class="uibutton tipsy south" title="Grafik Rincian" ><span class="icon-tasks icon-large">&nbsp;</span>Grafik Garis</a>
<a href="<?php echo site_url("analisis/bar_rincian/$kk[id]")?>" class="uibutton tipsy south" title="Grafik Rincian" ><span class="icon-bar-chart icon-large">&nbsp;</span>Grafik Batang</a>
  </div>
   </div>
    </div>
   
    
    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
<div class="table-panel top">

<style type="text/css">
table.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
table.tftable th {font-size:12px;background-color:#8DABD4;border-width: 1px;padding: 3px;border-style: solid;border-color: #7195BA;text-align:left;}
table.tftable tr {background-color:#ffffff;}
table.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
</style>

<p>&nbsp;</p>
<table><tr>
<td>Kepala Rumah Tangga</td>
<td>:</td>
<td><?php  echo unpenetration($kk['nama']) ?></td></tr>
<tr><td>No. Rumah Tangga</td>
<td>:</td>
<td><?php  echo $kk['no_kk'] ?></td></tr>
<tr><td>Tahun</td>
<td>:</td>
<td><?php  echo $_SESSION['tahun'] ?></td></tr>


</table>

<div>
<table width="100%" id="tfhover" class="tftable" border="1">
<thead>
<tr><th align="center">No</th>
<th align="center">Indikator</th>
<th align="center">Bobot</th>
<th align="center">Pilihan Jawaban</th>
<th align="center">Nilai Jawaban</th>
</tr>
</thead>

<tbody>
<?php  $i=1; $tot_bobot=0; $tot_nilai=0; $tot_hasil=0;?>
<?php  foreach($analisis AS $data){?>
	<tr><td><?php  echo $i;?></td>
	<td><?php  echo $data['pertanyaan']?></td>
	<td align="center"><?php  echo $data['bobot']?> </td>
	<td><?php  echo $data['jawaban']?> </td>
	<td align="center"><?php  echo $data['nilai']?> </td>
	</tr>
<?php  $i++;$tot_b1=$tot_bobot+$data['bobot'];$tot_nilai=$tot_nilai+$data['nilai'];$tot_hasil=$tot_hasil+$data['bobot']*$data['nilai']; } ?>
</tbody>

<tfoot>
<tr><th></th>
<th colspan="3">JUMLAH (Sigma (bobot x nilai))</th>
<th align="center" colspan="2"><?php  echo $tot_hasil;?></th>
</tr>
</tfoot>
</table>      
</div>












<div class="ui-layout-south panel bottom">
   <div class="left">
  <a href="<?php echo site_url()?>analisis" class="uibutton icon prev">Kembali</a>
   </div>

   </div>
</div> 
</form> 
</div>
</td></tr>
</table>
</div>
