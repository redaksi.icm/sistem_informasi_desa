<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
			<td class="side-menu">
		<fieldset>
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li ><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li  class="selected" ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
		</fieldset>
	</td>
		</td>
		<td style="background:#fff;padding:5px;"> 
<div class="content-header">
    <h3>Rentang Analisis</h3>
</div>
<div id="contentpane">    
<div class="ui-layout-center" id="maincontent" style="padding: 0px;">
        <div class="table-panel top">
	<form id="mainform" name="mainform" action="" method="post">
            <div class="left">
				<select name="tahun" onchange="formAction('mainform','<?php echo site_url('analisis/laporan_keluarga/rentang_tahun')?>')">';   
				<?php $i=2013;?>
				<?php while($i++<2019){?>
				<option value="<?php echo $i?>" <?php if($i==$_SESSION['tahun']){?>selected<?php }?>><?php echo $i?></option>
				<?php }?>
				</select>  
				<font align="center"> <b>Klasifikasi Hasil Analisis Berdasarkan Rentang</b></h1>
            </div>
           
        </div>
        <table class="list">
		<thead>
		    	<tr>
				<th width="5%">No</th>
				<th width="20%">Nama Rentang Analisis</th>
		    	<th width="20%">Rentang</th>	
				<th >Jumlah Keluarga</th>	
		   	 </tr>
		</thead>
		<tbody>
        		<?php  $no=1; foreach($main as $data): ?>
			<tr>
		  		<td align="center" width="2"><?php echo $no?></td>
				 <td><?php echo $data['nama']?></td>
				 <td><?php echo $data['dari']?> - <?php echo $data['sampai']?> </td>
				 <td><a href="<?php echo site_url("analisis/laporan_keluarga/detail_rentang/$data[id]")?>"><?php echo $data['jumlah']?></a></td>
			</tr>
      			<?php  $no++; endforeach; ?>
		</tbody>
        </table>
	</form>

</div><div class="ui-layout-south panel bottom">
	<div class="left">     
		<a href="<?php echo site_url()?>analisis/laporan_keluarga" class="uibutton icon prev">Kembali</a>
	</div>
</div>
</td></tr></table>

</div>
