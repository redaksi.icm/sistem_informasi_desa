<script type="text/javascript" src="<?php echo base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC"> 
<!-- Start of Space Admin -->
	<table class="inner">
	<tr style="vertical-align:top">
	
		
		
<td style="background:#fff;padding:0px;"> 
<div class="content-header">
    <h3>Laporan</h3>
</div>
<div id="contentpane" style="overflow:auto;">    
	<form id="mainform" name="mainform" action="" method="post">

    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <div class="table-panel top">

<table class="form">
	<tr>
		<tr><td colspan=3 align="center">RINCIAN LAPORAN HASIL ANALISIS PENDUDUK <?php  echo $jk ; ?> </td></tr>
		<tr><td width=7%>Statistik</td><td width=1%>:</td><td><?php  echo $tanya['tanya']['nama']; ?></td></tr>
		<tr><td>Jawaban</td><td>:</td><td><?php  echo $jawab['jawab']['nama']; ?></td></tr>
		<tr><td>Tahun</td><td>:</td><td><?php  echo $thn ; ?></td></tr>
		<tr><td>Bulan</td><td>:</td><td><?php  echo $bln ; ?></td></tr>
	</tr>
</table>
        </div>
        <table class="list">
		<thead>
	        <tr>
                <th>No</th>
				<th align="left" align="center">NIK</th>
				<th align="left" align="center">Nama</th>
				<th align="left" align="center">No KK</th>
				<th align="left" align="center">Tgl Lahir</th>
				<th align="left" align="center">Pendidikan</th>
				<th align="left" align="center">Pekerjaan</th>
				<th align="left" align="center">Jenis Kelamin</th>
				<th align="left" align="center">Status</th>       
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
          <td align="center" width="2"><?php echo $data['no']?></td>
          <td ><?php echo $data['NIK']?></td>
          <td><?php echo $data['nama']?></td>
	  <td><?php echo $data['id_kk']?></td>
	  <td><?php echo $data['tanggallahir']?></td>     
	  <td><?php echo $data['pendidikan']?></td>  
	  <td><?php echo $data['pekerjaan']?></td>  
	  <td><?php echo $data['jk']?></td>  
	  <td><?php echo $data['status_kawin']?></td>  
		  </tr>
        <?php  endforeach; ?>
		</tbody>
        </table>
    </div>
	</form>
    <div class="ui-layout-south panel bottom">
<div class="left">     
<a href="<?php echo site_url()?>analisis/laporan_penduduk" class="uibutton icon prev">Kembali</a>
</div>
<div class="right">
<div class="uibutton-group">

<button class="uibutton confirm" type="submit" >Cetak</button>
</div>
</div>
</div> </form>
</div>
</td></tr></table>
</div>
