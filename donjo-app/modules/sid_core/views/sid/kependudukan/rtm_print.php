<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Data Rumah Tangga</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url()?>assets/css/report.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">
<!-- Print Body -->
<div id="body">
	<div class="header" align="center">
	<h3> Data Rumah Tangga <?php echo $kepala_kk['nama']?> - <?php echo $kepala_kk['no_kk']?></h3>
	</div>
	<br>
	<table class="border thick">
		<thead>
		<tr class="border thick">
			<th>No</th>
			<th width='100'>NIK</th>
			<th>Nama</th>
			<th>Jenis Kelamin</th>
			<th>Tempat, Tanggal Lahir</th>
			<th>Pendidikan</th>
			<th>Keterangan</th>
			<th>Alamat</th>
		</tr>
		</thead>
		
		<tbody>
			<?php  foreach($main as $data): ?>
			<tr>
				<td  width="2"><?php echo $data['no']?></td>
				<td><?php echo $data['nik']?></td>
				<td><?php echo strtoupper(unpenetration($data['nama']))?></td>
				<td><?php echo strtoupper($data['sex'])?></td>
				<td><?php echo strtoupper($data['tempatlahir']) .", ".tgl_indo($data['tanggallahir']); ?></td>
				<td><?php echo strtoupper($data['pendidikan'])?></td>
				<td><?php echo strtoupper($data['hubungan'])?></td>
				<td><?php echo strtoupper(unpenetration(ununderscore($data['alamat'])))?></td>
			</tr>
			<?php  endforeach; ?>
		</tbody>
	</table>
</div>
   
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>

</body></html>
