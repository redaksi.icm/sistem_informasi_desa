<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Laporan Statistik</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_url()?>assets/css/report.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">

<!-- Print Body -->
<div id="body">

<table>
    <tbody><tr>
        <td align="center" >
            <img src="1_files/logo-pemprov-diy-print.jpg" alt="" style="float: left;">
            <h3>PEMERINTAH KABUPATEN <?php echo strtoupper(unpenetration($config['nama_kabupaten']))?> </h3>
            <h3 style="text-transform: uppercase;"></h3>
            <h3>KECAMATAN <?php echo strtoupper(unpenetration($config['nama_kecamatan']))?> </h3>
	    <h3>DESA <?php echo strtoupper(unpenetration($config['nama_desa']))?></h3>
            <h3>LAPORAN ANALISIS STATISTIK <?php echo strtoupper($stat)?> TAHUN<?php echo strtoupper($_SESSION['tahun'])?></h3>
        </td>
    </tr>
</table>
		<br>
	<?php if($noticer_akp!=""){?>
	<div id="noticer_akp">
	<?php
	echo "<p>Data AKP dengan kriteria";
	echo $noticer_akp;
	echo "</p>";
	?>
	</div>
	<?php } ?>
<div>
<table width="100%" id="tfhover" class="tftable" border="1">
		<thead>
            <tr>
                <th>No</th>
				<th align="left" align="center">Pertanyaan</th>
				<th align="left" align="center">Jawaban</th>
				<th align="left" align="center">Jumlah responden</th>
			</tr>
		</thead>
		<tbody>
        <?php  foreach($main as $data): ?>
		<tr>
          <td align="left" width="2"><?php echo $data['no']?></td>
          <td ><?php echo $data['tanya']?></td>
          <td><?php echo $data['jawaban']?></td>
	  <td><?php echo $data['jml_responden']?></td>  
		  </tr>
        <?php  endforeach; ?>
		</tbody>
        </table>     
</div>
<!-- End of Print Body -->
<div style="page-break-after: always;"></div>
</div>
</body></html>