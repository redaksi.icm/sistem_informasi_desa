<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/validasi.js"></script>
<form action="<?php echo $form_action?>" method="post" id="validasi">
<table style="width:100%">
	<tr>
		<th>Klasifikasi Analisis</th>
		<td>
		<select  name="id_klasifikasi" >
			<?php  foreach($list_klasifikasi AS $sub){?>
				<option value="<?php echo $sub['id']?>" <?php  if($sub['id']==$klasifikasi['id_klasifikasi']){?>selected<?php  }?>><?php echo $sub['nama']?></option>
			<?php  }?>
		</select></td>
	</tr>  	
</table>

<div class="buttonpane"  style="text-align: right; width:400px;position:absolute;bottom:0px;>
    <div class="uibutton-group">
        <button class="uibutton" type="button" onclick="$('#window').dialog('close');">Tutup</button>
        <button class="uibutton confirm" type="submit">Simpan</button>
    </div>
</div>
</form>
