<div id="pageC">
	<table class="inner">
	<tr style="vertical-align:top">
			<td class="side-menu">
			<div id="sidecontent3" class="lmenu">
				<ul>
					<li class="selected"><a href="<?php echo site_url()?>analisis/jenis_analisis">Klaster</a></li>
					<li ><a href="<?php echo site_url()?>analisis/master_keluarga">Indikator</a></li>
					<li><a href="<?php echo site_url()?>analisis/rentang_analisis">Rentang</a></li>
					<li><a href="<?php echo site_url()?>analisis/keluarga">Input</a></li>
					<li ><a href="<?php echo site_url()?>analisis/laporan_keluarga">Laporan</a></li>
				</ul>
			</div>
	</td>
		</td>
		<td style="background:#fff;padding:5px;"> 
<div class="content-header">
</div>
<div id="contentpane">    
	<form id="mainform" name="mainform" action="" method="post">
    <div class="ui-layout-north panel">
    <h3>Klaster Analisis</h3>
        <div class="left">
            <div class="uibutton-group">
            </div>
        </div>
		
		<div class="right">
            <div class="uibutton-group">
            </div>
        </div>
    </div>

    <div class="ui-layout-center" id="maincontent" style="padding: 5px;">
        <table class="list">
		    <tr>
				<td>Keterangan</td>
				<td>Silahkan Pilih Klaster analisis desa.</td>
			</tr>
		    <tr>
				<td>Klaster</td>
				<td><select name="kluster" onchange="formAction('mainform','<?php echo site_url("analisis/kluster")?>')">
					<option value="">Pilih Klaster</option>					
					<?php  foreach($list_kluster AS $data){?>
					<option value="<?php echo $data['id']?>" <?php if($kluster['id'] == $data['id']) :?>selected<?php endif?>><?php echo $data['nama']?></option>
					<?php  }?>
				</select>
				</td>
			</tr>
        </table>

    	</div>
	</form>

</div>
</td></tr></table>
</div>
