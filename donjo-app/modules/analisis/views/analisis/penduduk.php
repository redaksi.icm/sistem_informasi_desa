<script>
$(function() {
var keyword = <?php echo $keyword?> ;
$( "#cari" ).autocomplete({
source: keyword
});
});
</script>
<script src="<?php echo base_url()?>assets/js/chosen/chosen.jquery.js"></script>
<div id="pageC"> 
<!-- Start of Space Admin -->
<table class="inner">
<tr style="vertical-align:top">

<td style="background:#fff;padding:0px;"> 
<div class="content-header">
<h3>Data Penduduk</h3>
</div>
<div id="contentpane"> 
<form id="mainform" name="mainform" action="" method="post">
 
<div class="ui-layout-center" id="maincontent" style="padding: 5px;">
<div class="table-panel top">

<div class="left">

<select name="tahun"  onchange="formAction('mainform','<?php echo site_url('analisis/penduduk/tahun')?>')">';   
<option value="0"> -- Pilih Tahun -- </option>
<?php $i=2010;?>
<?php while($i++<2020){?>
<option value="<?php echo $i?>" <?php if($i==$tahun){?>selected<?php }?>><?php echo $i?></option>
<?php }?>
</select>      


<select name="bulan"  onchange="formAction('mainform','<?php echo site_url('analisis/penduduk/bulan')?>')">';   
<option value="0"> -- Pilih Bulan -- </option>
<?php $i=0;?>
<?php while($i++<12){?>
<option value="<?php echo $i?>" <?php if($i==$bulan){?>selected<?php }?>><?php echo $i?></option>
<?php }?>
</select>  
<button type="button" title="Hapus Data" onclick="deleteAllBox('mainform','<?php echo site_url("analisis/penduduk/clear_analisis_all")?>')" class="uibutton tipsy south"><span class="icon-trash icon-large">&nbsp;</span>Hapus Data
</div>


<div class="right">
<input name="cari" id="cari" type="text" class="inputbox help tipped" size="20" value="<?php echo $cari?>" title="Search.."/>
<button type="button" onclick="$('#'+'mainform').attr('action','<?php echo site_url('analisis/penduduk/search')?>');$('#'+'mainform').submit();" class="uibutton tipsy south"title="Cari Data"><span class="icon-search icon-large">&nbsp;</span>Search</button>
</div>
</div>
<table class="list">
<thead>
<tr>
 <th>No</th>
 <th><input type="checkbox" class="checkall"/></th>
 <th width="65">Aksi</th>
<th align="left" align="center" width="100">NIK</th>
<th align="left" align="center"width="100">Nama Penduduk</th>
<th align="left" align="center" width="100">Total</th>

</tr>
</thead>
<tbody>
<?php  foreach($analisis as $data): ?>
<tr>
 <td align="center" width="2"><?php echo $data['no']?></td>
<td align="center" width="5">
<input type="checkbox" name="id_cb[]" value="<?php echo $data['id']?>" />
</td>
 <td width="100"><div class="uibutton-group">
<a href="<?php echo site_url("analisis/penduduk/rincian/$p/$o/$data[id]")?>" class="uibutton tipsy south" title="Rincian Analisis Penduduk"><span  class="icon-zoom-in icon-large"> Rincian </span></a>
<a href="<?php echo site_url("analisis/penduduk/form/$p/$o/$data[id]")?>" class="uibutton tipsy south" title="Ubah Data"><span  class="icon-edit icon-large" ></span> </a>

<a href="<?php echo site_url("analisis/penduduk/clear_analisis/$data[id]")?>"  class="uibutton tipsy south" title="Hapus Data" target="confirm" message="Apakah Anda Yakin?" header="Hapus Data"><span  class="icon-trash icon-large"></span></a>
</div>
</td>
<td><?php echo $data['nik']?></td>
<td><?php echo unpenetration($data['nama'])?></td>
<td><?php  if ($data['hasil']){ echo $data['hasil'] ;} else { echo "-" ;}?></td>

</tr>
<?php  endforeach; ?>
</tbody>
</table>
</div>
</form>
<div class="ui-layout-south panel bottom">
<div class="left"> 
<div class="table-info">
<form id="paging" action="<?php echo site_url('analisis/penduduk')?>" method="post">
<label>Tampilkan</label>
<select name="per_page" onchange="$('#paging').submit()" >
<option value="50" <?php  selected($per_page,50); ?> >50</option>
<option value="100" <?php  selected($per_page,100); ?> >100</option>
<option value="200" <?php  selected($per_page,200); ?> >200</option>
</select>
<label>Dari</label>
<label><strong><?php echo $paging->num_rows?></strong></label>
<label>Total Data</label>
</form>
</div>
</div>
<div class="right">
<div class="uibutton-group">
<?php  if($paging->start_link): ?>
<a href="<?php echo site_url("analisis/penduduk/index/$paging->start_link/$o")?>" class="uibutton">First</a>
<?php  endif; ?>
<?php  if($paging->prev): ?>
<a href="<?php echo site_url("analisis/penduduk/index/$paging->prev/$o")?>" class="uibutton">Prev</a>
<?php  endif; ?>
</div>
<div class="uibutton-group">
 
<?php  for($i=$paging->start_link;$i<=$paging->end_link;$i++): ?>
<a href="<?php echo site_url("analisis/penduduk/index/$i/$o")?>" <?php  jecho($p,$i,"class='uibutton special'")?> class="uibutton"><?php echo $i?></a>
<?php  endfor; ?>
</div>
<div class="uibutton-group">
<?php  if($paging->next): ?>
<a href="<?php echo site_url("analisis/penduduk/index/$paging->next/$o")?>" class="uibutton">Next</a>
<?php  endif; ?>
<?php  if($paging->end_link): ?>
 <a href="<?php echo site_url("analisis/penduduk/index/$paging->end_link/$o")?>" class="uibutton">Last</a>
<?php  endif; ?>
</div>
</div>
 </div>
</div>
</td></tr></table>
</div>
