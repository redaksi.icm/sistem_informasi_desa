<?php 
class Keluarga_Model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	function autocomplete(){
		$sql   = "SELECT t.nama FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id LEFT JOIN tweb_wil_clusterdesa c ON t.id_cluster = c.id WHERE 1  ";
		$query = $this->db->query($sql);
		$data  = $query->result_array();
		
		$i=0;
		$outp='';
		while($i<count($data)){
			$outp .= ",'" .$data[$i]['nama']. "'";
			$i++;
		}
		$outp = strtolower(substr($outp, 1));
		$outp = '[' .$outp. ']';
		return $outp;
	}
	
	function dusun_sql(){		
		if(isset($_SESSION['dusun'])){
			$kf = $_SESSION['dusun'];
			$dusun_sql= " AND c.dusun = '$kf' ";
		return $dusun_sql;
		}
	}
	
	function get_config(){	
		$sql   = "SELECT * FROM config WHERE 1";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}
	
	function rw_sql(){		
		if(isset($_SESSION['rw'])){
			$kf = $_SESSION['rw'];
			$rw_sql= " AND c.rw = '$kf'";
		return $rw_sql;
		}
	}
	
	function rt_sql(){		
		if(isset($_SESSION['rt'])){
			$kf = $_SESSION['rt'];
			$rt_sql= " AND c.rt = '$kf'";
		return $rt_sql;
		}
	}
	
	function kelas_sql(){		
		if(isset($_SESSION['kategori'])){
			$kf = $_SESSION['kategori'];
			$kw="(";
			$i=0;
			while($i<count($kf)){
				$kw .=$kf[$i].",";
				$i++;
			}
			$kw =$kw."7)";
			$kelas_sql= " AND k.id IN $kw";
		return $kelas_sql;
		}
	}
	
	function klaster_sql(){		
		if(isset($_SESSION['klaster'])){
			$kf = $_SESSION['klaster'];
			$kw="(";
			$i=0;
			while($i<count($kf)){
				$kw .=$kf[$i].",";
				$i++;
			}
			$kw =$kw."7)";
			$kelas_sql= " AND l.id IN $kw";
		return $kelas_sql;
		}
	}
	
	function search_sql(){
		if(isset($_SESSION['cari'])){
		$cari = $_SESSION['cari'];
			$kw = $this->db->escape_like_str($cari);
			$kw = '%' .$kw. '%';
			$search_sql= " AND (t.nama LIKE '$kw' OR nik LIKE '$kw')";
			return $search_sql;
			}
		}
	
	function search_keluarga_sql(){
		if(isset($_SESSION['cari'])){
		$cari = $_SESSION['cari'];
			$kw = $this->db->escape_like_str($cari);
			$kw = '%' .$kw. '%';
			$search_keluarga_sql= " AND (t.nama LIKE '$kw' OR no_kk LIKE '$kw')";
			return $search_keluarga_sql;
			}
		}
		

	function search_penduduk_sql(){
		if(isset($_SESSION['cari'])){
		$cari = $_SESSION['cari'];
			$kw = $this->db->escape_like_str($cari);
			$kw = '%' .$kw. '%';
			$search_keluarga_sql= " AND (tb1.nama LIKE '$kw' OR nik LIKE '$kw')";
			return $search_keluarga_sql;
			}
		}
	function rentang1_search(){
		if(isset($_SESSION['rentang1'])){
		$rentang1 = $_SESSION['rentang1'];
			$rentang1_search= " AND (hasil >= ".$rentang1.") ";
			return $rentang1_search;
			}
	}
	function rentang2_search(){
		if(isset($_SESSION['rentang2'])){
		$rentang2 = $_SESSION['rentang2'];
			$rentang2_search= " AND (hasil <= ".$rentang2.") ";
			return $rentang2_search;
			}
	}
	function jenis_sql(){		
		if(isset($_SESSION['jenis'])){
			$kh = $_SESSION['jenis'];
			$jenis_sql= " AND jenis = $kh";
		return $jenis_sql;
		}
	}	

	function bulan_sql(){		
		if(isset($_SESSION['bulan'])){
			$kh = $_SESSION['bulan'];
			$bulan_sql= " AND bulan = $kh";
		}else{
			$bulan_sql= " AND bulan = month(now())";
		}
	return $bulan_sql;
	}	

	function tahun_sql(){		
		if(isset($_SESSION['tahun'])){
			$kh = $_SESSION['tahun'];
			$tahun_sql= " AND tahun = $kh";
			return $tahun_sql;
		} 
	}	
	
	function clear_analisis($id=''){

		$sql  = "DELETE FROM analisis_keluarga WHERE id_kel='$id' AND tahun=$_SESSION[tahun]";
		$outp = $this->db->query($sql);
		
		$sql  = "DELETE FROM hasil_analisis_keluarga WHERE id_kel='$id'  AND tahun=$_SESSION[tahun]";
		$outp = $this->db->query($sql);
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function clear_analisis_all(){
		$id_cb = $_POST['id_cb'];
		
		if(count($id_cb)){
			foreach($id_cb as $id){
				$sql  = "DELETE FROM analisis_keluarga WHERE id_kel='$id'  AND tahun=$_SESSION[tahun]";
				$outp = $this->db->query($sql);
		
				$sql  = "DELETE FROM hasil_analisis_keluarga WHERE id_kel='$id' AND tahun=$_SESSION[tahun]";
				$outp = $this->db->query($sql);
			}
		}
		else $outp = false;
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function get_dusun($id=0){
		$sql   = "SELECT * FROM tweb_rtm WHERE dusun_id=?";
		$query = $this->db->query($sql,$id);
		$data  = $query->row_array();
		return $data;
	}
		
	function get_keluarga($id=0){
		$sql   = "SELECT r.*,p.nama FROM tweb_rtm r LEFT JOIN tweb_penduduk p ON r.id = p.id_rtm WHERE r.id=? AND p.rtm_level = 1";
		$query = $this->db->query($sql,$id);
		$data  = $query->row_array();
		return $data;
	}
	
	function get_anggota($id=0){
		$sql   = "SELECT * FROM tweb_penduduk WHERE id=?";
		$query = $this->db->query($sql,$id);
		$data  = $query->row_array();
		return $data;
	}
	
	function get_kepala_kk($id){
		
		$sql   = "SELECT nik,u.nama,tempatlahir,tanggallahir,a.nama as agama,d.nama as pendidikan,j.nama as pekerjaan, x.nama as sex,w.nama as status_kawin,h.nama as hubungan,warganegara_id,nama_ayah,nama_ibu,g.nama as golongan_darah ,c.rt as rt,c.rw as rw,c.dusun as dusun, (SELECT no_kk FROM tweb_rtm WHERE id = ?) AS no_kk FROM tweb_penduduk u LEFT JOIN tweb_penduduk_pekerjaan j ON u.pekerjaan_id = j.id LEFT JOIN tweb_golongan_darah g ON u.golongan_darah_id = g.id LEFT JOIN tweb_penduduk_pendidikan d ON u.pendidikan_id = d.id LEFT JOIN tweb_penduduk_agama a ON u.agama_id = a.id LEFT JOIN tweb_penduduk_kawin w ON u.status_kawin = w.id LEFT JOIN tweb_penduduk_sex x ON u.sex = x.id LEFT JOIN tweb_penduduk_hubungan h ON u.kk_level = h.id LEFT JOIN tweb_wil_clusterdesa c ON u.id_cluster = c.id WHERE u.id = (SELECT nik_kepala FROM tweb_rtm WHERE id = ?) ";
		$query = $this->db->query($sql,array($id,$id));
		return $query->row_array();
		
	}
	
	function get_kepala_a($id){
		
		$sql   = "SELECT u.*,c.*, (SELECT no_kk FROM tweb_rtm WHERE id = ?) AS no_kk FROM tweb_penduduk u LEFT JOIN tweb_wil_clusterdesa c ON u.id_cluster = c.id WHERE u.id = (SELECT nik_kepala FROM tweb_rtm WHERE id = ?) ";
		$query = $this->db->query($sql,array($id,$id));
		return $query->row_array();
		
	}
        
    function get_desa(){
		$sql   = "SELECT * FROM config WHERE 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	function list_hubungan(){
		$sql   = "SELECT *,nama as hubungan FROM tweb_penduduk_hubungan WHERE 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function list_analisis_master($id=0, $th=0, $jns=1){
		$sql   = "SELECT * FROM master_analisis_keluarga WHERE aktif = 1 AND jenis = $jns ORDER BY id";
		$query = $this->db->query($sql);
		$data = $query->result_array();
	
		//Formating Output
		$i=0;
		while($i<count($data)){
			//$data[$i]['no']=$i+1;
			
			$sql   = "SELECT a.*,(SELECT id_sub_analisis FROM analisis_keluarga WHERE id_master = ? AND id_kel = ?  AND tahun = $th ORDER BY  tahun DESC LIMIT 1) as sel FROM sub_analisis_keluarga a WHERE a.id_master = ? ";
			
			$query = $this->db->query($sql,array($data[$i]['id'],$id,$data[$i]['id']));
			$data[$i]['sub_analis'] =  $query->result_array();
			$i++;
		}
		return $data;
	
	}
	
	function list_analisis_master_penduduk($id=0){
		$sql   = "SELECT * FROM master_analisis_penduduk WHERE aktif = 1 ORDER BY id";
		$query = $this->db->query($sql);
		$data = $query->result_array();
	
		//Formating Output
		$i=0;
		while($i<count($data)){
			//$data[$i]['no']=$i+1;
			
			$sql   = "SELECT a.*,(SELECT id_sub_analisis FROM analisis_penduduk WHERE id_master = ? AND id_pend = ? ORDER BY  tahun DESC LIMIT 1) as sel FROM sub_analisis_penduduk a WHERE a.id_master = ? ";
			
			$query = $this->db->query($sql,array($data[$i]['id'],$id,$data[$i]['id']));
			$data[$i]['sub_analis'] =  $query->result_array();
			$i++;
		}
		return $data;
	
	}
	function list_analisis_rincian($id=0){
		$sql   = "SELECT *, (bobot*nilai)/100 as hasil FROM 
		(SELECT a.id,a.nama as pertanyaan, a.bobot, a.jenis, 
(SELECT nama FROM sub_analisis_keluarga b INNER JOIN analisis_keluarga c ON b.id=c.id_sub_analisis WHERE c.id_master=a.id AND c.id_kel='$id' AND tahun=$_SESSION[tahun] ) as jawaban, 
(SELECT nilai FROM sub_analisis_keluarga b INNER JOIN analisis_keluarga c ON b.id=c.id_sub_analisis WHERE c.id_master=a.id AND c.id_kel='$id'  AND tahun=$_SESSION[tahun] ) as nilai  
FROM master_analisis_keluarga a  WHERE a.aktif=1) as tb WHERE 1 ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->result_array();
		//print_r($data);
		return $data;
	
	}
		
	function list_analisis_rincian_penduduk($id=0){
		$sql   = "SELECT *, (nilai)/100 as hasil FROM (SELECT a.id,a.nama as pertanyaan,  a.jenis, 
(SELECT nama FROM sub_analisis_penduduk b INNER JOIN analisis_penduduk c ON b.id=c.id_sub_analisis WHERE c.id_master=a.id AND c.id_pend='$id' ORDER BY tahun LIMIT 1) as jawaban, 
(SELECT nilai FROM sub_analisis_penduduk b INNER JOIN analisis_penduduk c ON b.id=c.id_sub_analisis WHERE c.id_master=a.id AND c.id_pend='$id' ORDER BY tahun LIMIT 1) as nilai  
FROM master_analisis_penduduk a  WHERE a.aktif=1) as tb WHERE 1 ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->result_array();
		//print_r($data);
		return $data;
	
	}
	
	function list_line_graph($id=0){
		$sql   = "SELECT c.nama as pertanyaan,
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]-5 and id_kel=a.id_kel and d.id_master=a.id_master),0) as satu, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]-4 and id_kel=a.id_kel and d.id_master=a.id_master),0) as dua, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]-3 and id_kel=a.id_kel and d.id_master=a.id_master),0) as tiga, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]-2 and id_kel=a.id_kel and d.id_master=a.id_master),0) as empat, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]-1 and id_kel=a.id_kel and d.id_master=a.id_master),0) as lima, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and id_kel=a.id_kel and d.id_master=a.id_master),0) as enam, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]+1 and id_kel=a.id_kel and d.id_master=a.id_master),0) as tujuh, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]+2 and id_kel=a.id_kel and d.id_master=a.id_master),0) as delapan, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]+3 and id_kel=a.id_kel and d.id_master=a.id_master),0) as sembilan, 
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]+4 and id_kel=a.id_kel and d.id_master=a.id_master),0) as sepuluh,
ifnull((SELECT (nilai*c.bobot)/100 FROM analisis_keluarga d LEFT JOIN sub_analisis_keluarga e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun]+5 and id_kel=a.id_kel and d.id_master=a.id_master),0) as sebelas
 FROM analisis_keluarga a LEFT JOIN sub_analisis_keluarga b ON a.id_sub_analisis=b.id RIGHT JOIN master_analisis_keluarga c ON a.id_master=c.id WHERE id_kel='$id' GROUP BY a.id_master";
//		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->result_array();
		//print_r($data);
		return $data;
	
	}
	
	function list_bar_graph($id=0){
		$sql   = "SELECT c.nama as pertanyaan,(nilai*c.bobot)/100 as nilai FROM analisis_keluarga a LEFT JOIN sub_analisis_keluarga b ON a.id_sub_analisis=b.id RIGHT JOIN master_analisis_keluarga c ON a.id_master=c.id WHERE id_kel='$id' GROUP BY a.id_master ";
//		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->result_array();
		//print_r($data);
		return $data;
	
	}
	function list_line_graph_penduduk($id=0){
		$sql   = "SELECT c.nama as pertanyaan,
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=1 and id_pend=a.id_pend and d.id_master=a.id_master),0) as jan, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=2 and id_pend=a.id_pend and d.id_master=a.id_master),0) as feb, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=3 and id_pend=a.id_pend and d.id_master=a.id_master),0) as mar, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=4 and id_pend=a.id_pend and d.id_master=a.id_master),0) as apr, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=5 and id_pend=a.id_pend and d.id_master=a.id_master),0) as mei, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=6 and id_pend=a.id_pend and d.id_master=a.id_master),0) as jun, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=7 and id_pend=a.id_pend and d.id_master=a.id_master),0) as jul, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=8 and id_pend=a.id_pend and d.id_master=a.id_master),0) as ags, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=9 and id_pend=a.id_pend and d.id_master=a.id_master),0) as sep, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=10 and id_pend=a.id_pend and d.id_master=a.id_master),0) as okt, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=11 and id_pend=a.id_pend and d.id_master=a.id_master),0) as nov, 
ifnull((SELECT (nilai)/100 FROM analisis_penduduk d LEFT JOIN sub_analisis_penduduk e ON d.id_sub_analisis=e.id  where tahun=$_SESSION[tahun] and bulan=12 and id_pend=a.id_pend and d.id_master=a.id_master),0) as des
 FROM analisis_penduduk a LEFT JOIN sub_analisis_penduduk b ON a.id_sub_analisis=b.id RIGHT JOIN master_analisis_penduduk c ON a.id_master=c.id WHERE id_pend='$id' GROUP BY a.id_master";
//		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
	
		$data = $query->result_array();
		//print_r($data);
		return $data;
	
	}
	
	function insert_a(){
		$data = $_POST;
		$lokasi_file = $_FILES['foto']['tmp_name'];
		$tipe_file   = $_FILES['foto']['type'];
		$nama_file   = $_FILES['foto']['name'];
		if (!empty($lokasi_file)){
			if ($tipe_file != "image/jpeg" AND $tipe_file != "image/pjpeg" AND $tipe_file != "image/png"){
				unset($data['foto']);
			} else {
				UploadFoto($nama_file);
				$data['foto'] = $nama_file;
			}
		}else{
			unset($data['foto']);
		}
		
		unset($data['file_foto']);
		unset($data['old_foto']);
		
		$data['tanggallahir'] = tgl_indo_in($data['tanggallahir']);
		
		$outp = $this->db->insert('tweb_penduduk',$data);
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}

	function list_pertanyaan(){
		$sql   = "SELECT * FROM master_analisis_keluarga WHERE aktif = 1 ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
		$data = $query->result_array();
	
		return $data;
	
	}
	
	function list_pertanyaan_penduduk(){
		$sql   = "SELECT * FROM master_analisis_penduduk WHERE aktif = 1 ";
		$sql .= $this->jenis_sql();
		$query = $this->db->query($sql);
		$data = $query->result_array();
	
		return $data;
	
	}

	function list_nilai_keluarga(){
		
		$sql   = "SELECT MAX(hasil) as nilai_max, MIN(hasil) as nilai_min, AVG(hasil) as nilai_rata FROM hasil_analisis_keluarga WHERE tahun=$_SESSION[tahun]  AND jenis=$_SESSION[jenis]";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		return $data;
	}	
	function list_nilai_penduduk(){
		
		$sql   = "SELECT MAX(hasil) as nilai_max, MIN(hasil) as nilai_min, AVG(hasil) as nilai_rata FROM hasil_analisis_penduduk WHERE tahun=$_SESSION[tahun]  AND jenis=$_SESSION[jenis]";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		return $data;
	}	
	function list_analisis_keluarga($o=0,$offset=0,$limit=500){
		
		$sql   = "SELECT u.*,t.nama AS kepala_kk,u.id as id FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id WHERE 1 ";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		$i=0;
		while($i<count($data)){
			$data[$i]['no'] = $i+1;
			$i++;
		}
		
		return $data;
	}
	
	function update_klasifikasi(){
		$kls=$_POST['kelas_sosial'];
		$sql   = "select * from (SELECT (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id   AND tahun=$_SESSION[tahun]) as hasil,u.*,t.nama AS kepala_kk, t.nik FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id  order by (SELECT hasil from hasil_analisis_keluarga where id_kel=u.id  AND tahun=$_SESSION[tahun]) desc) as tb1 where 1 ";
	
		$sql .= $this->search_keluarga_sql();
		$sql .= $this->rentang1_search();
		$sql .= $this->rentang2_search();
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		if(count($data)){
			foreach($data as $id){
				$sql  = "UPDATE tweb_rtm SET kelas_sosial='$kls' WHERE id=?";
				$outp = $this->db->query($sql,($id['id']));
			}
		}
		else $outp = false;
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
		function list_rw($dusun=''){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt = '0' AND dusun = ? AND rw <> '0'";
		$query = $this->db->query($sql,$dusun);
		$data=$query->result_array();
		return $data;
	}
	
	function list_rw_all(){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt = '0' AND rw <> '0'";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		return $data;
	}
				
	function list_rt_all(){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt <> '0' AND rw <> '-'";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		return $data;
	}
	
	function list_rt($dusun='',$rw=''){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rw = ? AND dusun = ? AND rt <> '0'";
		$query = $this->db->query($sql,array($rw,$dusun));
		$data=$query->result_array();
		return $data;
	}
	
	function list_dusun(){
		$sql   = "SELECT * FROM tweb_wil_clusterdesa WHERE rt = '0' AND rw = '0' ";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		return $data;
	}
	
	function list_kluster(){
		$sql   = "SELECT * FROM tipe_analisis WHERE 1";
		$query = $this->db->query($sql);
		$data=$query->result_array();
		return $data;
	}
	
	function get_kluster(){
		$sql   = "SELECT * FROM tipe_analisis WHERE aktif = 1";
		$query = $this->db->query($sql);
		$data=$query->row_array();
		return $data;
	}
	
	function update_kluster(){
		$data = $_POST;
		
		$non['aktif']=0;
		//$this->db->where(true);
		$this->db->update("tipe_analisis",$non);
		
		$aktif['aktif']=1;
		$this->db->where("id",$data['kluster']);
		$outp=$this->db->update("tipe_analisis",$aktif);
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
		
	}
	
	function jawab_sql(){		
		if(isset($_SESSION['jawab'])){
			$tahun = $_SESSION['tahun'];
			$kf = $_SESSION['jawab'];
			$jmkf =  $_SESSION['jmkf'];
			$jawab_sql= "  AND x.id_sub_analisis IN ($kf) AND ((SELECT COUNT(id_sub_analisis) FROM analisis_keluarga WHERE  id_kel = u.id AND id_sub_analisis IN ($kf) AND tahun = $tahun) = $jmkf)";
		return $jawab_sql;
		}
	}

	function pagingx($p=1,$o=0){
	
		$sql   = "SELECT max,id FROM tipe_analisis WHERE aktif = 1";
		$query = $this->db->query($sql);
		$row  = $query->row();
		$b = $row->id;
		$d = "dari".$b;
		$s = "sampai".$b;
		$tahun = $_SESSION['tahun'];
		$analisis = "";
		if(isset($_SESSION['jawab'])){
			$analisis = " LEFT JOIN analisis_keluarga x ON u.id = x.id_kel AND x.tahun =$tahun ";
		}
		
		
		
		$sql="SELECT count(u.id) as id FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON t.id_rtm = u.id AND t.rtm_level = 1 LEFT JOIN tweb_wil_clusterdesa c ON t.id_cluster=c.id LEFT JOIN hasil_analisis_keluarga d ON u.id = d.id_kel AND d.tahun = $tahun $analisis LEFT JOIN klasifikasi_analisis_keluarga k ON (hasil >= k.dari AND hasil <= k.sampai) LEFT JOIN klasifikasi_analisis_keluarga l ON (hasil2 >= l.$d AND hasil2 <= l.$s) WHERE 1 ";
		$sql .= $this->search_keluarga_sql();   
		$sql     .= $this->dusun_sql();   
		$sql     .= $this->rw_sql();  
		$sql     .= $this->rt_sql();    
		$sql .= $this->kelas_sql();
		$sql .= $this->klaster_sql();
		$sql     .= $this->jawab_sql(); 
		
		//$sql .= " group by u.id ";	
		$query    = $this->db->query($sql);
		$row      = $query->row_array();
		$jml_data = $row['id'];
		
		$this->load->library('paging');
		$cfg['page']     = $p;
		$cfg['per_page'] = $_SESSION['per_page'];
		$cfg['num_rows'] = $jml_data;
		$this->paging->init($cfg);
		
		return $this->paging;
	}
	
	function list_analisis_keluargax($o=0,$offset=0,$limit=500){
	
		switch($o){
			case 1: $order_sql = ' ORDER BY hasil'; break;
			case 2: $order_sql = ' ORDER BY hasil DESC'; break;
			case 3: $order_sql = ' ORDER BY hasil2'; break;
			case 4: $order_sql = ' ORDER BY hasil2 DESC'; break;
			case 5: $order_sql = ' ORDER BY g.nama'; break;
			case 6: $order_sql = ' ORDER BY g.nama DESC'; break;
			default:$order_sql = '';
		}
	
		//$order_sql=" order by (SELECT hasil from hasil_analisis_keluarga WHERE id_kel=u.id  AND jenis=$_SESSION[jenis]) desc ";
		
		
		$paging_sql = ' LIMIT ' .$offset. ',' .$limit;
		
		$sql   = "SELECT max,id FROM tipe_analisis WHERE aktif = 1";
		$query = $this->db->query($sql);
		$row  = $query->row();
		$b = $row->id;
		$d = "dari".$b;
		$s = "sampai".$b;
		$tahun = $_SESSION['tahun'];
		
		$analisis = "";
		if(isset($_SESSION['jawab'])){
			$analisis = " LEFT JOIN analisis_keluarga x ON u.id = x.id_kel AND x.tahun =$tahun ";
		}
		
		$sql="SELECT no_kk,u.id, t.nama as kepala_kk, nik, dusun,rt,sex,d.hasil,d.hasil2,k.nama AS analisis,l.nama AS analisis2,
		(SELECT COUNT(id) from tweb_penduduk WHERE id_rtm=u.id AND sex = 1) as lk, 
		(SELECT COUNT(id) from tweb_penduduk WHERE id_rtm=u.id AND sex = 2) as pr
		FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON t.id_rtm = u.id AND t.rtm_level = 1 LEFT JOIN tweb_wil_clusterdesa c ON t.id_cluster=c.id LEFT JOIN hasil_analisis_keluarga d ON u.id = d.id_kel AND d.tahun = $tahun $analisis LEFT JOIN klasifikasi_analisis_keluarga k ON (hasil >= k.dari AND hasil <= k.sampai) LEFT JOIN klasifikasi_analisis_keluarga l ON (hasil2 >= l.$d AND hasil2 <= l.$s) WHERE 1 ";

		$sql .= $this->search_keluarga_sql();
		$sql .= $this->dusun_sql();
		$sql .= $this->rw_sql();
		$sql .= $this->rt_sql();
		$sql .= $this->kelas_sql();
		$sql .= $this->klaster_sql();
		
		if(isset($_SESSION['jawab'])){ 
			$sql     .= $this->jawab_sql();
			$sql .= " group by u.id";	
		}
		
		$sql .= $order_sql;
		$sql .= $paging_sql;
		$query = $this->db->query($sql);
		$data=$query->result_array();
		$i=0;
		$j=$offset;
		while($i<count($data)){
			$data[$i]['no']=$j+1;
			$data[$i]['alamat']=$data[$i]['dusun']." RT-".$data[$i]['rt'];
			$data[$i]['anggota']=$data[$i]['lk']."(L),".$data[$i]['pr']."(P)";
			
			if($data[$i]['sex']==2)
				$data[$i]['kepala_kk'].= " (p)";
			
			$i++;
			$j++;
		}
		return $data;
	}
		
	function pagingy($p=1,$o=0){
	
		$sql      = "select count(id) as id from (SELECT (SELECT hasil from hasil_analisis_penduduk where id_pend=u.id   AND tahun=$_SESSION[tahun]) as hasil,u.* FROM tweb_penduduk u   order by (SELECT hasil from hasil_analisis_penduduk where id_pend=u.id   AND tahun=$_SESSION[tahun]) desc) as tb1 where 1 ";
		
		$sql .= $this->search_penduduk_sql();
		$query    = $this->db->query($sql);
		$row      = $query->row_array();
		$jml_data = $row['id'];
		
		$this->load->library('paging');
		$cfg['page']     = $p;
		$cfg['per_page'] = $_SESSION['per_page'];
		$cfg['num_rows'] = $jml_data;
		$this->paging->init($cfg);
		
		return $this->paging;
	}
	
	function list_analisis_penduduk($o=0,$offset=0,$limit=500){
		
		$paging_sql = ' LIMIT ' .$offset. ',' .$limit;
		
//		$sql   = "SELECT u.* FROM tweb_penduduk u  WHERE 1 ";
		$sql   = "select * from (SELECT (SELECT hasil from hasil_analisis_penduduk where id_pend=u.id   AND tahun=$_SESSION[tahun]) as hasil,$_SESSION[tahun] as tahun,u.* FROM tweb_penduduk u  order by (SELECT hasil from hasil_analisis_penduduk where id_pend=u.id   AND tahun=$_SESSION[tahun]) desc) as tb1 where 1 ";
		$sql .= $this->search_penduduk_sql();
		$sql .= $paging_sql;		
		$query = $this->db->query($sql);
		$data=$query->result_array();
			
		//Formating Output
		$i=0;
		$j=$offset;
		while($i<count($data)){
			$data[$i]['no']=$j+1;
			$i++;
			$j++;
		}
		return $data;
	
	}

	function insert_analisis($id=0){
		$data = $_POST;
		//$data['bulan'] = date("m"); 
		$data['tahun'] = $_SESSION['tahun']; 
		$data['id_kel'] = $id; 
		$sub = $data['id_sub'];
		UNSET($data['id_sub']);
		$master = $data['id_master'];
		UNSET($data['id_master']);
	
		$i=0;
		if(count($sub)){
			foreach($sub as $nilai){
				$data['id_master'] = $master[$i];
				$where = (" tahun = $data[tahun] AND id_kel = $id AND id_master = $master[$i]");
				$data['id_sub_analisis'] = $nilai;
				if($nilai !=""){
					
					$sql   = "SELECT id FROM analisis_keluarga WHERE  ".$where."";
					$query = $this->db->query($sql,array($id,$id));
					$out = $query->row_array();
					
					if($out){
						$sql="UPDATE analisis_keluarga SET id_sub_analisis=$data[id_sub_analisis] where ".$where."";
						$outp= $this->db->query($sql);
					}else{
						$outp = $this->db->insert('analisis_keluarga',$data);
					}	
				}
				$i++;
			}
		}
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
		
	function insert_hasil_analisis($id=0){
        $data = $_POST;
                
		$tahun = $_SESSION['tahun']; 
        $sql   = "DELETE FROM hasil_analisis_keluarga WHERE id_kel='$id' AND tahun = $tahun";
		$query = $this->db->query($sql);
		
		$sql   = "INSERT INTO hasil_analisis_keluarga(id_kel, jenis, hasil, tahun) (SELECT a.id_kel, c.jenis, SUM((nilai*bobot))/3004 as hasil, a.tahun FROM analisis_keluarga a INNER JOIN sub_analisis_keluarga b ON a.id_sub_analisis=b.id INNER JOIN master_analisis_keluarga c ON b.id_master=c.id WHERE id_kel='$id' AND tahun = $tahun)";
		$query = $this->db->query($sql);
		
		$sql   = "SELECT max,id FROM tipe_analisis WHERE aktif = 1";
		$query = $this->db->query($sql);
		$row  = $query->row();
		$max = $row->max;
		$b = $row->id;
		$b = "b".$b;
		
		$sql   = "UPDATE hasil_analisis_keluarga SET hasil2 = (SELECT SUM((nilai*$b))/$max  FROM analisis_keluarga a INNER JOIN sub_analisis_keluarga b ON a.id_sub_analisis=b.id INNER JOIN master_analisis_keluarga c ON b.id_master=c.id WHERE id_kel = $id AND tahun = $tahun) WHERE id_kel = $id";
		$query = $this->db->query($sql);

		//$sql   = "UPDATE tweb_rtm t SET kelas_sosial=(SELECT id_klasifikasi FROM hasil_analisis_keluarga WHERE id_kel=t.id AND tahun = $tahun) ";
		//$query = $this->db->query($sql);
		
	}
	
	function insert_hasil_analisis_penduduk($id=0){
        $data = $_POST;
                
         $sql   = "DELETE FROM hasil_analisis_penduduk WHERE id_pend='$id' AND tahun=$data[tahun] ";
		$query = $this->db->query($sql);
		
		$sql   = "INSERT INTO hasil_analisis_penduduk(id_pend, jenis, hasil, tahun) (SELECT a.id_pend, c.jenis, SUM((nilai)/100) as hasil,  a.tahun FROM analisis_penduduk a INNER JOIN sub_analisis_penduduk b ON a.id_sub_analisis=b.id INNER JOIN master_analisis_penduduk c ON b.id_master=c.id WHERE id_pend='$id' AND tahun=$data[tahun] )";
		$query = $this->db->query($sql);
		
	}
	
	function insert_analisis_penduduk($id=0){
		$data = $_POST;
		//$data['bulan'] = date("m"); 
		//$data['tahun'] = date("Y"); 
		$data['id_pend'] = $id; 
		$sub = $data['id_sub'];
		UNSET($data['id_sub']);
		$master = $data['id_master'];
		UNSET($data['id_master']);

		$i=0;
		if(count($sub)){
			foreach($sub as $nilai){
				$data['id_master'] = $master[$i];
				$where = (' AND tahun = '.$data['tahun'].' AND id_pend = '.$id.' AND id_master = '.$master[$i]);
				$data['id_sub_analisis'] = $nilai;
				if($nilai !=""){
					
					$sql   = "SELECT id FROM analisis_penduduk WHERE ".$where."";
					$query = $this->db->query($sql,array($id,$id));
					$out = $query->row_array();
					
					if($out){
						$this->db->where($where);
						$outp = $this->db->update('analisis_penduduk',$data);
					}else{
						$outp = $this->db->insert('analisis_penduduk',$data);
					}	
				}
				$i++;
			}
		}
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}

	function list_data_rentang(){
		$sql   = "SELECT id FROM tipe_analisis WHERE aktif = 1";
		$query = $this->db->query($sql);
		$row  = $query->row();
		$b = $row->id;
		$d = "dari".$b;
		$c = "sampai".$b;
	
		$order_sql=" order by dari ";

		$sql   = "SELECT r.*,$d AS darix, $c AS sampaix, (SELECT count(id_kel) from hasil_analisis_keluarga where hasil>r.dari-1 AND hasil<=r.sampai AND tahun=$_SESSION[tahun]) as jumlah FROM klasifikasi_analisis_keluarga r WHERE 1 "; 
		
		$sql .= $this->jenis_sql(); 
		$sql .= $order_sql; 
		$query = $this->db->query($sql);
		$data=$query->result_array();

		return $data;
	}
	
	function get_rentang($id=0){	
		$sql   = "SELECT * FROM klasifikasi_analisis_keluarga WHERE id= $id ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}
	
	function get_rentang_terakhir(){	
		$sql   = "SELECT (case when max(sampai) is null then '0' else (max(sampai)+0.01) end) as dari FROM klasifikasi_analisis_keluarga WHERE jenis=$_SESSION[jenis] ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}	
	
	function insert_rentang(){
		$data = $_POST;
		$data['jenis']=$_SESSION['jenis'];
		$outp = $this->db->insert('klasifikasi_analisis_keluarga',$data);
		
		$sql   = "UPDATE hasil_analisis_keluarga h SET id_klasifikasi=(SELECT id FROM klasifikasi_analisis_keluarga where h.hasil>=dari AND h.hasil<= sampai and jenis=h.jenis limit 1), verifikasi='0' WHERE jenis=$_SESSION[jenis]";
		$this->db->query($sql);
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
		
	function update_rentang($id=0){
		$data = $_POST;
		
		$sql   = "UPDATE klasifikasi_analisis_keluarga SET nama='$data[nama]', dari='$data[dari]', sampai='$data[sampai]' WHERE id='$id' ";
		$outp=$this->db->query($sql);

		$sql   = "UPDATE hasil_analisis_keluarga h SET id_klasifikasi=(SELECT id FROM klasifikasi_analisis_keluarga where h.hasil>=dari AND h.hasil<= sampai and jenis=h.jenis limit 1), verifikasi='0' WHERE jenis=$_SESSION[jenis]";
		$outp=$this->db->query($sql);
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function delete_rentang($id=0){
		$sql   = "DELETE FROM klasifikasi_analisis_keluarga WHERE id='$id' ";
		$outp=$this->db->query($sql);
		
		$sql   = "UPDATE hasil_klasifikasi_keluarga SET id_klasifikasi= null, verifikasi='0' WHERE id_klasifikasi='$id' ";
		$outp=$this->db->query($sql);
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function delete_all_rentang(){
		$id_cb = $_POST['id_cb'];
		
		if(count($id_cb)){
			foreach($id_cb as $id){
				$sql  = "DELETE FROM klasifikasi_analisis_keluarga WHERE id=?";
				$outp = $this->db->query($sql,array($id));
				$sql   = "UPDATE hasil_klasifikasi_keluarga SET id_klasifikasi= null, verifikasi='0' WHERE id_klasifikasi='$id' ";
				$outp=$this->db->query($sql);
			}
		}
		else $outp = false;
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function list_detail_rentang($id){
	
		$sql   = "SELECT k.*, p.nama as kepala_kk, (SELECT COUNT(id) FROM tweb_penduduk WHERE id_kk=a.id_kel) as jml_anggota, c.dusun, c.rw, c.rt 
				FROM hasil_analisis_keluarga a LEFT JOIN tweb_rtm k ON a.id_kel=k.id LEFT JOIN tweb_penduduk p ON k.nik_kepala=p.id LEFT JOIN tweb_wil_clusterdesa c ON p.id_cluster=c.id WHERE  hasil>=(select dari from klasifikasi_analisis_keluarga where id=$id) and hasil<=(select sampai from klasifikasi_analisis_keluarga where id=$id)  ";  
		
		$sql .= $thnsql;
		//$sql .= $blnsql;
		$sql .= $this->dusun_sql();
		$sql .= $this->rw_sql();
		$sql .= $this->rt_sql();
		
		$query = $this->db->query($sql);
		$data=$query->result_array();
		
		//Formating Output
		$i=0;
		$j=$offset;
		$x=$offset;
		while($i<count($data)){
			$data[$i]['no']=$j+1;
			
			$i++;
			$j++;
			
		}

		return $data;
	}
	
	function list_data_jenis(){

		$sql   = "SELECT r.* FROM jenis_analisis_keluarga r WHERE 1 order by nama "; 
		$query = $this->db->query($sql);
		$data=$query->result_array();

		return $data;
	}
	
	function get_jenis_analisis($id=0){	
		$sql   = "SELECT * FROM jenis_analisis_keluarga WHERE id= $id ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}
	
	function insert_jenis(){
		$data = $_POST;
		$outp = $this->db->insert('jenis_analisis_keluarga',$data);
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
		
	function update_jenis($id=0){
		$data = $_POST;
		$sql   = "UPDATE jenis_analisis_keluarga SET nama='$data[nama]' WHERE id='$id' ";
		$outp=$this->db->query($sql);
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function delete_jenis($id=0){
		$sql   = "DELETE FROM jenis_analisis_keluarga WHERE id='$id' ";
		$outp=$this->db->query($sql);
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function delete_all_jenis(){
		$id_cb = $_POST['id_cb'];
		
		if(count($id_cb)){
			foreach($id_cb as $id){
				$sql  = "DELETE FROM jenis_analisis_keluarga WHERE id=?";
				$outp = $this->db->query($sql,array($id));
			}
		}
		else $outp = false;
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function list_jenis(){
		$sql   = "SELECT * FROM jenis_analisis_keluarga WHERE 1 ";
		
		$query = $this->db->query($sql);
		$data  = $query->result_array();
		return $data;
	}
	
	function list_hasil_analisis(){
		$sql   = "SELECT * FROM klasifikasi_analisis_keluarga WHERE jenis=$_SESSION[jenis] ";
		
		$query = $this->db->query($sql);
		$data  = $query->result_array();
		return $data;
	}
	
	function get_hasil_analisis($id=0){	
		$sql   = "SELECT * FROM hasil_analisis_keluarga where id_kel=$id and tahun=$_SESSION[tahun] and jenis=$_SESSION[jenis] ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data;
	}
	
	function update_hasil_klasifikasi($id=0){
		$data = $_POST;
		$sql   = "UPDATE hasil_analisis_keluarga SET id_klasifikasi=$data[id_klasifikasi], verifikasi='1' WHERE id_kel='$id' and tahun=$_SESSION[tahun] and jenis=$_SESSION[jenis] ";
		$outp=$this->db->query($sql);
		
		$sql   = "UPDATE tweb_rtm t SET kelas_sosial=(SELECT id_klasifikasi FROM hasil_analisis_keluarga WHERE id_kel=t.id AND tahun=(SELECT MAX(tahun) FROM hasil_analisis_keluarga WHERE id_kel=t.id)) ";
		$query = $this->db->query($sql);
		
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function verifikasi($o=0,$offset=0,$limit=500){
		$id_ver = $_POST['verifikasi'];
		
		$order_sql=" order by (SELECT hasil from hasil_analisis_keluarga WHERE id_kel=u.id  AND tahun=$_SESSION[tahun] AND jenis=$_SESSION[jenis]) desc ";
		$paging_sql = ' LIMIT ' .$offset. ',' .$limit;
		
		$sql="SELECT u.id as id_kel
		FROM tweb_rtm u LEFT JOIN tweb_penduduk t ON u.nik_kepala = t.id LEFT JOIN tweb_wil_clusterdesa c ON t.id_cluster=c.id WHERE 1 ";

		$sql .= $this->search_keluarga_sql();
		$sql .= $this->dusun_sql();
		$sql .= $this->rw_sql();
		$sql .= $this->rt_sql();
		$sql .= $order_sql;
		$sql .= $paging_sql;
		$query = $this->db->query($sql);
		$keluarga=$query->result_array();
		
		foreach($keluarga as $data){
			$sql  = "UPDATE hasil_analisis_keluarga set verifikasi='0' where id_kel=$data[id_kel] and tahun=$_SESSION[tahun] and jenis=$_SESSION[jenis] ";
			$outp = $this->db->query($sql);
		}
		
		if(count($id_ver)){
			foreach($id_ver as $id){
				$sql  = "UPDATE hasil_analisis_keluarga set verifikasi='1' where id_kel=$id and tahun=$_SESSION[tahun] and jenis=$_SESSION[jenis] ";
				$outp = $this->db->query($sql);
			}
		}
		
		if($outp) $_SESSION['success']=1;
			else $_SESSION['success']=-1;
	}
	
	function list_kelas(){	
		$sql   = "SELECT id,nama AS kelas FROM klasifikasi_analisis_keluarga WHERE 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function kategori_proses(){
		$_SESSION['kategori'] = $_POST['id_cb'];
	}
	
	function klaster_proses(){
		$_SESSION['klaster'] = $_POST['id_cb'];
	}
	
	function list_jawab2(){	
		if(isset($_SESSION['jawab']))
			$kf = $_SESSION['jawab'];
		else
			$kf= "777";

		$sql   = "SELECT s.id as id_jawaban,m.id,m.nama as pertanyaan,s.nama as jawaban,(SELECT count(id) FROM sub_analisis_keluarga WHERE id IN ($kf) AND id = s.id) as cek FROM master_analisis_keluarga m LEFT JOIN sub_analisis_keluarga s ON m.id = s.id_master";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function hitung_jmkf($ids=""){	
		$sql   = "SELECT COUNT(DISTINCT a.id_master) AS jml FROM sub_analisis_keluarga a WHERE id IN($ids) ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		return $data['jml'];
	}
	
	function list_kategori($ids=""){	
		$sql   = "SELECT nama FROM sub_analisis_keluarga WHERE id IN($ids)";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		//Formating Output
		$i=0;
		while($i<count($data)){
			$datax = $datax." | ".$data[$i]['nama'];
			$i++;
		}

		return $datax;
	}
	
}

?>
