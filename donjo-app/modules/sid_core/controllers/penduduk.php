 <?php 
/*
 * Berkas default dari halaman web utk publik
 * 
 * Copyright 2013 
 * Rizka Himawan <himawan.rizka@gmail.com>
 * Muhammad Khollilurrohman <adsakle1@gmail.com>
 * Asep Nur Ajiyati <asepnurajiyati@gmail.com>
 *
 * SID adalah software tak berbayar (Opensource) yang boleh digunakan oleh siapa saja selama bukan untuk kepentingan profit atau komersial.
 * Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat ciptaan turunan bukan untuk kepentingan komersial
 * selama mereka mencantumkan asal pembuat kepada Anda dan melisensikan ciptaan turunan dengan syarat yang serupa dengan ciptaan asli.
 * Untuk mendapatkan SID RESMI, Anda diharuskan mengirimkan surat permohonan ataupun izin SID terlebih dahulu, 
 * aplikasi ini akan tetap bersifat opensource dan anda tidak dikenai biaya.
 * Bagaimana mendapatkan izin SID, ikuti link dibawah ini:
 * http://lumbungkomunitas.net/bergabung/pendaftaran/daftar-online/
 * Creative Commons Attribution-NonCommercial 3.0 Unported License
 * SID Opensource TIDAK BOLEH digunakan dengan tujuan profit atau segala usaha  yang bertujuan untuk mencari keuntungan. 
 * Pelanggaran HaKI (Hak Kekayaan Intelektual) merupakan tindakan  yang menghancurkan dan menghambat karya bangsa.
 */
?>

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penduduk extends CI_Controller{

	function __construct(){
		parent::__construct();
		session_start();
		$this->load->model('user_model');
		$grup	= $this->user_model->sesi_grup($_SESSION['sesi']);
		if($grup!=1 AND $grup!=2) redirect('siteman');
		
		$this->load->model('penduduk_model');
		$this->load->model('header_model');
		
	}
	
	function clear(){
		unset($_SESSION['log']);
		unset($_SESSION['cari']);
		unset($_SESSION['filter']);
		unset($_SESSION['sex']);
		unset($_SESSION['warganegara']);
		unset($_SESSION['cacat']);
		unset($_SESSION['menahun']);
		unset($_SESSION['cacatx']);
		unset($_SESSION['menahunx']);		
		unset($_SESSION['golongan_darah']);
		unset($_SESSION['dusun']);
		unset($_SESSION['rw']);
		unset($_SESSION['rt']);
		unset($_SESSION['agama']);
		unset($_SESSION['umur_min']);
		unset($_SESSION['umur_max']);
		unset($_SESSION['pekerjaan_id']);
		unset($_SESSION['status']);
		unset($_SESSION['pendidikan_id']);
		unset($_SESSION['pendidikan_kk_id']);
		unset($_SESSION['umurx']);
		unset($_SESSION['status_penduduk']);
		unset($_SESSION['judul_statistik']);
		unset($_SESSION['hamil']);
		$_SESSION['per_page'] = 50;
		$_SESSION['tahun'] = date("Y");
		unset($_SESSION['kategori']);
		unset($_SESSION['klaster']);
		redirect('sid_core/penduduk');
	}
	
	function index($p=1,$o=0){
		
		unset($_SESSION['log']);
	
		$data['noticer'] = "";
		$data['noticer_akp'] = "";
		$data['p']        = $p;
		$data['o']        = $o;
		
		if(isset($_SESSION['dusun'])){
			$data['dusun'] = $_SESSION['dusun'];
			$data['noticer'] .= " | Dusun : <b>".$_SESSION['dusun']."</b>";
			$data['list_rw'] = $this->penduduk_model->list_rw($data['dusun']);
			
			if(isset($_SESSION['rw'])){
				$data['rw'] = $_SESSION['rw'];
				$data['noticer'] .= " | RW : <b>".$_SESSION['rw']."</b>";
				$data['list_rt'] = $this->penduduk_model->list_rt($data['dusun'],$data['rw']);
							
				if(isset($_SESSION['rt'])){
					$data['rt'] = $_SESSION['rt'];
					$data['noticer'] .= " | RT : <b>".$_SESSION['rt']."</b>";
				}
				else $data['rt'] = '';	
			
			}
			else $data['rw'] = '';
				
		}else{
			$data['dusun'] = '';
			$data['rw'] = '';
			$data['rt'] = '';
			unset($_SESSION['dusun']);
			unset($_SESSION['rw']);
			unset($_SESSION['rt']);
		}
		if(isset($_SESSION['cari'])){
			$data['cari'] = $_SESSION['cari'];
			$data['noticer'] .= " | Nama : <b>".$data['cari']."</b>";
		}else $data['cari'] = '';
		
		if(isset($_SESSION['judul_statistik']))
			$data['judul_statistik'] = $_SESSION['judul_statistik'];
		else $data['judul_statistik'] = '';
		
		if(isset($_SESSION['filter'])){
			$data['filter'] = $_SESSION['filter'];
			$data['noticer'] .= " | Status Penduduk : <b>".$this->penduduk_model->get_status($data['filter'])."</b>";
		}
		else $data['filter'] = '';
	
		if(isset($_SESSION['sex'])){
			$data['sex'] = $_SESSION['sex'];
			$data['noticer'] .= " | Jenis Kelamin : <b>".$this->penduduk_model->get_sex($data['sex'])."</b>";
		}else $data['sex'] = '';
		
		if(isset($_SESSION['agama'])){
			$data['agama'] = $_SESSION['agama'];
			$data['noticer'] .= " | Agama : <b>".$this->penduduk_model->get_agama($data['agama'])."</b>";
		}else $data['agama'] = '';

		if(isset($_SESSION['tahun'])){
			$data['tahun'] = $_SESSION['tahun'];
			if(isset($_SESSION['kategori']) OR isset($_SESSION['klaster'])){
				$data['noticer_akp'] .= " | Periode AKP : <b>".$data['tahun']."</b>";
			}
		}else $data['tahun'] = date("Y");

        if(isset($_SESSION['cacat']))
			$data['cacat'] = $_SESSION['cacat'];
		else $data['cacat'] = '';
		
		if(isset($_SESSION['pekerjaan_id'])){
			$data['pekerjaan_id'] = $_SESSION['pekerjaan_id'];
			$data['noticer'] .= " | Pekerjaan : <b>".$this->penduduk_model->get_pekerjaan($data['pekerjaan_id'])."</b>";
		}else $data['pekerjaan_id'] = '';

		if(isset($_SESSION['status']))
			$data['status'] = $_SESSION['status'];
		else $data['status'] = '';

		if(isset($_SESSION['pendidikan_id'])){
			$data['pendidikan_id'] = $_SESSION['pendidikan_id'];
			$data['noticer'] .= " | Pendidikan Sedang : <b>".$this->penduduk_model->get_pendidikan_sedang($data['pendidikan_id'])."</b>";
		}else $data['pendidikan_id'] = '';

		if(isset($_SESSION['pendidikan_kk_id'])){
			$data['pendidikan_kk_id'] = $_SESSION['pendidikan_kk_id'];
			$data['noticer'] .= " | Pendidikan KK : <b>".$this->penduduk_model->get_pendidikan_kk($data['pendidikan_kk_id'])."</b>";
		}else $data['pendidikan_kk_id'] = '';

		if(isset($_SESSION['status_penduduk']))
			$data['status_penduduk'] = $_SESSION['status_penduduk'];
		else $data['status_penduduk'] = '';

        if(isset($_SESSION['umur_min']) AND !isset($_SESSION['umur_max']))
			$data['noticer'] .= " | Usia <b>".$_SESSION['umur_min']."Th ke atas</b>";
				
        if(isset($_SESSION['umur_max']) AND !isset($_SESSION['umur_min']))
			$data['noticer'] .= " | Usia <b>".$_SESSION['umur_max']."Th ke bawah</b>";
				
        if(isset($_SESSION['umur_max']) AND isset($_SESSION['umur_min']))
			$data['noticer'] .= " | Usia <b>".$_SESSION['umur_min']."Th s/d ".$_SESSION['umur_max']."Th</b>";
		
		if(isset($_SESSION['kategori'])){
			$data['noticer_akp'] .= " | Klasifikasi Daerah : ".$this->penduduk_model->get_akp_daerah()."";
		}
		
		if(isset($_SESSION['klaster'])){
			$data['noticer_akp'] .= " | Klasifikasi Kluster : ".$this->penduduk_model->get_akp_klaster()."";
		}
		
		if(isset($_POST['per_page'])) 
			$_SESSION['per_page']=$_POST['per_page'];
		$data['per_page'] = $_SESSION['per_page'];
		
		$data['grup']	= $this->user_model->sesi_grup($_SESSION['sesi']);
		$data['paging']  = $this->penduduk_model->paging($p,$o);
		$data['main']    = $this->penduduk_model->list_data($o, $data['paging']->offset, $data['paging']->per_page);
		$data['keyword'] = $this->penduduk_model->autocomplete();
		$data['list_agama'] = $this->penduduk_model->list_agama();
		$data['list_dusun'] = $this->penduduk_model->list_dusun();
		
		$header = $this->header_model->get_data();
		$nav['act']= 2;
		
		$this->load->view('header', $header);
		$this->load->view('sid/menu');
		$this->load->view('sid/nav',$nav);
		$this->load->view('sid/kependudukan/penduduk',$data);
		$this->load->view('footer');
	}
	
	function ajax_kategori(){
	
		$data['list_kelas']   	= $this->penduduk_model->list_kelas();
		$data['form_action'] = site_url("sid_core/penduduk/kategori_proses");
	
		$this->load->view('sid/kependudukan/ajax_kategori_form', $data);
	}
	
	function kategori_proses(){
		unset($_SESSION['kategori']);
		$this->penduduk_model->kategori_proses();
		redirect('sid_core/penduduk');
	}
		
	function ajax_klaster(){
	
		$data['list_kelas']   	= $this->penduduk_model->list_kelas();
		$data['form_action'] = site_url("sid_core/penduduk/klaster_proses");
	
		$this->load->view('sid/kependudukan/ajax_klaster_form', $data);
	}
	
	function klaster_proses(){
		unset($_SESSION['klaster']);
		$this->penduduk_model->klaster_proses();
		redirect('sid_core/penduduk');
	}
		
	function form($p=1,$o=0,$id=''){
	
		$data['p'] = $p;
		$data['o'] = $o;
		
		if(isset($_POST['dusun']))
			$data['dus_sel'] = $_POST['dusun'];
		else
			$data['dus_sel'] = '';
			
		if(isset($_POST['rw']))
			$data['rw_sel'] = $_POST['rw'];
		else
			$data['rw_sel'] = '';
			
		if(isset($_POST['rt']))
			$data['rt_sel'] = $_POST['rt'];
		else
			$data['rt_sel'] = '';
			
		if($id){
			$data['penduduk']        = $this->penduduk_model->get_penduduk($id);
			$data['form_action'] = site_url("sid_core/penduduk/update/$p/$o/$id");
		}
		else{
			$data['penduduk']        = null;
			$data['form_action'] = site_url("sid_core/penduduk/insert");
		}
		
		$header = $this->header_model->get_data();
		$data['dusun'] = $this->penduduk_model->list_dusun();
		$data['rw']    = $this->penduduk_model->list_rw($data['dus_sel']);
		$data['rt']    = $this->penduduk_model->list_rt($data['dus_sel'],$data['rw_sel']);
		$data['agama'] = $this->penduduk_model->list_agama();
		$data['pendidikan_telah'] = $this->penduduk_model->list_pendidikan_telah();
		$data['pendidikan_sedang'] = $this->penduduk_model->list_pendidikan_sedang();
		$data['pendidikan_kk'] = $this->penduduk_model->list_pendidikan_kk();
		$data['pekerjaan'] = $this->penduduk_model->list_pekerjaan();
		$data['warganegara'] = $this->penduduk_model->list_warganegara();
		$data['hubungan'] = $this->penduduk_model->list_hubungan();
		$data['kawin'] = $this->penduduk_model->list_status_kawin();
		$data['golongan_darah'] = $this->penduduk_model->list_golongan_darah();
		$data['cacat'] = $this->penduduk_model->list_cacat();
		
		$this->load->view('header', $header);
		$nav['act']= 2;
		$this->load->view('sid/menu');
		$this->load->view('sid/nav',$nav);
		$this->load->view('sid/kependudukan/penduduk_form',$data);
		$this->load->view('footer');
	}

        function detail($p=1,$o=0,$id=''){
	
		$data['p'] = $p;
		$data['o'] = $o;
		$data['penduduk'] = $this->penduduk_model->get_penduduk($id);
		$header = $this->header_model->get_data();
		
		$this->load->view('header', $header);
		$nav['act']= 2;
		$this->load->view('sid/menu');
		$this->load->view('sid/nav',$nav);
		$this->load->view('sid/kependudukan/penduduk_detail',$data);
		$this->load->view('footer');
	}

        function cetak_biodata($id=''){
		$data['penduduk'] = $this->penduduk_model->get_penduduk($id);
		$this->load->view('sid/kependudukan/cetak_biodata',$data);
	}

	function search(){
		$cari = $this->input->post('cari');
		if($cari!='')
			$_SESSION['cari']=$cari;
		else unset($_SESSION['cari']);
		redirect('sid_core/penduduk');
	}
	
	function filter(){
		$filter = $this->input->post('filter');
		if($filter!="")
			$_SESSION['filter']=$filter;
		else unset($_SESSION['filter']);
		redirect('sid_core/penduduk');
	}
	
	function sex(){
		$sex = $this->input->post('sex');
		if($sex!="")
			$_SESSION['sex']=$sex;
		else unset($_SESSION['sex']);
		redirect('sid_core/penduduk');
	}
	
	function agama(){
		$agama = $this->input->post('agama');
		if($agama!="")
			$_SESSION['agama']=$agama;
		else unset($_SESSION['agama']);
		redirect('sid_core/penduduk');
	}
	
	function warganegara(){
		$warganegara = $this->input->post('warganegara');
		if($warganegara!="")
			$_SESSION['warganegara']=$warganegara;
		else unset($_SESSION['warganegara']);
		redirect('sid_core/penduduk');
	}
	
	function dusun(){
		unset($_SESSION['rw']);
		unset($_SESSION['rt']);
		$dusun = $this->input->post('dusun');
		if($dusun!="")
			$_SESSION['dusun']=$dusun;
		else unset($_SESSION['dusun']);
		redirect('sid_core/penduduk');
	}
	
	function rw(){
		unset($_SESSION['rt']);
		$rw = $this->input->post('rw');
		if($rw!="")
			$_SESSION['rw']=$rw;
		else unset($_SESSION['rw']);
		redirect('sid_core/penduduk');
	}
	
	function rt(){
		$rt = $this->input->post('rt');
		if($rt!="")
			$_SESSION['rt']=$rt;
		else unset($_SESSION['rt']);
		redirect('sid_core/penduduk');
	}
	
	function tahun(){
		$tahun = $this->input->post('tahun');
		if($tahun!=0)
			$_SESSION['tahun']=$tahun;
		else $_SESSION['tahun'] = date("Y");
		redirect('sid_core/penduduk');
	}
	
	function insert(){
		$this->penduduk_model->insert();
		redirect('sid_core/penduduk');
	}
	
	function update($p=1,$o=0,$id=''){
		$this->penduduk_model->update($id);
		redirect("sid_core/penduduk/index/$p/$o");
	}
	
	function update_maps($p=1,$o=0,$id=''){
		$this->penduduk_model->update_position($id);
		redirect("sid_core/penduduk/index/$p/$o");
	}
		
	function delete_confirm($p=1,$o=0,$id=''){
		$data['form_action'] = site_url("sid_core/penduduk/index/$p/$o/$id");
		$this->load->view("sid/kependudukan/ajax_delete", $data);
	}
	
	function delete($p=1,$o=0,$id=''){
		$this->penduduk_model->delete($id);
		redirect("sid_core/penduduk/index/$p/$o");
	}
	
	function delete_all($p=1,$o=0){
		$this->penduduk_model->delete_all();
		redirect("sid_core/penduduk/index/$p/$o");
	}
	
	function ajax_adv_search(){
	
	
		if(isset($_SESSION['cari']))
			$data['cari'] = $_SESSION['cari'];
		else $data['cari'] = '';
		
		if(isset($_SESSION['judul_statistik']))
			$data['judul_statistik'] = $_SESSION['judul_statistik'];
		else $data['judul_statistik'] = '';
		
		if(isset($_SESSION['filter']))
			$data['filter'] = $_SESSION['filter'];
		else $data['filter'] = '';
	
		if(isset($_SESSION['sex']))
			$data['sex'] = $_SESSION['sex'];
		else $data['sex'] = '';
		
		if(isset($_SESSION['umur_min']))
			$data['umur_min'] = $_SESSION['umur_min'];
		else $data['umur_min'] = '';
		
		if(isset($_SESSION['umur_max']))
			$data['umur_max'] = $_SESSION['umur_max'];
		else $data['umur_max'] = '';
		
		if(isset($_SESSION['agama']))
			$data['agama'] = $_SESSION['agama'];
		else $data['agama'] = '';

		if(isset($_SESSION['tahun']))
			$data['tahun'] = $_SESSION['tahun'];
		else $data['tahun'] = date("Y");

        if(isset($_SESSION['cacat']))
			$data['cacat'] = $_SESSION['cacat'];
		else $data['cacat'] = '';
		
		if(isset($_SESSION['pekerjaan_id']))
			$data['pekerjaan_id'] = $_SESSION['pekerjaan_id'];
		else $data['pekerjaan_id'] = '';

		if(isset($_SESSION['status']))
			$data['status'] = $_SESSION['status'];
		else $data['status'] = '';

		if(isset($_SESSION['pendidikan_id']))
			$data['pendidikan_id'] = $_SESSION['pendidikan_id'];
		else $data['pendidikan_id'] = '';

		if(isset($_SESSION['pendidikan_kk_id']))
			$data['pendidikan_kk_id'] = $_SESSION['pendidikan_kk_id'];
		else $data['pendidikan_kk_id'] = '';

		if(isset($_SESSION['status_penduduk']))
			$data['status_penduduk'] = $_SESSION['status_penduduk'];
		else $data['status_penduduk'] = '';
		
		$data['list_agama'] = $this->penduduk_model->list_agama();
		$data['pendidikan'] = $this->penduduk_model->list_pendidikan();
		$data['pendidikan_kk'] = $this->penduduk_model->list_pendidikan_kk();
		$data['pekerjaan'] = $this->penduduk_model->list_pekerjaan();
		$data['form_action'] = site_url("sid_core/penduduk/adv_search_proses");
	
		$this->load->view("sid/kependudukan/ajax_adv_search_form", $data);
	}
	
	function adv_search_proses(){
	
		$adv_search = $_POST;
		$i=0;
		while($i++ < count($adv_search)){
			$col[$i] = key($adv_search);
				next($adv_search);
		}
		$i=0;
		while($i++ < count($col)){
			if($adv_search[$col[$i]]==""){
				UNSET($adv_search[$col[$i]]);
				UNSET($_SESSION[$col[$i]]);
			}else{
				$_SESSION[$col[$i]]=$adv_search[$col[$i]];
			}
		}
		
		redirect('sid_core/penduduk');
	}
	
	function akp_detail($id=0){
	
		$this->load->model('rtm_model');
		$data['main']     = $this->rtm_model->list_anggota($id);
		$data['kk'] 			  = $this->rtm_model->get_kepala_kk($id);
		
		$this->load->view('sid/kependudukan/ajax_detail_akp',$data);
	}
	
	function ajax_penduduk_pindah($id=0){
	
		$data['dusun'] = $this->penduduk_model->list_dusun();
		
		$data['form_action'] = site_url("sid_core/penduduk/pindah_proses/$id");
		$this->load->view('sid/kependudukan/ajax_pindah_form',$data);
	}
	
	function ajax_penduduk_pindah_rw($dusun=''){
		$rw = $this->penduduk_model->list_rw($dusun);
		
		echo"<td>RW</td>
		<td><select name='rw' onchange=RWSel('".$dusun."',this.value)>
		<option value=''>Pilih RW&nbsp;</option>";
		foreach($rw as $data){
			echo "<option>".$data['rw']."</option>";
		}echo"</select>
		</td>";
	}
	
	function ajax_penduduk_pindah_rt($dusun='',$rw=''){
		$rt = $this->penduduk_model->list_rt($dusun,$rw);

		echo "<td>RT</td>
		<td><select name='id_cluster'>
		<option value=''>Pilih RT&nbsp;</option>";
		foreach($rt as $data){
			echo "<option value=".$data['id'].">".$data['rt']."</option>";
		}echo"</select>
		</td>";
	}
	
	
	function ajax_penduduk_cari_rw($dusun=''){
		$rw = $this->penduduk_model->list_rw($dusun);
		
		echo"<td>RW</td>
		<td><select name='rw' onchange=RWSel('".$dusun."',this.value)>
		<option value=''>Pilih RW&nbsp;</option>";
		foreach($rw as $data){
			echo "<option>".$data['rw']."</option>";
		}echo"</select>
		</td>";
	}
	
	function ajax_penduduk_cari_rt($dusun='',$rw=''){
		$rt = $this->penduduk_model->list_rt($dusun,$rw);

		echo "<td>RT</td>
		<td><select name='rt'>
		<option value=''>Pilih RT&nbsp;</option>";
		foreach($rt as $data){
			echo "<option value=".$data['rt'].">".$data['rt']."</option>";
		}echo"</select>
		</td>";
	}
	
	function pindah_proses($id=0){
		$id_cluster = $_POST['id_cluster'];
		$this->penduduk_model->pindah_proses($id,$id_cluster);
		redirect("sid_core/penduduk");
	}
	
	function ajax_penduduk_maps($p=1,$o=0,$id=''){

		$data['p'] = $p;
		$data['o'] = $o;
		
		$data['penduduk'] = $this->penduduk_model->get_penduduk($id);
		$data['desa'] = $this->penduduk_model->get_desa();
		
		$data['form_action'] = site_url("sid_core/penduduk/update_maps/$p/$o/$id");
		
		$this->load->view("sid/kependudukan/maps", $data);
	}
			
	function wilayah_sel($p=1,$o=0,$id=''){

		$data['p'] = $p;
		$data['o'] = $o;
		
		$data['form_action'] = site_url("sid_core/penduduk");
		
		$this->load->view("sid/kependudukan/maps", $data);
	}
	
	
	function edit_status_dasar($p=1,$o=0,$id=0){
	$data['nik']          = $this->penduduk_model->get_penduduk($id);
		$data['form_action'] = site_url("sid_core/penduduk/update_status_dasar/$p/$o/$id");
		$this->load->view('sid/kependudukan/ajax_edit_status_dasar',$data);
	}
			
	function update_status_dasar($p=1,$o=0,$id=''){
		$this->penduduk_model->update_status_dasar($id);
		redirect("sid_core/penduduk/index/$p/$o");
	}
		
	function cetak($o=0){

		$data['main']    = $this->penduduk_model->list_data($o,0, 10000);

		$this->load->view('sid/kependudukan/penduduk_print',$data);
	}

	function excel($o=0){

		$data['main']    = $this->penduduk_model->list_data($o,0, 10000);

		$this->load->view('sid/kependudukan/penduduk_excel',$data);
	}
		
	function statistik($tipe=0,$nomor=0){
		unset($_SESSION['log']);
		unset($_SESSION['cari']);
		unset($_SESSION['filter']);
		unset($_SESSION['sex']);
		unset($_SESSION['warganegara']);
		unset($_SESSION['cacat']);
		unset($_SESSION['menahun']);
		unset($_SESSION['golongan_darah']);
		unset($_SESSION['dusun']);
		unset($_SESSION['rw']);
		unset($_SESSION['rt']);
		unset($_SESSION['agama']);
		unset($_SESSION['umur_min']);
		unset($_SESSION['umur_max']);
		unset($_SESSION['pekerjaan_id']);
		unset($_SESSION['status']);
		unset($_SESSION['pendidikan_id']);
		unset($_SESSION['pendidikan_kk_id']);
		unset($_SESSION['status_penduduk']);
		unset($_SESSION['umurx']);
		switch($tipe){
			case 0: $_SESSION['pendidikan_id'] = $nomor; $pre="PENDIDIKAN : "; break;
			case 1: $_SESSION['pekerjaan_id'] = $nomor; $pre="PEKERJAAN : ";  break;
			case 2: $_SESSION['status'] = $nomor; $pre="STATUS PERKAWINAN : ";  break;
			case 3: $_SESSION['agama'] = $nomor; $pre="AGAMA : ";  break;
			case 4: $_SESSION['sex'] = $nomor; $pre="JENIS KELAMIN : ";  break;
			case 5: $_SESSION['warganegara'] = $nomor;  $pre="WARGANEGARA : "; break;
			case 6: $_SESSION['status_penduduk'] = $nomor; $pre="STATUS PENDUDUK : ";  break;
			case 7: $_SESSION['golongan_darah'] = $nomor; $pre="GOLONGAN DARAH : ";  break;
			case 9: $_SESSION['cacat'] = $nomor; $pre="CACAT : ";  break;
			case 10: $_SESSION['menahun'] = $nomor;  $pre="SAKIT MENAHUN : "; break;
			case 11: $_SESSION['jamkesmas'] = $nomor;  $pre="JAMKESMAS : "; break;
			case 12: $_SESSION['pendidikan_kk_id'] = $nomor;  $pre="PENDIDIKAN DALAM KK : "; break;	
			case 13: $_SESSION['umurx'] = $nomor;  $pre="UMUR "; break;						
		}
		$judul= $this->penduduk_model->get_judul_statistik($tipe,$nomor);
		if($judul['nama']){
			$_SESSION['judul_statistik']=$pre.$judul['nama'];
		}else{
			unset($_SESSION['judul_statistik']);
		}
		redirect('sid_core/penduduk');
	}
	
	function lap_statistik($id_cluster=0,$tipe=0,$nomor=0){
		unset($_SESSION['sex']);
		unset($_SESSION['cacatx']);
		unset($_SESSION['menahun']);
		unset($_SESSION['menahunx']);
		unset($_SESSION['dusun']);
		unset($_SESSION['rw']);
		unset($_SESSION['rt']);
		unset($_SESSION['umur_min']);
		unset($_SESSION['umur_max']);
		unset($_SESSION['hamil']);
		unset($_SESSION['status']);
		$cluster= $this->penduduk_model->get_cluster($id_cluster);
		switch($tipe){
			case 1: 
				$_SESSION['sex'] = '1'; 
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="JENIS KELAMIN LAKI-LAKI  ";  
				break;
			case 2: 
				$_SESSION['sex'] = '2'; 
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt'];  
				$pre="JENIS KELAMIN PEREMPUAN ";  
				break;
			case 3: 
				$_SESSION['umur_min'] = '0'; 
				$_SESSION['umur_max'] = '0';  
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="BERUMUR <1 ";  
				break;
			case 4: 
				$_SESSION['umur_min'] = '1'; 
				$_SESSION['umur_max'] = '5';  
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt'];  
				$pre="BERUMUR 1-5 ";  
				break;
			case 5: 
				$_SESSION['umur_min'] = '6'; 
				$_SESSION['umur_max'] = '12'; 
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="BERUMUR 6-12 ";   
				break;
			case 6: 
				$_SESSION['umur_min'] = '13'; 
				$_SESSION['umur_max'] = '15';  
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="BERUMUR 13-16 ";  
				break;
			case 7: 
				$_SESSION['umur_min'] = '16'; 
				$_SESSION['umur_max'] = '18';  
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="BERUMUR 16-18 ";  
				break;
			case 8: 
				$_SESSION['umur_min'] = '61';  
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="BERUMUR >60";  
				break;
			case 9: 
				$_SESSION['cacatx'] = '7';   
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="CACAT "; 
				break;
			case 10: 
				$_SESSION['menahunx'] = '14'; 
				$_SESSION['sex']='1' ; 
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="SAKIT MENAHUN LAKI-LAKI "; 
				break;
			case 11:
				$_SESSION['menahunx'] = '14'; 
				$_SESSION['sex']='2';  
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt']; 
				$pre="SAKIT MENAHUN PEREMPUAN "; 
				break;	
			case 12: 
				$_SESSION['hamil'] = '1';  
				$_SESSION['dusun']=$cluster['dusun']; 
				$_SESSION['rw']=$cluster['rw']; 
				$_SESSION['rt']=$cluster['rt'];  
				$pre="HAMIL "; 
				break;						
		}
		//$judul= $this->penduduk_model->get_judul_lap_statistik($tipe,$nomor);
		if($pre){
			$_SESSION['judul_statistik']=$pre;
		}else{
			unset($_SESSION['judul_statistik']);
		}
		redirect("sid_core/penduduk");
	}
}
