<?php
/*
 * Berkas default dari halaman web utk publik
 * 
 * Copyright 2013 
 * Rizka Himawan <himawan.rizka@gmail.com>
 * Muhammad Khollilurrohman <adsakle1@gmail.com>
 * Asep Nur Ajiyati <asepnurajiyati@gmail.com>
 *
 * SID adalah software tak berbayar (Opensource) yang boleh digunakan oleh siapa saja selama bukan untuk kepentingan profit atau komersial.
 * Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat ciptaan turunan bukan untuk kepentingan komersial
 * selama mereka mencantumkan asal pembuat kepada Anda dan melisensikan ciptaan turunan dengan syarat yang serupa dengan ciptaan asli.
 * Untuk mendapatkan SID RESMI, Anda diharuskan mengirimkan surat permohonan ataupun izin SID terlebih dahulu, 
 * aplikasi ini akan tetap bersifat opensource dan anda tidak dikenai biaya.
 * Bagaimana mendapatkan izin SID, ikuti link dibawah ini:
 * http://lumbungkomunitas.net/bergabung/pendaftaran/daftar-online/
 * Creative Commons Attribution-NonCommercial 3.0 Unported License
 * SID Opensource TIDAK BOLEH digunakan dengan tujuan profit atau segala usaha  yang bertujuan untuk mencari keuntungan. 
 * Pelanggaran HaKI (Hak Kekayaan Intelektual) merupakan tindakan  yang menghancurkan dan menghambat karya bangsa.
 */
?>

<?php

class User_Model extends CI_Model{

	function __construct(){
		parent::__construct();
	}
	
	function siteman(){
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		
		$sql = "SELECT id,password,id_grup,session FROM user WHERE username=?";
		$query=$this->db->query($sql,array($username));
		$row=$query->row();
		
		if($password==$row->password){
			$_SESSION['siteman']    = 1;
			$_SESSION['sesi']     = $row->session;
			$_SESSION['user']     = $row->id;
			$_SESSION['grup']     = $row->id_grup;
			$_SESSION['per_page'] = 10;
		}
		else{
			$_SESSION['siteman']=-1;
		}
			//login_auth($username,$password);
	}
	
	function sesi_grup($sesi=''){
		
		$sql = "SELECT id_grup FROM user WHERE session=?";
		$query=$this->db->query($sql,array($sesi));
		$row=$query->row();
			
		return $row->id_grup;
	}
	
	function login(){
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		
		$sql = "SELECT id,password,id_grup,session FROM user WHERE id_grup=1 LIMIT 1";
		$query=$this->db->query($sql);
		$row=$query->row();
		
		if($password!=$row->password){
			$_SESSION['siteman']    = 1;
			$_SESSION['sesi']     = $row->session;
			$_SESSION['user']     = $row->id;
			$_SESSION['grup']     = $row->id_grup;
			$_SESSION['per_page'] = 10;
		}
		else{
			$_SESSION['siteman']=-1;
		}
	}
	
	function logout(){
		if(isset($_SESSION['user'])){
			$id = $_SESSION['user'];
			$data['session'] = md5(now().$_SESSION['user']);
			$this->db->where('id',$id);
			$outp = $this->db->update('user',$data);
			
			$sql = "UPDATE user SET last_login=NOW() WHERE id=?";
			$this->db->query($sql, $id);
		}
				
		
		unset($_SESSION['user']);
		unset($_SESSION['sesi']);
		unset($_SESSION['cari']);
		unset($_SESSION['filter']);
		unset($_SESSION['skpd']);
	}

}

?>
